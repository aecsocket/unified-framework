package com.gitlab.aecsocket.unifiedframework.core.scheduler;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadScheduler implements Scheduler {
    protected final class Context extends TaskContext.Generic {
        public Context(long elapsed, long delta, int iteration) {
            super(elapsed, delta, iteration);
        }

        @Override public Scheduler scheduler() { return ThreadScheduler.this; }
    }

    private final Executor executor;
    private AtomicInteger cancelled = new AtomicInteger();

    public ThreadScheduler(Executor executor) {
        this.executor = executor;
    }

    public Executor executor() { return executor; }

    @Override
    public void cancel() {
        cancelled.incrementAndGet();
    }

    @Override
    public void run(Task task) {
        int lastCancelled = cancelled.get();
        executor.execute(() -> {
            try {
                long start = System.currentTimeMillis();
                if (task.delay > 0) {
                    synchronized (this) { wait(task.delay); }
                }
                int iteration = 0;
                long last = System.currentTimeMillis();
                while (true) {
                    if (cancelled.get() > lastCancelled)
                        break;
                    long time = System.currentTimeMillis();
                    Context ctx = new Context(time - start, time - last, iteration);
                    task.action.run(ctx);
                    if (ctx.cancelled || task.interval <= 0)
                        break;
                    ++iteration;
                    synchronized (this) { wait(task.interval); }
                    last = System.currentTimeMillis();
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        });
    }
}
