package com.gitlab.aecsocket.unifiedframework.core.parsing.logic.term;

import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicVisitor;

public abstract class StandardGate implements LogicExpressionNode {
    protected LogicExpressionNode a;
    protected LogicExpressionNode b;

    public StandardGate(LogicExpressionNode a, LogicExpressionNode b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public StandardGate accept(LogicVisitor visitor) {
        visitor.visit(this);
        a.accept(visitor);
        b.accept(visitor);
        return this;
    }
}
