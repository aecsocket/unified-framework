package com.gitlab.aecsocket.unifiedframework.core.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public interface Slot {
    //region Container
    <C extends Component> C get();

    default <C extends Component> Optional<C> getOpt() { return Optional.ofNullable(get()); }

    Slot set(@Nullable Component component);

    boolean isCompatible(@NotNull Component component);
    //endregion

    //region Navigation
    <C extends Component> SlotRef<C> parent();

    Slot parent(Component parent, String slotKey);

    default String[] path() {
        String[] parentPath = parent().component().path();
        String[] newPath = new String[parentPath.length + 1];
        System.arraycopy(parentPath, 0, newPath, 0, parentPath.length);
        newPath[newPath.length - 1] = parent().slotKey();
        return newPath;
    }
    //endregion
}
