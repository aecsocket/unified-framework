package com.gitlab.aecsocket.unifiedframework.core.scheduler;

import java.util.Iterator;

public interface Scheduler {
    void run(Task task);

    default void run(HasTasks tasks) { tasks.runTasks(this); }
    default void run(HasTasks... tasks) {
        for (HasTasks task : tasks) {
            run(task);
        }
    }
    default void run(Iterator<HasTasks> tasks) {
        while (tasks.hasNext()) {
            run(tasks.next());
        }
    }

    void cancel();
}
