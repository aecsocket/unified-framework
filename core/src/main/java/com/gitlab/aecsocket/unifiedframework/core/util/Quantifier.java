package com.gitlab.aecsocket.unifiedframework.core.util;

import java.util.Collection;
import java.util.Objects;

/**
 * Stores an object along with the amount of that object.
 * @param <T> The type of object.
 */
public class Quantifier<T> implements Cloneable {
    private T object;
    private int amount;

    public Quantifier(T object, int amount) {
        this.object = object;
        this.amount = amount;
    }

    public T get() { return object; }
    public void set(T object) { this.object = object; }

    public int getAmount() { return amount; }
    public void setAmount(int amount) { this.amount = amount; }

    public Quantifier<T> add(int amount) { this.amount += amount; return this; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Quantifier<?> that = (Quantifier<?>) o;
        return amount == that.amount && object.equals(that.object);
    }

    @Override
    public int hashCode() {
        return Objects.hash(object, amount);
    }

    @Override public String toString() { return object + " x" + amount; }

    @SuppressWarnings("unchecked")
    @Override public Quantifier<T> clone() { try { return (Quantifier<T>) super.clone(); } catch (CloneNotSupportedException e) { return null; } }

    public static <T> int amount(Collection<Quantifier<T>> collection) {
        int amount = 0;
        for (Quantifier<T> quantifier : collection)
            amount += quantifier.amount;
        return amount;
    }
}
