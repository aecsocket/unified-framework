package com.gitlab.aecsocket.unifiedframework.core.parsing.math;

import com.gitlab.aecsocket.unifiedframework.core.parsing.lexing.Definition;
import com.gitlab.aecsocket.unifiedframework.core.parsing.lexing.Lexer;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.node.Constant;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.node.Exponent;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.node.InbuiltFunction;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.node.Variable;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.term.Addition;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.term.Multiplication;
import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.AbstractEvaluator;
import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.IllegalTokenException;

/**
 * Grammar:
 * <pre>{@code <expression>    ::= <signed_term> <sum_op>}</pre>
 * <pre>{@code <sum_op>        ::= PLUS_MINUS <term> <sum_op> | ε}</pre>
 * <pre>{@code <signed_term>   ::= PLUS_MINUS <term> | <term>}</pre>
 * <pre>{@code <term>          ::= <factor> <term_op>}</pre>
 * <pre>{@code <term_op>       ::= MULTIPLY_DIVIDE <factor> <term_op> | ε}</pre>
 * <pre>{@code <signed_factor> ::= PLUS_MINUS <factor> | <factor>}</pre>
 * <pre>{@code <factor>        ::= <argument> <factor_op>}</pre>
 * <pre>{@code <factor_op>     ::= EXPONENT <expression> | ε}</pre>
 * <pre>{@code <argument>      ::= FUNCTION <argument> | OPEN_BRACKET <sum> CLOSE_BRACKET | <value>}</pre>
 * <pre>{@code <value>         ::= NUMBER | VARIABLE}</pre>
 */
public class MathParser extends AbstractEvaluator<MathExpressionNode> implements Lexer {
    public static final MathParser INSTANCE = new MathParser();
    public static final Definition[] DEFINITIONS = {
            Definition.of("\\(", 1),    Definition.of("\\)", 2),                                                 // structure
            Definition.of("\\^", 3),    Definition.of("sin|cos|tan|asin|acos|atan|abs|sqrt|exp|ln|log|log2", 4), // complex
            Definition.of("[+-]+", 5),  Definition.of("[*/]", 6),                                                // basic
            Definition.of("[0-9.]+", 7), Definition.of("[a-zA-Z][a-zA-Z0-9_]*", 8),                               // values
    };

    public static final int OPEN_BRACKET = 1;
    public static final int CLOSE_BRACKET = 2;
    public static final int EXPONENT = 3;
    public static final int FUNCTION = 4;
    public static final int PLUS_MINUS = 5;
    public static final int MULTIPLY_DIVIDE = 6;
    public static final int NUMBER = 7;
    public static final int VARIABLE = 8;

    public MathExpressionNode parse(String text) { return evaluate(tokens(text)); }

    @Override public Definition[] getDefinitions() { return DEFINITIONS; }

    @Override
    protected MathExpressionNode evaluate() {
        return expression();
    }

    /**
     * <pre>{@code <expression> ::= <signed_term> <sum_op>}</pre>
     * @return The expression.
     */
    private MathExpressionNode expression() {
        return seq(this::signedTerm, this::sumOp);
    }

    /**
     * <<pre>{@code <signed_term> ::= PLUS_MINUS <term> | <term>}</pre>
     * @param term <pre>{@code <term>}</pre>
     * @return The expression.
     */
    private MathExpressionNode sumOp(MathExpressionNode term) {
        return handle()
                .add(PLUS_MINUS, () -> {
                    Addition sum;
                    if (term.type() == MathExpressionNode.ADDITION)
                        sum = (Addition) term;
                    else
                        sum = new Addition().add(term, true);

                    boolean positive = lookahead.sequence().equals("+");
                    nextToken();
                    MathExpressionNode next = term();
                    sum.add(next, positive);
                    return sumOp(sum);
                })
                .epsilon(() -> term)
                .run(lookahead);
    }

    /**
     * <pre>{@code <signed_term> ::= PLUS_MINUS <term> | <term>}</pre>
     * @return The expression.
     */
    private MathExpressionNode signedTerm() {
        return handle()
                .add(PLUS_MINUS, () -> {
                    boolean positive = lookahead.sequence().equals("+");
                    nextToken();
                    MathExpressionNode next = term();
                    if (positive)
                        return next;
                    else
                        return new Addition().add(next, false);
                })
                .epsilon(this::term)
                .run(lookahead);
    }

    /**
     * <pre>{@code <term>          ::= <factor> <term_op>}</pre>
     * @return The expression.
     */
    private MathExpressionNode term() {
        return seq(this::factor, this::termOp);
    }

    private MathExpressionNode termOp(MathExpressionNode expression) {
        return handle()
                .add(MULTIPLY_DIVIDE, () -> {
                    Multiplication product;
                    if (expression.type() == MathExpressionNode.MULTIPLICATION)
                        product = (Multiplication) expression;
                    else
                        product = new Multiplication().add(expression, true);

                    boolean positive = lookahead.sequence().equals("*");
                    nextToken();
                    MathExpressionNode next = signedFactor();
                    product.add(next, positive);

                    return termOp(product);
                })
                .epsilon(() -> expression)
                .run(lookahead);
    }

    private MathExpressionNode signedFactor() {
        return handle()
                .add(PLUS_MINUS, () -> {
                    boolean positive = lookahead.sequence().equals("+");
                    nextToken();
                    MathExpressionNode next = factor();
                    if (positive)
                        return next;
                    else
                        return new Addition().add(next, false);
                })
                .epsilon(this::factor)
                .run(lookahead);
    }

    private MathExpressionNode factor() {
        return seq(this::argument, this::factorOp);
    }

    private MathExpressionNode factorOp(MathExpressionNode expression) {
        return handle()
                .add(EXPONENT, () -> {
                    nextToken();
                    MathExpressionNode exponent = signedFactor();
                    return new Exponent(expression, exponent);
                })
                .epsilon(() -> expression)
                .run(lookahead);
    }

    private MathExpressionNode argument() {
        return handle()
                .add(FUNCTION, () -> {
                    InbuiltFunction.Type function = InbuiltFunction.Type.from(lookahead.sequence());
                    nextToken();
                    MathExpressionNode next = argument();
                    return new InbuiltFunction(function, next);
                })
                .add(OPEN_BRACKET, () -> {
                    nextToken();
                    MathExpressionNode result = expression();
                    if (lookahead.id() != CLOSE_BRACKET)
                        throw new IllegalTokenException(lookahead.sequence(), "closing bracket");
                    nextToken();
                    return result;
                })
                .epsilon(this::value)
                .run(lookahead);
    }

    /**
     * <pre>{@code <value> ::= NUMBER | VARIABLE}</pre>
     * @return The expression.
     */
    private MathExpressionNode value() {
        return handle()
                .add(NUMBER, () -> {
                    MathExpressionNode result = new Constant(lookahead.sequence());
                    nextToken();
                    return result;
                })
                .add(VARIABLE, () -> {
                    MathExpressionNode result = new Variable(lookahead.sequence());
                    nextToken();
                    return result;
                })
                .run(lookahead, "number or variable");
    }

    public static MathExpressionNode parses(String text) { return INSTANCE.parse(text); }
}
