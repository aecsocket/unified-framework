package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.util.color.HSV;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class HSVSerializer implements TypeSerializer<HSV>, ConfigurateSerializer {
    public static final HSVSerializer INSTANCE = new HSVSerializer();

    @Override
    public void serialize(Type type, @Nullable HSV obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.setList(float.class, Arrays.asList(obj.h(), obj.s(), obj.v()));
        }
    }

    @Override
    public HSV deserialize(Type type, ConfigurationNode node) throws SerializationException {
        List<? extends ConfigurationNode> list = asList(node, type, "h", "s", "v");
        return HSV.of(
                (float) list.get(0).getDouble(), // TODO .getFloat() doesn't work on negatives (returns 0) ????????????????
                (float) list.get(1).getDouble(),
                (float) list.get(2).getDouble()
        );
    }
}
