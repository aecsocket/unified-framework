package com.gitlab.aecsocket.unifiedframework.core.parsing.math.term;

import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathVisitor;

public class Addition extends Sequence {
    @Override public Addition add(MathExpressionNode expression, boolean positive) { return (Addition) super.add(expression, positive); }

    @Override public int type() { return MathExpressionNode.ADDITION; }
    @Override public Addition accept(MathVisitor visitor) { return (Addition) super.accept(visitor); }
    @Override
    public double value() {
        double result = 0d;
        for (Term term : terms) {
            if (term.positive())
                result += term.expression().value();
            else
                result -= term.expression().value();
        }
        return result;
    }
}
