package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Locale;

public class LocaleSerializer implements TypeSerializer<Locale> {
    @Override
    public void serialize(Type type, @Nullable Locale obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.set(obj.toLanguageTag());
        }
    }

    @Override
    public Locale deserialize(Type type, ConfigurationNode node) throws SerializationException {
        String arg = node.getString();
        if (arg == null)
            throw new SerializationException(node, type, "Must be string of locale language tag");
        return Locale.forLanguageTag(arg);
    }
}
