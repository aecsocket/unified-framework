package com.gitlab.aecsocket.unifiedframework.core.parsing.math;

import com.gitlab.aecsocket.unifiedframework.core.parsing.math.node.Variable;

public class SetVariableVisitor implements MathVisitor {
    private final String name;
    private final double value;

    public SetVariableVisitor(String name, double value) {
        this.name = name;
        this.value = value;
    }

    public String name() { return name; }
    public double value() { return value; }

    @Override
    public void visit(MathExpressionNode node) {
        if (node instanceof Variable) {
            Variable var = (Variable) node;
            if (var.name().equals(name))
                var.value(value);
        }
    }
}
