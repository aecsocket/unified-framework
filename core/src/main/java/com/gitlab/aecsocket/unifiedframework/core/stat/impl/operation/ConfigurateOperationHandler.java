package com.gitlab.aecsocket.unifiedframework.core.stat.impl.operation;

import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.FunctionCreationException;
import com.gitlab.aecsocket.unifiedframework.core.util.data.Tuple;
import com.gitlab.aecsocket.unifiedframework.core.util.data.Tuple3;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;

import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Function;

public class ConfigurateOperationHandler<T> extends OperationHandler<T> {
    public ConfigurateOperationHandler() {}
    public ConfigurateOperationHandler(OperationHandler<T> o) { super(o); }

    public Tuple3<String, List<ConfigurationNode>, Operation<T>> getOpToken(ConfigurationNode node) {
        // Get the operation
        String opToken = null;
        List<ConfigurationNode> arr;
        if (node.isList()) {
            arr = new ArrayList<>(node.childrenList());
            if (arr.size() > 0) {
                opToken = arr.get(0).getString();
                arr.remove(0);
            }
        } else {
            arr = new ArrayList<>();
            arr.add(node);
        }

        if (opToken == null) {
            if (defaultOperation != null)
                opToken = defaultOperation;
            else
                throw new FunctionCreationException("Could not find valid operation");
        }

        Operation<T> operation = operations.get(opToken);
        if (operation == null)
            throw new FunctionCreationException("Operation " + opToken + " is invalid: must be [" + String.join(", ", operations.keySet()) + "]");

        return Tuple.of(opToken, arr, operation);
    }

    public Function<T, T> evaluate(ConfigurationNode node) throws FunctionCreationException {
        Tuple3<String, List<ConfigurationNode>, Operation<T>> tuple = getOpToken(node);
        List<ConfigurationNode> arr = tuple.b();
        Operation<T> operation = tuple.c();

        // Loading (may be expensive)
        Map<String, Object> passedArgs = new HashMap<>();
        if (operation.getLoadOperationFactory() != null) {
            operation.getLoadOperationFactory().apply(ConfigurationNode.class).run(new ArgumentProvider() {
                @Override
                public <A> A get(int i, Type type) throws FunctionCreationException {
                    if (i < arr.size()) {
                        try {
                            @SuppressWarnings("unchecked")
                            A arg = (A) arr.get(i).get(type);
                            if (arg != null)
                                return arg;
                        } catch (SerializationException e) {
                            throw new FunctionCreationException("Expected argument " + i + " of type " + type.getTypeName(), e);
                        }
                    }
                    throw new FunctionCreationException("Expected argument " + i + " of type " + type.getTypeName());
                }
            }, node, passedArgs);
        }
        Map<String, Object> immutablePassed = Collections.unmodifiableMap(passedArgs);

        // Running (should not be expensive)
        return base -> operation.getRunOperation().run(base, immutablePassed);
    }
}
