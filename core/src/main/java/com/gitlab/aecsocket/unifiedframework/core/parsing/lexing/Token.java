package com.gitlab.aecsocket.unifiedframework.core.parsing.lexing;

public final class Token {
    public static final Token EPSILON = new Token(0, "");

    private final int id;
    private final String sequence;

    public Token(int id, String sequence) {
        this.id = id;
        this.sequence = sequence;
    }

    public int id() { return id; }
    public String sequence() { return sequence; }
}
