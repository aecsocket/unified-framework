package com.gitlab.aecsocket.unifiedframework.core.parsing.math.node;

import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.EvaluationException;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathVisitor;

public class Variable implements MathExpressionNode {
    private final String name;
    private Double value;

    public Variable(String name, Double value) {
        this.name = name;
        this.value = value;
    }

    public Variable(String name) {
        this.name = name;
    }

    @Override public int type() { return MathExpressionNode.VARIABLE; }
    @Override public Variable accept(MathVisitor visitor) { visitor.visit(this); return this; }

    public String name() { return name; }

    public boolean initialized() { return value != null; }
    @Override
    public double value() {
        if (value == null)
            throw new EvaluationException("Variable '" + name + "' was not initialized");
        return value;
    }
    public void value(Double value) { this.value = value; }
}
