package com.gitlab.aecsocket.unifiedframework.core.registry;

public class LinkingException extends RuntimeException {
    public LinkingException() {}
    public LinkingException(String message) { super(message); }
    public LinkingException(String message, Throwable cause) { super(message, cause); }
    public LinkingException(Throwable cause) { super(cause); }
    public LinkingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) { super(message, cause, enableSuppression, writableStackTrace); }
}
