package com.gitlab.aecsocket.unifiedframework.core.parsing.math.node;

import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathVisitor;

public class Exponent implements MathExpressionNode {
    private final MathExpressionNode base;
    private final MathExpressionNode exponent;

    public Exponent(MathExpressionNode base, MathExpressionNode exponent) {
        this.base = base;
        this.exponent = exponent;
    }

    public MathExpressionNode base() { return base; }
    public MathExpressionNode exponent() { return exponent; }

    @Override public int type() { return MathExpressionNode.EXPONENT; }
    @Override public Exponent accept(MathVisitor visitor) {
        visitor.visit(this);
        base.accept(visitor);
        exponent.accept(visitor);
        return this;
    }

    @Override public double value() { return Math.pow(base.value(), exponent.value()); }
}
