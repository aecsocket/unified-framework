package com.gitlab.aecsocket.unifiedframework.core.parsing.math;

import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.NodeVisitor;

public interface MathVisitor extends NodeVisitor<MathExpressionNode> {}
