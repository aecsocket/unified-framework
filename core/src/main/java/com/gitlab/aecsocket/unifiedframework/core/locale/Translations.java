package com.gitlab.aecsocket.unifiedframework.core.locale;

import java.util.*;

public final class Translations {
    private final Locale locale;
    private final Map<String, String> values;

    public Translations(Locale locale, Map<String, String> values) {
        this.locale = locale;
        this.values = values;
    }

    public Translations(Locale locale) {
        this.locale = locale;
        values = new HashMap<>();
    }

    public Locale locale() { return locale; }
    public Map<String, String> values() { return new HashMap<>(values); }

    public boolean has(String key) { return values.containsKey(key); }
    public String get(String key) { return values.get(key); }
    public String set(String key, String... value) { return values.put(key, String.join("\n", value)); }
    public void add(Map<String, String> values) { this.values.putAll(values); }
    public Translations add(Translations translations) { add(translations.values); return this; }

    @Override public String toString() {
        StringJoiner result = new StringJoiner(", ");
        for (var entry : values.entrySet()) {
            result.add(entry.getKey() + "=" + entry.getValue());
        }
        return "(" + locale.toLanguageTag() + "){" + result.toString() + "}";
    }
}
