package com.gitlab.aecsocket.unifiedframework.core.stat.impl.descriptor;

import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2D;
import io.leangen.geantyref.TypeToken;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Vector2DDescriptor;

public class Vector2DDescriptorStat extends DescriptorStat<Vector2D, Vector2DDescriptor> {
    public Vector2DDescriptorStat(Vector2DDescriptor defaultValue) { super(defaultValue); }
    public Vector2DDescriptorStat(Vector2D defaultValue) { this(new Vector2DDescriptor(defaultValue)); }
    public Vector2DDescriptorStat() {}
    public Vector2DDescriptorStat(Vector2DDescriptorStat o) { super(o); }
    @Override public TypeToken<Vector2DDescriptor> valueType() { return new TypeToken<>(){}; }

    @Override
    protected Vector2DDescriptor apply(Vector2DDescriptor base, Vector2DDescriptor value) {
        return new Vector2DDescriptor(value.apply(base.apply()));
    }
}
