package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.vector;

import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.ConfigurateSerializer;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2D;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class Vector2DSerializer implements TypeSerializer<Vector2D>, ConfigurateSerializer {
    public static final Vector2DSerializer INSTANCE = new Vector2DSerializer();

    @Override
    public void serialize(Type type, @Nullable Vector2D obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            if (Double.compare(obj.x(), obj.y()) == 0)
                node.set(obj.x());
            else
                node.setList(Double.class, Arrays.asList(
                        obj.x(), obj.y()
                ));
        }
    }

    @Override
    public Vector2D deserialize(Type type, ConfigurationNode node) throws SerializationException {
        if (node.isList()) {
            List<? extends ConfigurationNode> list = asList(node, Vector2D.class, "x", "y");
            return new Vector2D(
                    list.get(0).getDouble(),
                    list.get(1).getDouble()
            );
        } else
            return new Vector2D(node.getDouble());
    }
}
