package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.descriptor;

import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Descriptor;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.NumberDescriptor;
import io.leangen.geantyref.TypeToken;

/**
 * Allows proper deserialization of a {@link NumberDescriptor}.
 */
public abstract class NumberDescriptorSerializer<N extends Number> extends DescriptorSerializer<N, NumberDescriptor<N>> {
    public static final class Byte extends NumberDescriptorSerializer<java.lang.Byte> {
        public static final Byte INSTANCE = new Byte();
        @Override public TypeToken<java.lang.Byte> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor<java.lang.Byte> create(java.lang.Byte value) { return NumberDescriptor.of(value); }
        @Override protected NumberDescriptor<java.lang.Byte> create(java.lang.Byte value, Descriptor.Operation operation, Descriptor.Suffix suffix) { return new NumberDescriptor.Byte(value, operation, suffix); }
    }

    public static final class Short extends NumberDescriptorSerializer<java.lang.Short> {
        public static final Short INSTANCE = new Short();
        @Override public TypeToken<java.lang.Short> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor<java.lang.Short> create(java.lang.Short value) { return NumberDescriptor.of(value); }
        @Override protected NumberDescriptor<java.lang.Short> create(java.lang.Short value, Descriptor.Operation operation, Descriptor.Suffix suffix) { return new NumberDescriptor.Short(value, operation, suffix); }
    }

    public static final class Integer extends NumberDescriptorSerializer<java.lang.Integer> {
        public static final Integer INSTANCE = new Integer();
        @Override public TypeToken<java.lang.Integer> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor<java.lang.Integer> create(java.lang.Integer value) { return NumberDescriptor.of(value); }
        @Override protected NumberDescriptor<java.lang.Integer> create(java.lang.Integer value, Descriptor.Operation operation, Descriptor.Suffix suffix) { return new NumberDescriptor.Integer(value, operation, suffix); }
    }

    public static final class Long extends NumberDescriptorSerializer<java.lang.Long> {
        public static final Long INSTANCE = new Long();
        @Override public TypeToken<java.lang.Long> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor<java.lang.Long> create(java.lang.Long value) { return NumberDescriptor.of(value); }
        @Override protected NumberDescriptor<java.lang.Long> create(java.lang.Long value, Descriptor.Operation operation, Descriptor.Suffix suffix) { return new NumberDescriptor.Long(value, operation, suffix); }
    }

    public static final class Float extends NumberDescriptorSerializer<java.lang.Float> {
        public static final Float INSTANCE = new Float();
        @Override public TypeToken<java.lang.Float> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor<java.lang.Float> create(java.lang.Float value) { return NumberDescriptor.of(value); }
        @Override protected NumberDescriptor<java.lang.Float> create(java.lang.Float value, Descriptor.Operation operation, Descriptor.Suffix suffix) { return new NumberDescriptor.Float(value, operation, suffix); }
    }

    public static final class Double extends NumberDescriptorSerializer<java.lang.Double> {
        public static final Double INSTANCE = new Double();
        @Override public TypeToken<java.lang.Double> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor<java.lang.Double> create(java.lang.Double value) { return NumberDescriptor.of(value); }
        @Override protected NumberDescriptor<java.lang.Double> create(java.lang.Double value, Descriptor.Operation operation, Descriptor.Suffix suffix) { return new NumberDescriptor.Double(value, operation, suffix); }
    }
}
