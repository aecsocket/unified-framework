package com.gitlab.aecsocket.unifiedframework.core.util.log;

import com.gitlab.aecsocket.unifiedframework.core.util.TextUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A wrapper around a {@link Logger} which prints out a colored prefix according to the {@link LogLevel}.
 */
public class LabelledLogger {
    private final Logger logger;
    private LogLevel level = LogLevel.INFO;

    public LabelledLogger(Logger logger, LogLevel level) {
        this.logger = logger;
        this.level = level;
    }

    public LabelledLogger(Logger logger) {
        this.logger = logger;
    }

    public LogLevel getLevel() { return level; }
    public void setLevel(LogLevel level) { this.level = level; }

    /**
     * If the provided log level is greater than or equal to this instance's log level, prints out some info through this instance's Logger.
     * <p>
     * This uses {@link #format(String, Object...)} to format the text, and prints text separated with <code>\n</code> as multiple lines.
     * @param level The {@link LogLevel} to check against.
     * @param text The text to log.
     * @param args The arguments used in the formatting.
     */
    public void log(LogLevel level, String text, Object... args) {
        rlog(level, args.length == 0 ? text : format(text, args));
    }

    /**
     * If the provided log level is greater than or equal to this instance's log level, prints out some info through this instance's Logger.
     * <p>
     * This prints text separated with <code>\n</code> as multiple lines.
     * @param level The {@link LogLevel} to check against.
     * @param text The text to log.
     */
    public void rlog(LogLevel level, String text) {
        if (level.level() < this.level.level()) return;
        for (String line : text.split("\n"))
            logger.log(Level.INFO, level.generatePrefix() + " " + line + "\033[0m");
    }

    public void logBasic(LogLevel level, Throwable e, String text, Object... args) {
        rlog(level, infoBasic(e, text, args));
    }

    public void logDetail(LogLevel level, Throwable e, String text, Object... args) {
        rlog(level, infoDetail(e, text, args));
    }

    /**
     * Formats text using {@link String#format(String, Object...)}.
     * @param text The text.
     * @param args The arguments.
     * @return The formatted text.
     */
    public static String format(String text, Object... args) { return String.format(text, args); }

    public static String infoBasic(Throwable e, String text, Object... args) {
        if (e != null && e.getMessage() != null)
            return format(text, args) + ": " + TextUtils.combineMessages(e);
        else
            return format(text, args);
    }

    public static String infoDetail(Throwable e, String text, Object... args) {
        if (e != null)
            return format(text, args) + "\n" + TextUtils.getStackTrace(e);
        else
            return format(text, args);
    }
}
