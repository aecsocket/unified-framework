package com.gitlab.aecsocket.unifiedframework.core.stat.impl.operation;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class OperationHandler<T> {
    protected String defaultOperation;
    protected Map<String, Operation<T>> operations = new HashMap<>();

    public OperationHandler() {}

    public OperationHandler(OperationHandler<T> o) {
        defaultOperation = o.defaultOperation;
        operations = new HashMap<>(o.operations);
    }

    public OperationHandler<T> defaultOperation(String defaultOperation) { this.defaultOperation = defaultOperation; return this; }
    public String defaultOperation() { return defaultOperation; }

    public OperationHandler<T> operation(String token, Function<Class<?>, LoadOperation> loadOperationFactory, RunOperation<T> runOperation) {
        Operation<T> operation = new Operation<>(loadOperationFactory, runOperation);
        operations.put(token, operation);
        return this;
    }
    public Map<String, Operation<T>> operations() { return operations; }

    public Function<T, T> getFunction(String token, Map<String, Object> passedArgs) {
        Operation<T> op = operations.get(token);
        return op == null ? null : op.getFunction(passedArgs);
    }
}
