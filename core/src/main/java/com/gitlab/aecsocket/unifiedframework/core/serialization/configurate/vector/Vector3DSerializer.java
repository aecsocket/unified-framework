package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.vector;

import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.ConfigurateSerializer;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class Vector3DSerializer implements TypeSerializer<Vector3D>, ConfigurateSerializer {
    public static final Vector3DSerializer INSTANCE = new Vector3DSerializer();

    @Override
    public void serialize(Type type, @Nullable Vector3D obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            if ((Double.compare(obj.x(), obj.y()) == 0) && (Double.compare(obj.x(), obj.z()) == 0))
                node.set(obj.x());
            else
                node.setList(Double.class, Arrays.asList(
                        obj.x(), obj.y(), obj.z()
                ));
        }
    }

    @Override
    public Vector3D deserialize(Type type, ConfigurationNode node) throws SerializationException {
        if (node.isList()) {
            List<? extends ConfigurationNode> list = asList(node, Vector3D.class, "x", "y", "z");
            return new Vector3D(
                    list.get(0).getDouble(),
                    list.get(1).getDouble(),
                    list.get(2).getDouble()
            );
        } else
            return new Vector3D(node.getDouble());
    }
}
