package com.gitlab.aecsocket.unifiedframework.core.util;

import java.util.Map;

public class MapInit<K, V, T extends Map<K, V>> {
    private final T map;
    private MapInit(T map) { this.map = map; }
    public MapInit<K, V, T> init(K key, V value) { map.put(key, value); return this; }
    public MapInit<K, V, T> init(Map<K, V> map) { this.map.putAll(map); return this; }
    public T get() { return map; }

    public static <K, V, T extends Map<K, V>> MapInit<K, V, T> of(T map) { return new MapInit<>(map); }
}
