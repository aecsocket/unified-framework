package com.gitlab.aecsocket.unifiedframework.core.registry;

import com.gitlab.aecsocket.unifiedframework.core.util.GraphUtils;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.MutableGraph;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * A collection of saved {@link Identifiable}s as {@link Ref}s.
 * <p>
 * These are stored as Refs instead of the raw {@link T}s so that
 * if an Identifiable with the same ID as an existing one is registered again,
 * all instances will be updated as well.
 * @param <T> The Identifiable type.
 */
@SuppressWarnings("UnstableApiUsage")
public class Registry<T extends Identifiable> {
    public static class LinkingEntry<T extends Identifiable> {
        private final Ref<T> root;
        private final Ref<T> target;
        private final LinkingException exception;

        public LinkingEntry(Ref<T> root, Ref<T> target, LinkingException exception) {
            this.root = root;
            this.target = target;
            this.exception = exception;
        }

        public Ref<T> root() { return root; }
        public Ref<T> target() { return target; }
        public LinkingException exception() { return exception; }

        public void apply(Runnable ifSuccessful, Consumer<LinkingException> ifFailure) {
            if (exception == null)
                ifSuccessful.run();
            else
                ifFailure.accept(exception);
        }

        public static <T extends Identifiable> LinkingEntry<T> of(Ref<T> root, Ref<T> target, LinkingException exception) {
            return new LinkingEntry<>(root, target, exception);
        }

        public static <T extends Identifiable> LinkingEntry<T> of(Ref<T> root, Ref<T> target) {
            return of(root, target, null);
        }
    }

    public static class ResolutionEntry<T extends Identifiable> {
        private final Ref<T> object;
        private final ResolutionException exception;

        public ResolutionEntry(Ref<T> object, ResolutionException exception) {
            this.object = object;
            this.exception = exception;
        }

        public Ref<T> object() { return object; }
        public ResolutionException exception() { return exception; }

        public void apply(Consumer<Ref<T>> ifSuccessful, BiConsumer<Ref<T>, ResolutionException> ifFailure) {
            if (exception == null)
                ifSuccessful.accept(object);
            else
                ifFailure.accept(object, exception);
        }

        public static <T extends Identifiable> ResolutionEntry<T> of(Ref<T> object, ResolutionException exception) {
            return new ResolutionEntry<>(object, exception);
        }

        public static <T extends Identifiable> ResolutionEntry<T> of(Ref<T> object) {
            return of(object, null);
        }
    }

    private final HashMap<String, Ref<T>> registry = new HashMap<>();
    private MutableGraph<Ref<T>> dependencyGraph;

    public HashMap<String, Ref<T>> getRegistry() { return new HashMap<>(registry); }
    public MutableGraph<Ref<T>> getDependencyGraph() { return dependencyGraph; }

    //region Registration

    /**
     * Gets the Ref wrapper at the specified key.
     * @param key The key.
     * @return The Ref.
     */
    public Ref<T> getRef(String key) { return registry.get(key); }

    /**
     * Gets the {@link T} from the Ref wrapper at the specified key.
     * @param key The key.
     * @return The {@link T}.
     */
    public T get(String key) { return registry.containsKey(key) ? getRef(key).get() : null; }

    /**
     * Gets the {@link E} from the Ref wrapper at the specified key.
     * @param key The key.
     * @param type The type of {@link T}.
     * @param <E> The type of {@link T}.
     * @return The {@link E}.
     */
    public <E extends T> E get(String key, Class<E> type) {
        T result = get(key);
        return result == null ? null : type.isInstance(result) ? type.cast(result) : null;
    }

    /**
     * Checks if this instance has the specified key.
     * @param key The key.
     * @return The result.
     */
    public boolean has(String key) { return registry.containsKey(key); }

    /**
     * Checks if this instance has the specified key and the value is of the specified type.
     * @param key The key.
     * @param type The type of {@link T}.
     * @param <E> The type of {@link T}.
     * @return The result.
     */
    public <E extends T> boolean has(String key, Class<E> type) {
        return registry.containsKey(key) && type.isInstance(registry.get(key));
    }

    /**
     * Validates and registers a {@link T} into the registry.
     * <p>
     * If an Identifiable with the same ID as an existing one is registered, the internal Ref's Identifiable
     * will be updated instead. Otherwise, a new Ref will be registered.
     * @param object The {@link T}.
     * @return The {@link Ref} modified.
     * @throws ValidationException If the Identifiable failed {@link Identifiable#validate()}.
     */
    public Ref<T> register(T object) throws ValidationException {
        String id = object.id();
        object.validate();
        Ref<T> ref;
        if (registry.containsKey(id)) {
            ref = registry.get(id);
            ref.set(object);
        } else {
            ref = new Ref<>(object);
            registry.put(id, ref);
        }
        return ref;
    }

    /**
     * Unregisters a {@link T} from the registry with the specified key.
     * @param id The ID to unregister by.
     * @return The previously registered {@link T}.
     */
    public T unregister(String id) {
        Ref<T> ref = registry.remove(id);
        return ref == null ? null : ref.get();
    }

    /**
     * Unregisters the specified {@link T} from the registry.
     * @param object The Identifiable to unregister.
     * @return The previously registered Identifiable.
     */
    public T unregister(T object) { return unregister(object.id()); }

    /**
     * Unregisters all registered {@link Ref}s (and consequently {@link T}s).
     */
    public void unregisterAll() { registry.clear(); }

    //endregion

    //region Resolution

    /**
     * Creates a full {@link MutableGraph} of the resolution dependencies of all objects. This method modifies this
     * instance's dependency graph field.
     * <p>
     * The graph is directed and acyclic. If a cyclical dependency is found, it will throw an exception.
     * @return The result.
     */
    public List<LinkingEntry<T>> link() {
        List<LinkingEntry<T>> result = new ArrayList<>();
        dependencyGraph = GraphBuilder.directed()
                .allowsSelfLoops(false)
                .expectedNodeCount(registry.size())
                .build();
        // Make nodes
        registry.values().forEach(dependencyGraph::addNode);
        // Make connections
        registry.values().forEach(ref -> {
            for (String dependency : ref.get().dependencies()) {
                if (has(dependency)) {
                    Ref<T> depRef = getRef(dependency);
                    try {
                        dependencyGraph.putEdge(depRef, ref);
                        GraphUtils.topologicallySortedNodes(dependencyGraph).forEach(__ -> {}); // check for cycles
                        result.add(LinkingEntry.of(ref, depRef));
                    } catch (IllegalStateException e) {
                        result.add(LinkingEntry.of(ref, depRef, new LinkingException(String.format("Cyclical dependencies between %s and %s", ref.getId(), dependency))));
                        // The nodes with cyclical dependencies will not be resolved at all
                        dependencyGraph.removeNode(depRef);
                        dependencyGraph.removeNode(ref);
                    }
                }
            }
        });
        return result;
    }

    /**
     * Resolves all registered objects, topologically sorted according to the dependency graph. This method
     * does not call {@link Registry#link()} beforehand, so you must do that manually beforehand.
     * @return The result.
     */
    public List<ResolutionEntry<T>> resolve() {
        List<ResolutionEntry<T>> result = new ArrayList<>();
        Set<Ref<T>> sorted = GraphUtils.topologicallySortedNodes(dependencyGraph);
        sorted.forEach(ref -> {
            Map<String, Identifiable> dependencies = new HashMap<>();
            dependencyGraph.predecessors(ref).forEach(ref2 -> dependencies.put(ref2.getId(), ref2.get()));
            try {
                ref.get().resolve(dependencies::get);
                result.add(ResolutionEntry.of(ref));
            } catch (ResolutionException e) {
                result.add(ResolutionEntry.of(ref, e));
                dependencyGraph.removeNode(ref);
            } catch (Exception e) {
                result.add(ResolutionEntry.of(ref, new ResolutionException(e)));
                dependencyGraph.removeNode(ref);
            }
        });
        return result;
    }

    public void removeUnlinked() {
        registry.entrySet().removeIf(entry -> !dependencyGraph.nodes().contains(entry.getValue()));
    }

    //endregion
}
