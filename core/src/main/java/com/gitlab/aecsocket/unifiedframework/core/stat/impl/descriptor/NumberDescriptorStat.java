package com.gitlab.aecsocket.unifiedframework.core.stat.impl.descriptor;

import io.leangen.geantyref.TypeToken;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.NumberDescriptor;

public abstract class NumberDescriptorStat<V extends Number, T extends NumberDescriptor<V>> extends DescriptorStat<V, T> {
    public NumberDescriptorStat(T defaultValue) { super(defaultValue); }
    public NumberDescriptorStat() {}
    public NumberDescriptorStat(NumberDescriptorStat<V, T> o) { super(o); }

    protected abstract T create(V value);

    @Override
    protected T apply(T base, T value) {
        return create(value.apply(base.apply()));
    }

    public static class Byte extends NumberDescriptorStat<java.lang.Byte, NumberDescriptor.Byte> {
        public Byte(NumberDescriptor.Byte defaultValue) { super(defaultValue); }
        public Byte(byte defaultValue) { this(NumberDescriptor.of(defaultValue)); }
        public Byte() {}
        public Byte(NumberDescriptorStat<java.lang.Byte, NumberDescriptor.Byte> o) { super(o); }
        @Override public TypeToken<NumberDescriptor.Byte> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor.Byte create(java.lang.Byte value) { return NumberDescriptor.of(value); }
    }

    public static class Short extends NumberDescriptorStat<java.lang.Short, NumberDescriptor.Short> {
        public Short(NumberDescriptor.Short defaultValue) { super(defaultValue); }
        public Short(short defaultValue) { this(NumberDescriptor.of(defaultValue)); }
        public Short() {}
        public Short(NumberDescriptorStat<java.lang.Short, NumberDescriptor.Short> o) { super(o); }
        @Override public TypeToken<NumberDescriptor.Short> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor.Short create(java.lang.Short value) { return NumberDescriptor.of(value); }
    }

    public static class Integer extends NumberDescriptorStat<java.lang.Integer, NumberDescriptor.Integer> {
        public Integer(NumberDescriptor.Integer defaultValue) { super(defaultValue); }
        public Integer(int defaultValue) { this(NumberDescriptor.of(defaultValue)); }
        public Integer() {}
        public Integer(NumberDescriptorStat<java.lang.Integer, NumberDescriptor.Integer> o) { super(o); }
        @Override public TypeToken<NumberDescriptor.Integer> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor.Integer create(java.lang.Integer value) { return NumberDescriptor.of(value); }
    }

    public static class Long extends NumberDescriptorStat<java.lang.Long, NumberDescriptor.Long> {
        public Long(NumberDescriptor.Long defaultValue) { super(defaultValue); }
        public Long(long defaultValue) { this(NumberDescriptor.of(defaultValue)); }
        public Long() {}
        public Long(NumberDescriptorStat<java.lang.Long, NumberDescriptor.Long> o) { super(o); }
        @Override public TypeToken<NumberDescriptor.Long> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor.Long create(java.lang.Long value) { return NumberDescriptor.of(value); }
    }

    public static class Float extends NumberDescriptorStat<java.lang.Float, NumberDescriptor.Float> {
        public Float(NumberDescriptor.Float defaultValue) { super(defaultValue); }
        public Float(float defaultValue) { this(NumberDescriptor.of(defaultValue)); }
        public Float() {}
        public Float(NumberDescriptorStat<java.lang.Float, NumberDescriptor.Float> o) { super(o); }
        @Override public TypeToken<NumberDescriptor.Float> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor.Float create(java.lang.Float value) { return NumberDescriptor.of(value); }
    }

    public static class Double extends NumberDescriptorStat<java.lang.Double, NumberDescriptor.Double> {
        public Double(NumberDescriptor.Double defaultValue) { super(defaultValue); }
        public Double(double defaultValue) { this(NumberDescriptor.of(defaultValue)); }
        public Double() {}
        public Double(NumberDescriptorStat<java.lang.Double, NumberDescriptor.Double> o) { super(o); }
        @Override public TypeToken<NumberDescriptor.Double> valueType() { return new TypeToken<>(){}; }
        @Override protected NumberDescriptor.Double create(java.lang.Double value) { return NumberDescriptor.of(value); }
    }

    public static Integer of(int value) { return new Integer(value); }
    public static Long of(long value) { return new Long(value); }
    public static Float of(float value) { return new Float(value); }
    public static Double of(double value) { return new Double(value); }
    public static Byte of(byte value) { return new Byte(value); }
    public static Short of(short value) { return new Short(value); }
}
