package com.gitlab.aecsocket.unifiedframework.core.util.color;

public class ColorModifier {
    private final RGBA rgba;
    private final HSV hsv;

    public ColorModifier(RGBA value) {
        rgba = value;
        hsv = null;
    }

    public ColorModifier(HSV value) {
        rgba = null;
        hsv = value;
    }

    public RGBA rgba() { return rgba; }
    public HSV hsv() { return hsv; }
    public Object value() { return rgba == null ? hsv : rgba; }

    public RGBA combine(RGBA base) {
        if (rgba != null)
            return base.lerp(rgba);
        if (hsv != null)
            return base.hsv().add(hsv).rgba(base.a());
        return null;
    }

    @Override public String toString() { return value().toString(); }

    public static ColorModifier wrap(Object value) {
        if (value instanceof RGBA)
            return new ColorModifier((RGBA) value);
        if (value instanceof HSV)
            return new ColorModifier((HSV) value);
        throw new IllegalArgumentException("Object must be RGBA or HSV");
    }
}
