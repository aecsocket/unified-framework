package com.gitlab.aecsocket.unifiedframework.core.parsing.eval;

public class IllegalTokenException extends EvaluationException {
    public IllegalTokenException(String found, String expected) {
        super("Found '" + found + "', expected " + expected);
    }
}
