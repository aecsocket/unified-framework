package com.gitlab.aecsocket.unifiedframework.core.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.StringJoiner;

/** Methods for manipulating text. */
public final class TextUtils {

    private TextUtils() {}

    /**
     * Prepends a prefix to multiple lines of text.
     * @param lines The lines of text.
     * @param prefix The prefix.
     * @return The modified array of lines.
     */
    public static String[] prefixLines(String[] lines, String prefix) {
        for (int i = 0; i < lines.length; i++)
            lines[i] = prefix + lines[i];
        return lines;
    }

    /**
     * Joins an array of lines into one string separated by <code>\n</code>.
     * @param lines The array of lines.
     * @return The joined string,
     */
    public static String joinLines(String[] lines) {
        StringJoiner result = new StringJoiner("\n");
        for (String line : lines)
            result.add(line);
        return result.toString();
    }

    /**
     * Adapts the stack trace of a Throwable for a Minecraft client's chat.
     * @param throwable The Throwable.
     * @param indent The amount of spaces to indent with.
     * @return The modified stack trace.
     */
    public static String getStackTrace(Throwable throwable, int indent) {
        StringWriter writer = new StringWriter();
        throwable.printStackTrace(new PrintWriter(writer));
        return writer.toString().replace("\t", " ".repeat(indent));
    }

    /**
     * Adapts the stack trace of a Throwable for a Minecraft client's chat.
     * Uses a default of 4 spaces for indentation.
     * @param throwable The Throwable.
     * @return The modified stack trace.
     */
    public static String getStackTrace(Throwable throwable) {
        return getStackTrace(throwable, 4);
    }

    /**
     * Splits a string into its lines, using {@code \n} as the separator.
     * @param text The text.
     * @return The lines.
     */
    public static String[] lines(String text) {
        return text.split("\n");
    }

    /**
     * Recursively combines the messages of exceptions, ignoring null messages.
     * @param current The top level of the exceptions.
     * @param joiner The string to join with.
     * @return The combined messages.
     */
    public static String combineMessages(Throwable current, String joiner) {
        StringJoiner result = new StringJoiner(joiner);
        while (current != null) {
            if (current.getMessage() != null)
                result.add(current.getMessage());
            current = current.getCause();
        }
        return result.toString();
    }

    /**
     * Recursively combines the messages of exceptions, ignoring null messages.
     * <p>
     * Uses the default join string of `{@code : }`
     * @param current The top level of the exceptions.
     * @return The combined messages.
     */
    public static String combineMessages(Throwable current) { return combineMessages(current, ": "); }
}
