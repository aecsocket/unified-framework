package com.gitlab.aecsocket.unifiedframework.core.util.color;

import org.jetbrains.annotations.Range;

public interface HSV {
    float h();
    float s();
    float v();

    default HSV add(float h, float s, float v) { return HSV.of(h() + h, s() + s, v() + v); }
    default HSV add(HSV o) { return add(o.h(), o.s(), o.v()); }

    default RGBA rgba(float a) { return RGBA.ofHSV(h(), s(), v(), a); }


    static HSV of(float h, float s, float v) { return new HSVImpl(h, s, v); }

    static HSV ofRGB(
            @Range(from = 0x00, to = 0xff) int ir,
            @Range(from = 0x00, to = 0xff) int ig,
            @Range(from = 0x00, to = 0xff) int ib
    ) {
        float r = ir / 255f;
        float g = ig / 255f;
        float b = ib / 255f;

        float min = Math.min(r, Math.min(g, b));
        float max = Math.max(r, Math.max(g, b));
        float delta = max - min;

        float s;
        if (max != 0)
            s = delta / max;
        else
            s = 0;
        if (s == 0)
            return of(0, s, max);

        float h;
        if (r == max)
            h = (g - b) / delta;
        else if (g == max)
            h = 2 + (b - r) / delta;
        else
            h = 4 + (r - g) / delta;
        h *= 60;
        if (h < 0)
            h += 360;

        return of(h / 360, s, max);
    }
}
