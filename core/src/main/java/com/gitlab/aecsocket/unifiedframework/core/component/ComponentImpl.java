package com.gitlab.aecsocket.unifiedframework.core.component;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class ComponentImpl implements Component {
    private final Map<String, SlotImpl> slots;
    private Slot parent;

    public ComponentImpl(Map<String, SlotImpl> slots) {
        this.slots = slots;
        slots.forEach((key, slot) -> slot.parent(this, key));
    }

    @Override @SuppressWarnings("unchecked") public @NotNull <S extends Slot> Map<String, S> slots() { return (Map<String, S>) slots; }
    @Override @SuppressWarnings("unchecked") public <S extends Slot> S slot(String key) { return (S) slots.get(key); }
    public ComponentImpl slot(String key, SlotImpl slot) {
        SlotImpl old = slots.put(key, slot.parent(this, key));
        if (old != null)
            old.parent(null, null);
        return this;
    }

    @Override @SuppressWarnings("unchecked") public <S extends Slot> S parent() { return (S) parent; }
    @Override public ComponentImpl parent(Slot parent) { this.parent = parent; return this; }

    @Override
    public String toString() { return slots.toString(); }
}
