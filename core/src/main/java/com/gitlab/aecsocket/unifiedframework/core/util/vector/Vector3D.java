package com.gitlab.aecsocket.unifiedframework.core.util.vector;

import com.gitlab.aecsocket.unifiedframework.core.util.Utils;

import java.util.Objects;

/**
 * Immutable data class holding an x, y, z decimal vector.
 */
public class Vector3D {
    public static final double EPSILON = 1e-6D;
    public static final Vector3D ZERO = new Vector3D();

    private final double x;
    private final double y;
    private final double z;

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3D(double v) { this(v, v, v); }

    public Vector3D() { this(0, 0, 0); }

    public double x() { return x; }
    public Vector3D x(double x) { return new Vector3D(x, y, z); }

    public double y() { return y; }
    public Vector3D y(double y) { return new Vector3D(x, y, z); }

    public double z() { return z; }
    public Vector3D z(double z) { return new Vector3D(x, y, z); }

    public Vector3D add(double x, double y, double z) { return new Vector3D(this.x + x, this.y + y, this.z + z); }
    public Vector3D add(Vector3D o) { return add(o.x, o.y, o.z); }
    public Vector3D add(double v) { return add(v, v, v); }

    public Vector3D subtract(double x, double y, double z) { return add(-x, -y, -z); }
    public Vector3D subtract(Vector3D o) { return subtract(o.x, o.y, o.z); }
    public Vector3D subtract(double v) { return subtract(v, v, v); }

    public Vector3D multiply(double x, double y, double z) { return new Vector3D(this.x * x, this.y * y, this.z * z); }
    public Vector3D multiply(Vector3D o) { return multiply(o.x, o.y, o.z); }
    public Vector3D multiply(double v) { return multiply(v, v, v); }

    public Vector3D divide(double x, double y, double z) { return new Vector3D(this.x / x, this.y / y, this.z / z); }
    public Vector3D divide(Vector3D o) { return divide(o.x, o.y, o.y); }
    public Vector3D divide(double v) { return divide(v, v, v); }

    public Vector3D rotateX(double angle) {
        double cos = Math.cos(angle);
        double sin = Math.sin(angle);
        return new Vector3D(
                x,
                cos * y - sin * z,
                sin * y + cos * z);
    }

    public Vector3D rotateY(double angle) {
        double cos = Math.cos(angle);
        double sin = Math.sin(angle);
        return new Vector3D(
                cos * x + sin * z,
                y,
                -sin * x + cos * z);
    }

    public Vector3D rotateZ(double angle) {
        double cos = Math.cos(angle);
        double sin = Math.sin(angle);
        return new Vector3D(
                cos * x - sin * y,
                sin * x + cos * y,
                z
        );
    }

    public boolean normalized() {
        return Math.abs(this.manhattanLength() - 1) < EPSILON;
    }

    public Vector3D rotate(Vector3D axis, double angle) {
        return this.rotateNonUnit(axis.normalized() ? axis : axis.normalize(), angle);
    }

    public Vector3D rotateNonUnit(Vector3D axis, double angle) {
        double x2 = axis.x;
        double y2 = axis.y;
        double z2 = axis.z;
        double cosTheta = Math.cos(angle);
        double sinTheta = Math.sin(angle);
        double dotProduct = this.dot(axis);
        double xPrime = x2 * dotProduct * (1 - cosTheta) + x * cosTheta + (-z2 * y + y2 * z) * sinTheta;
        double yPrime = y2 * dotProduct * (1 - cosTheta) + y * cosTheta + (z2 * x - x2 * z) * sinTheta;
        double zPrime = z2 * dotProduct * (1 - cosTheta) + z * cosTheta + (-y2 * x + x2 * y) * sinTheta;
        return new Vector3D(xPrime, yPrime, zPrime);
    }

    /**
     * Gets <code>abs(x) + abs(y) + abs(z)</code>.
     * @return The result.
     */
    public double manhattanLength() { return Math.abs(x) + Math.abs(y) + Math.abs(z); }

    /**
     * Gets <code>sqrt((x*x) + (y*y))</code>.
     * @return The result.
     */
    public double length() { return Math.sqrt((x*x) + (y*y) + (z*z)); }

    public Vector3D normalize() {
        double length = length();
        return new Vector3D(x / length, y / length, z / length);
    }

    public Vector3D crossProduct(Vector3D o) {
        return new Vector3D(
                y * o.z - o.y * z,
                z * o.x - o.z * x,
                x * o.y - o.x * y
        );
    }

    public double dot(Vector3D o) { return (x*o.x) + (y*o.y) + (z*o.z); }

    /**
     * Gets the result of <code>this - (o * ((this . v2) * 2))</code>.
     * @param o The other vector.
     * @return The reflected vector.
     */
    public Vector3D deflect(Vector3D o) { return subtract(o.multiply(dot(o) * 2)); }

    /**
     * Gets the angle between this vector and another.
     * @param o The other vector.
     * @return The angle.
     */
    public double angle(Vector3D o) {
        double dot = Utils.clamp(dot(o) / (length() * o.length()), -1, 1);
        return Math.acos(dot);
    }

    /**
     * Gets the squared distance between this vector and another.
     * @param o The other vector.
     * @return The distance.
     */
    public double manhattanDistance(Vector3D o) {
        return Utils.square(x - o.x) + Utils.square(y - o.y) + Utils.square(z - o.z);
    }

    /**
     * Gets the actual distance between this vector and another. This is expensive to compute.
     * @param o The other vector.
     * @return The distance.
     */
    public double distance(Vector3D o) {
        return Math.sqrt(manhattanDistance(o));
    }

    public Vector3I toInts() { return new Vector3I((int) x, (int) y, (int) z); }

    public ViewCoordinates toViewCoordinates() {
        double pi = Math.PI;
        double _2pi = pi * 2;
        if (x == 0 && z == 0)
            return new ViewCoordinates(0, y > 0 ? pi : -pi);
        else {
            double theta = Math.atan2(-x, z);
            double xz = Math.sqrt(x*x + z*z);
            return new ViewCoordinates(
                    (theta + _2pi) % _2pi,
                    Math.atan(-y / xz)
            );
        }
    }

    @Override public String toString() { return "(" + x + ", " + y + ", " + z + ")"; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector3D vector3D = (Vector3D) o;
        return Double.compare(vector3D.x, x) == 0 && Double.compare(vector3D.y, y) == 0 && Double.compare(vector3D.z, z) == 0;
    }

    @Override
    public int hashCode() { return Objects.hash(x, y, z); }
}
