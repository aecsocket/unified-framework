package com.gitlab.aecsocket.unifiedframework.core.parsing.logic.node;

import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.EvaluationException;
import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicVisitor;

public class Variable implements LogicExpressionNode {
    private final String name;
    private Boolean value;

    public Variable(String name, Boolean value) {
        this.name = name;
        this.value = value;
    }

    public Variable(String name) {
        this.name = name;
    }

    @Override public int type() { return LogicExpressionNode.VARIABLE; }
    @Override public Variable accept(LogicVisitor visitor) { visitor.visit(this); return this; }

    public String name() { return name; }

    public boolean initialized() { return value != null; }
    @Override
    public boolean value() {
        if (value == null)
            throw new EvaluationException("Variable '" + name + "' was not initialized");
        return value;
    }
    public void value(Boolean value) { this.value = value; }
}
