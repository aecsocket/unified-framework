package com.gitlab.aecsocket.unifiedframework.core.stat.impl;

import com.gitlab.aecsocket.unifiedframework.core.stat.AbstractStat;
import io.leangen.geantyref.TypeToken;
import com.gitlab.aecsocket.unifiedframework.core.stat.impl.operation.ConfigurateOperationHandler;
import com.gitlab.aecsocket.unifiedframework.core.stat.impl.operation.LoadOperation;
import com.gitlab.aecsocket.unifiedframework.core.stat.impl.operation.OperationHandler;
import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.ConfigurateStat;
import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.FunctionCreationException;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * A stat which holds an array of type {@link E}. Operations:
 * <ul>
 *     <li>{@code =} (default): = v</li>
 *     <li>{@code +}: b + v</li>
 * </ul>
 * @param <E> The array type.
 */
public abstract class ArrayStat<E> extends AbstractStat<E[]> implements ConfigurateStat<E[]> {
    private E[] result(E[] b, Map<String, Object> args, BiFunction<E[], E[], E[]> func) {
        @SuppressWarnings("unchecked")
        E[] result = (E[]) args.get("v");
        return func.apply(b, result);
    }

    public abstract TypeToken<E> arrayValueType();
    @Override public TypeToken<E[]> valueType() { return new TypeToken<>(){}; }

    private final Function<Class<?>, LoadOperation> loadOperationFactory = c -> {
            if (c == ConfigurationNode.class) {
                return (args, r, passed) -> {
                    ConfigurationNode ctx = (ConfigurationNode) r;
                    try {
                        List<?> list = ctx.getList(arrayValueType());
                        if (list == null)
                            throw new FunctionCreationException("Could not create list");
                        passed.put("v", list.toArray(newArray(0)));
                    } catch (SerializationException e) {
                        throw new FunctionCreationException(e);
                    }
                };
            }
            throw new FunctionCreationException("Load context type " + c + " is not supported");
        };
    private final OperationHandler<E[]> opHandler = new OperationHandler<E[]>()
            .defaultOperation("=")

            .operation("=", loadOperationFactory, (x, y) -> result(x, y, (b, v) -> v))
            .operation("+", loadOperationFactory, (x, y) -> result(x, y, (b, v) -> {
                E[] result = newArray(b.length + v.length);
                System.arraycopy(b, 0, result, 0, b.length);
                System.arraycopy(v, 0, result, b.length, v.length);
                return result;
            }))
            .operation("-", c -> (args, ctx, passed) -> passed.put("v", args.get(0, int.class)), (b, args) -> {
                int len = b.length - Math.min((int) args.get("v"), b.length);
                E[] result = newArray(len);
                System.arraycopy(b, 0, result, 0, len);
                return result;
            });
    private final ConfigurateOperationHandler<E[]> configOpHandler = new ConfigurateOperationHandler<>(opHandler);

    public ArrayStat(E[] defaultValue) { super(defaultValue); }
    public ArrayStat() {}
    public ArrayStat(ArrayStat<E> o) { super(o); }

    @Override public E[] preModify(E[] value) { return value == null ? newArray(0) : value; }

    protected abstract E[] newArray(int length);

    @Override
    public Function<E[], E[]> getModFunction(ConfigurationNode node) throws FunctionCreationException {
        return configOpHandler.evaluate(node);
    }
}
