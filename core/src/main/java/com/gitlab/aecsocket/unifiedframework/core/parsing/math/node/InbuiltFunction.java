package com.gitlab.aecsocket.unifiedframework.core.parsing.math.node;

import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.EvaluationException;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathVisitor;

import java.util.function.Function;

public class InbuiltFunction implements MathExpressionNode {
    public enum Type {
        SIN(Math::sin), COS(Math::cos), TAN(Math::tan),
        ASIN(Math::asin), ACOS(Math::acos), ATAN(Math::atan),
        ABS(Math::abs), SQRT(Math::sqrt), EXP(Math::exp),
        LN(Math::log),
        LOG(n -> Math.log(n) * 0.43429448190325182765),
        LOG2(n -> Math.log(n) * 1.442695040888963407360);

        private final Function<Double, Double> function;

        Type(Function<Double, Double> function) {
            this.function = function;
        }

        public double apply(double base) { return function.apply(base); }

        public static Type from(String name) {
            switch (name) {
                case "sin":  return SIN;  case "cos":  return COS;  case "tan":  return TAN;
                case "asin": return ASIN; case "acos": return ACOS; case "atan": return ATAN;
                case "abs":  return ABS;  case "sqrt": return SQRT; case "exp":  return EXP;
                case "ln":   return LN;   case "log":  return LOG;  case "log2": return LOG2;
            }
            throw new EvaluationException("Invalid inbuilt function '" + name + "'");
        }
    }

    private final Type function;
    private final MathExpressionNode expression;

    public InbuiltFunction(Type function, MathExpressionNode expression) {
        this.function = function;
        this.expression = expression;
    }

    public Type function() { return function; }
    public MathExpressionNode expression() { return expression; }

    @Override public int type() { return MathExpressionNode.FUNCTION; }
    @Override public InbuiltFunction accept(MathVisitor visitor) {
        visitor.visit(this);
        expression.accept(visitor);
        return this;
    }

    @Override public double value() { return function.apply(expression.value()); }
}
