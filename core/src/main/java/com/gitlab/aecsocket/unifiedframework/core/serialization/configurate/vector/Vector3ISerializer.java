package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.vector;

import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.ConfigurateSerializer;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3I;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class Vector3ISerializer implements TypeSerializer<Vector3I>, ConfigurateSerializer {
    public static final Vector3ISerializer INSTANCE = new Vector3ISerializer();

    @Override
    public void serialize(Type type, @Nullable Vector3I obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            if ((obj.x() == obj.y()) && (obj.x() == obj.z()))
                node.set(obj.x());
            else
                node.setList(Integer.class, Arrays.asList(
                        obj.x(), obj.y(), obj.z()
                ));
        }
    }

    @Override
    public Vector3I deserialize(Type type, ConfigurationNode node) throws SerializationException {
        if (node.isList()) {
            List<? extends ConfigurationNode> list = asList(node, Vector3I.class, "x", "y", "z");
            return new Vector3I(
                    list.get(0).getInt(),
                    list.get(1).getInt(),
                    list.get(2).getInt()
            );
        } else
            return new Vector3I(node.getInt());
    }
}
