package com.gitlab.aecsocket.unifiedframework.core.parsing.logic.node;

import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicVisitor;

public class Not implements LogicExpressionNode {
    private final LogicExpressionNode expression;

    public Not(LogicExpressionNode expression) {
        this.expression = expression;
    }

    public LogicExpressionNode expression() { return expression; }

    @Override public int type() { return LogicExpressionNode.NOT; }
    @Override public Not accept(LogicVisitor visitor) {
        visitor.visit(this);
        expression.accept(visitor);
        return this;
    }

    @Override public boolean value() { return !expression.value(); }
}
