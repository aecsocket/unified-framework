package com.gitlab.aecsocket.unifiedframework.core.parsing.logic;

import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.NodeVisitor;

public interface LogicVisitor extends NodeVisitor<LogicExpressionNode> {}
