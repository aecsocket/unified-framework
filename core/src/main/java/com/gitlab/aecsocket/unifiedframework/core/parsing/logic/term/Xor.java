package com.gitlab.aecsocket.unifiedframework.core.parsing.logic.term;

import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicVisitor;

public class Xor extends StandardGate {
    public Xor(LogicExpressionNode a, LogicExpressionNode b) { super(a, b); }

    @Override public int type() { return LogicExpressionNode.OR; }
    @Override public Xor accept(LogicVisitor visitor) { return (Xor) super.accept(visitor); }

    @Override public boolean value() { return a.value() ^ b.value(); }
}
