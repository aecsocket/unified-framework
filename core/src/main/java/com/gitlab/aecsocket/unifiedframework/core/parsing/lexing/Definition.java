package com.gitlab.aecsocket.unifiedframework.core.parsing.lexing;

import java.util.regex.Pattern;

public final class Definition {
    private final Pattern pattern;
    private final int id;

    public Definition(Pattern pattern, int id) {
        this.pattern = pattern;
        this.id = id;
    }

    public Pattern pattern() { return pattern; }
    public int id() { return id; }

    public Token inst(String sequence) { return new Token(id, sequence); }

    public static Definition of(String text, int id) {
        return new Definition(Pattern.compile("^(" + text + ")"), id);
    }
}
