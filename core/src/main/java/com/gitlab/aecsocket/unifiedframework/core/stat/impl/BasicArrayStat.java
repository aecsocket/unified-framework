package com.gitlab.aecsocket.unifiedframework.core.stat.impl;

import io.leangen.geantyref.TypeToken;

import java.util.function.Function;

/**
 * A basic implementation of {@link ArrayStat}.
 * @param <E> The array type.
 */
public class BasicArrayStat<E> extends ArrayStat<E> {
    private final Function<Integer, E[]> newArray;

    public BasicArrayStat(E[] defaultValue, Function<Integer, E[]> newArray) {
        super(defaultValue);
        this.newArray = newArray;
    }

    public BasicArrayStat(Function<Integer, E[]> newArray) {
        this.newArray = newArray;
    }

    public BasicArrayStat(BasicArrayStat<E> o) {
        super(o);
        newArray = o.newArray;
    }

    @Override public TypeToken<E> arrayValueType() { return new TypeToken<>(){}; }
    protected E[] newArray(int length) { return newArray.apply(length); }
}
