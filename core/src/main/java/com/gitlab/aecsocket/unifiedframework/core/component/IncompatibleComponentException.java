package com.gitlab.aecsocket.unifiedframework.core.component;

public class IncompatibleComponentException extends RuntimeException {
    public IncompatibleComponentException() {}
    public IncompatibleComponentException(String message) { super(message); }
    public IncompatibleComponentException(String message, Throwable cause) { super(message, cause); }
    public IncompatibleComponentException(Throwable cause) { super(cause); }
    public IncompatibleComponentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) { super(message, cause, enableSuppression, writableStackTrace); }

    public IncompatibleComponentException(String slot, String component) {
        this("Component [" + component + "] is not compatible with [" + slot + "]");
    }
    public IncompatibleComponentException(Slot slot, Component component) {
        this(slot.toString(), component.toString());
    }
}
