package com.gitlab.aecsocket.unifiedframework.core.registry;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * Context for a resolvable object by a resolver.
 */
public interface ResolutionContext {
    /**
     * Resolves another object with the specified ID.
     * @param id The ID.
     * @return The object.
     */
    Identifiable rawResolve(String id);

    /**
     * Resolves another object with the specified ID, or an empty Optional if none was found.
     * @param id The ID.
     * @param type The object type.
     * @param <T> The object type.
     * @return An Optional of the object.
     */
    default <T> Optional<T> resolve(String id, Class<T> type) {
        Identifiable result = rawResolve(id);
        return type.isInstance(result) ? Optional.of(type.cast(result)) : Optional.empty();
    }

    /**
     * Resolves another object with the specified ID, or throws an exception if none was found.
     * @param id The ID.
     * @param type The object type.
     * @param <T> The object type.
     * @return The object.
     * @throws ResolutionException If there was a failure getting the dependency.
     */
    default <T> T getResolve(String id, Class<T> type) throws ResolutionException {
        if (id == null) return null;
        Identifiable result = rawResolve(id);
        if (result == null) throw new ResolutionException("Could not resolve dependency " + id);
        if (!type.isInstance(result)) throw new ResolutionException("Dependency " + id + " is of wrong type (found " + result.getClass().getName() + ", expected " + type.getName() + ")");
        return type.cast(result);
    }

    /**
     * Resolves another object with the specified ID and runs a consumer on it, or throws an exception if none was found.
     * @param id The ID.
     * @param type The object type.
     * @param consumer The consumer of the object.
     * @param <T> The object type.
     * @throws ResolutionException If there was a failure getting the dependency.
     */
    default <T> void consumeResolve(String id, Class<T> type, Consumer<T> consumer) throws ResolutionException {
        T result = getResolve(id, type);
        if (result != null) consumer.accept(result);
    }
}
