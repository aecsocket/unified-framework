package com.gitlab.aecsocket.unifiedframework.core.util.data;

public final class Tuple2<A, B> {
    private final A a;
    private final B b;

    private Tuple2(A a, B b) {
        this.a = a;
        this.b = b;
    }

    public A a() { return a; }
    public B b() { return b; }

    @Override public String toString() { return "(" + a + ", " + b + ")"; }

    public static <A, B> Tuple2<A, B> of(A a, B b) { return new Tuple2<>(a, b); }
}
