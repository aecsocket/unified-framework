package com.gitlab.aecsocket.unifiedframework.core.stat.impl;

import com.gitlab.aecsocket.unifiedframework.core.stat.AbstractStat;
import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.ConfigurateStat;
import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.FunctionCreationException;
import io.leangen.geantyref.TypeToken;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;

import java.util.function.Function;

/**
 * A stat which holds an {@link Enum}. Format:
 * <p>
 * {@code enum}: sets to v
 */
public class EnumStat<E extends Enum<E>> extends AbstractStat<E> implements ConfigurateStat<E> {
    private final Class<E> type;

    public EnumStat(E defaultValue, Class<E> type) {
        super(defaultValue);
        this.type = type;
    }

    public EnumStat(Class<E> type) {
        this.type = type;
    }

    public EnumStat(EnumStat<E> o) {
        super(o);
        type = o.type;
    }

    @Override public TypeToken<E> valueType() { return new TypeToken<>(){}; }

    @Override
    public Function<E, E> getModFunction(ConfigurationNode node) {
        return b -> {
            try {
                return node.get(type);
            } catch (SerializationException e) {
                throw new FunctionCreationException(e);
            }
        };
    }
}
