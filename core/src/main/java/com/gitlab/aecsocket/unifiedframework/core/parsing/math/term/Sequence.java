package com.gitlab.aecsocket.unifiedframework.core.parsing.math.term;

import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathVisitor;

import java.util.LinkedList;
import java.util.List;

public abstract class Sequence implements MathExpressionNode {
    protected List<Term> terms = new LinkedList<>();

    public Sequence add(MathExpressionNode node, boolean positive) {
        terms.add(new Term(node, positive));
        return this;
    }

    @Override
    public Sequence accept(MathVisitor visitor) {
        visitor.visit(this);
        for (Term term : terms)
            term.expression().accept(visitor);
        return this;
    }
}
