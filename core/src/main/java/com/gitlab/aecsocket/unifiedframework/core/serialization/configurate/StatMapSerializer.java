package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.stat.Stat;
import com.gitlab.aecsocket.unifiedframework.core.stat.StatMap;
import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.ConfigurateStat;
import com.google.common.base.Preconditions;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Serializes and deserializes {@link StatMap}s. It is important to note that deserialization
 * requires setting the {@link StatMapSerializer#originals} field, through {@link StatMapSerializer#originals(Map)}.
 */
public class StatMapSerializer implements TypeSerializer<StatMap>, ConfigurateSerializer {
    private Map<String, Stat<?>> originals;

    public StatMapSerializer(Map<String, Stat<?>> originals) {
        this.originals = originals;
    }

    public StatMapSerializer() {}

    public Map<String, Stat<?>> originals() { return originals; }
    public void originals(Map<String, Stat<?>> originals) { this.originals = originals; }

    @Override
    public void serialize(Type type, @Nullable StatMap obj, ConfigurationNode node) {}

    @Override
    public StatMap deserialize(Type type, ConfigurationNode node) throws SerializationException {
        return deserialize(type, node, originals);
    }

    public StatMap deserialize(Type type, ConfigurationNode node, Map<String, Stat<?>> originals) throws SerializationException {
        Preconditions.checkNotNull(originals);
        StatMap map = new StatMap();

        Map<?, ? extends ConfigurationNode> nodes = asMap(node, type);
        for (Map.Entry<?, ? extends ConfigurationNode> entry : nodes.entrySet()) {
            addStat(type, entry.getValue(), map, entry.getKey().toString());
        }

        originals.forEach((key, stat) -> {
            if (!map.containsKey(key))
                map.put(key, stat.inst());
        });
        return map;
    }

    // use a separate method so I can use <T>
    protected <T> void addStat(Type type, ConfigurationNode node, StatMap map, String key) throws SerializationException {
        if (!originals.containsKey(key)) return;
        @SuppressWarnings("unchecked")
        Stat<T> raw = (Stat<T>) originals.get(key);
        if (!(raw instanceof ConfigurateStat<?>))
            throw new SerializationException(node, type, "Given stat [" + key + "] is not a HoconStat");
        ConfigurateStat<T> stat = (ConfigurateStat<T>) raw;

        map.put(key, stat.inst(stat.getModFunction(node)));
    }
}
