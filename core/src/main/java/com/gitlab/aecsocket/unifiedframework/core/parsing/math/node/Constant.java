package com.gitlab.aecsocket.unifiedframework.core.parsing.math.node;

import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.EvaluationException;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathVisitor;

public class Constant implements MathExpressionNode {
    private final double value;

    public Constant(double value) {
        this.value = value;
    }

    public Constant(String text) {
        try {
            value = Double.parseDouble(text);
        } catch (NumberFormatException e) {
            throw new EvaluationException(e);
        }
    }

    @Override public int type() { return MathExpressionNode.CONSTANT; }
    @Override public Constant accept(MathVisitor visitor) { visitor.visit(this); return this; }

    @Override public double value() { return value; }
}
