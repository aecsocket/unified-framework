package com.gitlab.aecsocket.unifiedframework.core.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SlotImpl implements Slot {
    private ComponentImpl component;
    private SlotRef<?> parent;

    public SlotImpl() {}

    @Override @SuppressWarnings("unchecked") public @Nullable <C extends Component> C get() { return (C) component; }

    @Override
    public SlotImpl set(@Nullable Component component) {
        if (component != null && !isCompatible(component))
            throw new IncompatibleComponentException(this, component);
        if (this.component != null)
            this.component.parent(null);
        this.component = (ComponentImpl) component;
        if (component != null)
            component.parent(this);
        return this;
    }

    @Override
    public boolean isCompatible(@NotNull Component component) {
        return component instanceof ComponentImpl;
    }

    @Override @SuppressWarnings("unchecked") public <C extends Component> SlotRef<C> parent() { return (SlotRef<C>) parent; }
    @Override public SlotImpl parent(Component parent, String slotKey) {
        this.parent = (parent == null && slotKey == null) ? null : new SlotRef<>(parent, slotKey);
        return this;
    }

    @Override
    public String toString() { return "[" + (component == null ? "null" : component.toString()) + "]"; }
}
