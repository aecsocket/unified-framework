package com.gitlab.aecsocket.unifiedframework.core.locale;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;

public abstract class AbstractLocalization<R> implements Localization<R> {
    private final Map<Locale, Translations> translations = new HashMap<>();
    private Locale defaultLocale = Locale.US;

    @Override public Map<Locale, Translations> translations() { return new HashMap<>(translations); }
    @Override public Translations get(Locale locale) { return translations.get(locale); }
    @Override public boolean has(Locale locale) { return translations.containsKey(locale); }

    @Override public void set(Translations translations) { this.translations.put(translations.locale(), translations); }
    @Override public void set(Locale locale, String key, String... value) { translations.computeIfAbsent(locale, Translations::new).set(key, value); }

    @Override public Translations add(Translations translations) { return this.translations.computeIfAbsent(translations.locale(), Translations::new).add(translations); }
    @Override public Translations remove(Locale locale) { return translations.remove(locale); }
    @Override public void clear() { translations.clear(); }

    @Override public Locale defaultLocale() { return defaultLocale; }
    @Override public void defaultLocale(Locale defaultLocale) { this.defaultLocale = defaultLocale; }

    protected abstract R genFallback(Locale locale, String key, Object... args);
    protected abstract R format(Locale locale, String value, Object... args);

    protected String use(Locale locale, String key, Supplier<String> fallback) {
        if (locale != null) {
            Translations current = translations.get(defaultLocale);
            if (current != null) {
                String value = current.get(key);
                if (value != null) {
                    return value;
                }
            }
        }
        return fallback.get();
    }

    public String values(Locale locale, String key) {
        return use(locale, key, () -> use(defaultLocale, key, () -> null));
    }

    @Override
    public R gen(Locale locale, String key, Object... args) {
        String value = values(locale, key);
        if (value == null)
            return genFallback(locale, key, args);

        return format(locale, value, args);
    }
}
