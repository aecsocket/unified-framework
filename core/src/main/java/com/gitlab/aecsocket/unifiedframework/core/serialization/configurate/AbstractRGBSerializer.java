package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.util.color.RGBA;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractRGBSerializer<T> implements TypeSerializer<T>, ConfigurateSerializer {
    private final RGBA.Format format;

    public AbstractRGBSerializer(@NotNull RGBA.Format format) {
        this.format = format;
    }

    public AbstractRGBSerializer() { this(RGBA.Format.HEX); }

    public RGBA.Format format() { return format; }

    protected abstract int value(T obj);
    protected abstract T of(int value);

    @Override
    public void serialize(Type type, @Nullable T obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            int value = value(obj);
            switch (format) {
                case VALUE: node.set(value);
                case HEX: node.set(String.format("#%x", value));
                case COMPONENTS: node.setList(int.class, Arrays.asList(
                        (value >> 16) & 0xff,
                        (value >> 8) & 0xff,
                        value & 0xff
                ));
            }
        }
    }

    @Override
    public T deserialize(Type type, ConfigurationNode node) throws SerializationException {
        if (node.isList()) {
            List<? extends ConfigurationNode> list = asList(node, type, "r", "g", "b");
            return of(
                    (list.get(0).getInt() & 0xff) << 16
                    | (list.get(1).getInt() & 0xff) << 8
                    | (list.get(2).getInt() & 0xff));
        }

        String hex = node.getString();
        if (hex != null && hex.startsWith("#")) {
            try {
                return of((int) Long.parseLong(hex.substring(1), 16));
            } catch (NumberFormatException e) {
                throw new SerializationException(node, type, "Invalid hex color `" + hex + "`: must be in format `#rrggbbaa`", e);
            }
        }

        return fallback(type, node);
    }

    protected T fallback(Type type, ConfigurationNode node) {
        return of(node.getInt());
    }
}
