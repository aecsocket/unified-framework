package com.gitlab.aecsocket.unifiedframework.core.util.projectile;

import com.gitlab.aecsocket.unifiedframework.core.scheduler.TaskContext;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import org.jetbrains.annotations.NotNull;

/**
 * A point in space which is affected by physics and can collide with objects.
 * @param <C> The type of object that is collidable with this projectile.
 * @param <R> The type of ray trace.
 */
public abstract class Projectile<C, R extends RayTrace<C>> {
    /**
     * The acceleration of gravity in m/s^2.
     */
    public static final double GRAVITY = 9.81;
    /** The accuracy of a projectile going through a block. */
    public static final double TOLERANCE = 0.1;

    public static final int REMOVE_REASON_REMOVED = 0;

    protected Vector3D position;
    protected Vector3D velocity; // in m/s
    protected double bounce = 0d;
    protected double drag = 0d;
    protected double gravity = GRAVITY;

    private int steps;
    private int collisions;
    private double travelled;

    public Projectile(Vector3D position, Vector3D velocity) {
        this.position = position;
        this.velocity = velocity;
    }

    public Vector3D position() { return position; }
    public Projectile<C, R> position(Vector3D position) { this.position = position; return this; }

    public Vector3D velocity() { return velocity; }
    public Projectile<C, R> velocity(Vector3D velocity) { this.velocity = velocity; return this; }

    public double bounce() { return bounce; }
    public Projectile<C, R> bounce(double bounce) { this.bounce = bounce; return this; }

    public double drag() { return drag; }
    public Projectile<C, R> drag(double drag) { this.drag = drag; return this; }

    public double gravity() { return gravity; }
    public Projectile<C, R> gravity(double gravity) { this.gravity = gravity; return this; }

    public int steps() { return steps; }
    public int hits() { return collisions; }
    public double travelled() { return travelled; }

    public void tick(TaskContext ctx) {
        double step = ctx.delta() / 1000d;

        double drag = 1 - this.drag;
        velocity = new Vector3D(
                velocity.x() * drag,
                (velocity.y() - (gravity * step)) * drag,
                velocity.z() * drag
        );

        if (velocity.manhattanLength() > 0) {
            double distance = velocity.length() * step;
            R ray = rayTrace(distance);
            Vector3D from = position;
            position = ray.position();

            C collided = ray.collided();
            if (collided != null) {
                if (!ignoreCollided(ray, collided)) {
                    collide(ctx, ray, collided);
                    ++collisions;
                }

                // go through the collidable we're in
                Vector3D vecStep = velocity.normalize().multiply(TOLERANCE);
                while (rayTrace(TOLERANCE).collided() != null) {
                    position = position.add(vecStep);
                }
            }

            Vector3D delta = position.subtract(from);

            double deltaLength = delta.length();
            step(ctx, ray, from, delta, deltaLength);
            if (ctx.cancelled()) {
                remove(REMOVE_REASON_REMOVED);
                return;
            }
            travelled += deltaLength;
        }
        ++steps;
    }

    public void bounce(R ray) {
        velocity = velocity.deflect(ray.collisionNormal()).multiply(bounce);
    }

    protected abstract @NotNull R rayTrace(double distance);

    protected boolean ignoreCollided(R ray, C collided) { return false; }

    protected abstract void step(TaskContext ctx, R ray, Vector3D from, Vector3D delta, double deltaLength);

    protected abstract void collide(TaskContext ctx, R ray, C collided);

    protected void remove(int reason) {}
}
