package com.gitlab.aecsocket.unifiedframework.core.component;

import java.util.Objects;

public class SlotRef<C extends Component> {
    private final C component;
    private final String slotKey;

    public SlotRef(C component, String slotKey) {
        this.component = component;
        this.slotKey = slotKey;
    }

    public C component() { return component; }
    public String slotKey() { return slotKey; }

    public Slot get() { return component.slot(slotKey); }
    public Slot parent() { return component.parent(); }

    @Override public String toString() { return slotKey; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SlotRef<?> slotRef = (SlotRef<?>) o;
        return component.equals(slotRef.component) && slotKey.equals(slotRef.slotKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(component, slotKey);
    }
}
