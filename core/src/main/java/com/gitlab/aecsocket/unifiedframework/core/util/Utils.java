package com.gitlab.aecsocket.unifiedframework.core.util;

import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2D;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import org.spongepowered.configurate.objectmapping.ObjectMapper;
import org.spongepowered.configurate.serialize.TypeSerializer;
import org.spongepowered.configurate.serialize.TypeSerializerCollection;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiConsumer;

/**
 * Generic plugin utilities.
 */
public final class Utils {
    public static final int MSPT = 50;
    public static final int TPS = 20;

    /**
     * Gets the value at a specified index of a collection if it is in range, otherwise returns a default value.
     * @param collection The collection.
     * @param index The index.
     * @param defaultValue The default value.
     * @param <E> The collection value type.
     * @return The value.
     */
    public static <E> E atOr(List<E> collection, int index, E defaultValue) {
        return index >= collection.size() ? defaultValue : collection.get(index);
    }

    /**
     * Gets the value at a specified index of a collection if it is in range, otherwise returns null.
     * @param collection The collection.
     * @param index The index.
     * @param <E> The collection value type.
     * @return The value.
     */
    public static <E> E atOr(List<E> collection, int index) { return atOr(collection, index, null); }

    public static int clamp(int v, int min, int max) { return Math.min(max, Math.max(min, v)); }
    public static long clamp(long v, long min, long max) { return Math.min(max, Math.max(min, v)); }
    public static float clamp(float v, float min, float max) { return Math.min(max, Math.max(min, v)); }
    public static double clamp(double v, double min, double max) { return Math.min(max, Math.max(min, v)); }

    public static int clamp01(int v) { return clamp(v, 0, 1); }
    public static long clamp01(long v) { return clamp(v, 0, 1); }
    public static float clamp01(float v) { return clamp(v, 0, 1); }
    public static double clamp01(double v) { return clamp(v, 0, 1); }

    public static int wrap(int v, int min, int max) { return (((v - min) % (max - min)) + (max - min)) % (max - min) + min; }
    public static long wrap(long v, long min, long max) { return (((v - min) % (max - min)) + (max - min)) % (max - min) + min; }
    public static float wrap(float v, float min, float max) { return (((v - min) % (max - min)) + (max - min)) % (max - min) + min; }
    public static double wrap(double v, double min, double max) { return (((v - min) % (max - min)) + (max - min)) % (max - min) + min; }

    public static Vector2D addRandom(Vector2D base, Vector2D random) {
        ThreadLocalRandom r = ThreadLocalRandom.current();
        return base.add(
                r.nextGaussian() * random.x(),
                r.nextGaussian() * random.y()
        );
    }

    public static Vector3D addRandom(Vector3D base, Vector3D random) {
        ThreadLocalRandom r = ThreadLocalRandom.current();
        return base.add(
                r.nextGaussian() * random.x(),
                r.nextGaussian() * random.y(),
                r.nextGaussian() * random.z()
        );
    }

    /**
     * Gets all fields for a class, and its supertypes.
     * @param type The type.
     * @return The fields.
     */
    public static List<Field> getAllModelFields(Class<?> type) {
        List<Field> fields = new ArrayList<>();
        do {
            Collections.addAll(fields, type.getDeclaredFields());
            type = type.getSuperclass();
        } while (type != null);
        return fields;
    }

    public static void applyRecursively(File root, Path relative, BiConsumer<File, Path> consumer) {
        File file = root.toPath().resolve(relative).toFile();
        if (file.isDirectory()) {
            for (String child : file.list())
                applyRecursively(root, relative.resolve(child), consumer);
        } else {
            consumer.accept(file, relative);
        }
    }

    public static void applyRecursively(File root, BiConsumer<File, Path> consumer) {
        applyRecursively(root, Path.of(""), consumer);
    }

    public static Vector3D relativeOffset(Vector3D origin, Vector3D direction, Vector3D offset) {
        Vector3D xzTangent = new Vector3D(-direction.z(), 0.0, direction.x()).normalize();
        Vector3D yTangent = xzTangent.crossProduct(direction).normalize();

        Vector3D finalOffset = xzTangent.multiply(offset.x())
                .add(direction.multiply(offset.z())
                        .add(yTangent.multiply(offset.y())));

        return origin.add(finalOffset);
    }

    /**
     * Converts an object to a string.
     * <p>
     * If it is null, returns {@code null}.
     * If it is an array, returns {@link Arrays#asList(Object[])} on it.
     * Otherwise, uses {@link Object#toString()}.
     * @param object The object.
     * @return The string representation.
     */
    public static String toString(Object object) {
        if (object == null) return "null";
        if (object.getClass().isArray()) return Arrays.toString((Object[]) object);
        return object.toString();
    }

    /**
     * Gets a delegate serializer while building serializers. Because Configurate sucks.
     * @param type The type.
     * @param builder The builder.
     * @param mapper The object mapper factory.
     * @param <T> The type.
     * @return The serializer.
     */
    @SuppressWarnings("unchecked")
    public static <T> TypeSerializer<T> delegate(Type type, TypeSerializerCollection.Builder builder, ObjectMapper.Factory mapper) {
        return (TypeSerializer<T>) builder.build().childBuilder().registerAnnotatedObjects(mapper).build().get(type);
    }

    public static Component repeat(Component component, int amount) {
        TextComponent.Builder result = Component.text();
        for (int i = 0; i < amount; i++)
            result.append(component);
        return result.build();
    }

    public static Component join(Component delimeter, Component... values) {
        TextComponent.Builder result = Component.text();
        for (int i = 0; i < values.length; i++) {
            result.append(values[i]);
            if (i < values.length - 1)
                result.append(delimeter);
        }
        return result.asComponent();
    }

    public static Component join(Component delimeter, Iterable<Component> values) {
        TextComponent.Builder result = Component.text();
        Iterator<Component> iterator = values.iterator();
        while (iterator.hasNext()) {
            result.append(iterator.next());
            if (iterator.hasNext())
                result.append(delimeter);
        }
        return result.asComponent();
    }

    public static int wrapIndex(int size, int index) {
        if (size == 0)
            throw new IllegalArgumentException("Size is 0");
        return (index < 0 ? size + index : index) % size;
    }

    public static int wrapIndex(Collection<?> collection, int index) {
        return wrapIndex(collection.size(), index);
    }

    public static long toMs(double seconds) { return (long) (seconds * 1000); }
    public static double fromMs(long ms) { return ms / 1000d; }

    public static long toTicks(long ms) { return ms / MSPT; }
    public static long fromTicks(long ticks) { return ticks * MSPT; }

    public static double square(double n) { return n * n; }
    public static double lerp(double a, double b, double percent) {
        return a + ((b - a) * percent);
    }
    public static float lerp(float a, float b, float percent) {
        return a + ((b - a) * percent);
    }
}
