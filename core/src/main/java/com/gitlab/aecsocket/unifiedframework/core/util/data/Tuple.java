package com.gitlab.aecsocket.unifiedframework.core.util.data;

public final class Tuple {
    public static <A, B> Tuple2<A, B> of(A a, B b) { return Tuple2.of(a, b); }
    public static <A, B, C> Tuple3<A, B, C> of(A a, B b, C c) { return Tuple3.of(a, b, c); }
}
