package com.gitlab.aecsocket.unifiedframework.core.util.color;

import com.gitlab.aecsocket.unifiedframework.core.util.Utils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;

public interface RGBA {
    enum Format {
        VALUE, HEX, COMPONENTS
    }

    int value();
    default int rgbValue() { return value() >> 8; }

    default @Range(from = 0x00, to = 0xff) int r() { return (value() >> 24) & 0xff; }
    default RGBA r(int r) { return RGBA.of(r, g(), b(), a()); }
    default float pr() { return r() / 255f; }
    default RGBA pr(float r) { return RGBA.of((int) (Utils.clamp01(r) * 255), g(), b(), a()); }

    default @Range(from = 0x00, to = 0xff) int g() { return (value() >> 16) & 0xff; }
    default RGBA g(int g) { return RGBA.of(r(), g, b(), a()); }
    default float pg() { return g() / 255f; }
    default RGBA pg(float g) { return RGBA.of(r(), (int) (Utils.clamp01(g) * 255), b(), a()); }

    default @Range(from = 0x00, to = 0xff) int b() { return (value() >> 8) & 0xff; }
    default RGBA b(int b) { return RGBA.of(r(), g(), b, a()); }
    default float pb() { return b() / 255f; }
    default RGBA pb(float b) { return RGBA.of(r(), g(), (int) (Utils.clamp01(b) * 255), a()); }

    default @Range(from = 0x00, to = 0xff) int a() { return value() & 0xff; }
    default RGBA a(int a) { return RGBA.of(r(), g(), b(), a); }
    default float pa() { return a() / 255f; }
    default RGBA pa(float a) { return RGBA.of(r(), g(), b(), (int) (Utils.clamp01(a) * 255)); }

    default RGBA add(int r, int g, int b, int a) { return RGBA.of(r() + r, g() + g, b() + b, a() + a); }
    default RGBA add(RGBA o) { return RGBA.of(o.r(), o.g(), o.b(), o.a()); }

    default RGBA lerp(@NotNull RGBA top) {
        float a = top.pa();
        return of(
                Utils.lerp(pr(), top.pr(), a),
                Utils.lerp(pg(), top.pg(), a),
                Utils.lerp(pb(), top.pb(), a),
                Utils.lerp(pa(), top.pa(), a)
        );
    }

    default HSV hsv() { return HSV.ofRGB(r(), g(), b()); }

    default String toString(@NotNull RGBA.Format format) {
        switch (format) {
            case VALUE: return String.format("%d", value());
            case HEX: return String.format("#%08x", value());
            case COMPONENTS: return String.format("%d, %d, %d, %d", r(), g(), b(), a());
        }
        throw new IllegalArgumentException("Invalid format");
    }


    static RGBA of(int value) { return new RGBAImpl(value); }
    static RGBA ofRGB(int value) { return of(value << 8); }

    static RGBA of(
            @Range(from = 0x00, to = 0xff) int r,
            @Range(from = 0x00, to = 0xff) int g,
            @Range(from = 0x00, to = 0xff) int b,
            @Range(from = 0x00, to = 0xff) int a
    ) {
        return of(
                (r & 0xff) << 24
                | (g & 0xff) << 16
                | (b & 0xff) << 8
                | (a & 0xff)
        );
    }

    static RGBA of(float r, float g, float b, float a) {
        return of(
                (int) (Utils.clamp01(r) * 255),
                (int) (Utils.clamp01(g) * 255),
                (int) (Utils.clamp01(b) * 255),
                (int) (Utils.clamp01(a) * 255)
        );
    }

    static RGBA ofHSV(float h, float s, float v, float a) {
        s = Utils.clamp01(s);
        v = Utils.clamp01(v);
        if (s == 0) {
            return of(v, v, v, a);
        }

        h = Utils.wrap(h, 0, 1) * 6;
        int sector = (int) h;
        float f = h - sector;
        float x = v * (1 - s);
        float y = v * (1 - s * f);
        float z = v * (1 - s * (1 - f));

        switch (sector) {
            case 0: return of(v, z, x, a);
            case 1: return of(y, v, x, a);
            case 2: return of(x, v, z, a);
            case 3: return of(x, y, v, a);
            case 4: return of(z, x, v, a);
            case 5: return of(v, x, y, a);
        }
        throw new IllegalArgumentException("HSV sector fell outside bounds (HOW)");
    }
}
