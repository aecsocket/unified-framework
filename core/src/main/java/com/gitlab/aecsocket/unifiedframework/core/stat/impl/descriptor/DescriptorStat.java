package com.gitlab.aecsocket.unifiedframework.core.stat.impl.descriptor;

import com.gitlab.aecsocket.unifiedframework.core.stat.AbstractStat;
import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.ConfigurateStat;
import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.FunctionCreationException;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Descriptor;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;

import java.util.function.Function;

public abstract class DescriptorStat<V, T extends Descriptor<V>> extends AbstractStat<T> implements ConfigurateStat<T> {
    public DescriptorStat(T defaultValue) { super(defaultValue); }
    public DescriptorStat() {}
    public DescriptorStat(DescriptorStat<V, T> o) { super(o); }

    @Override
    public Function<T, T> getModFunction(ConfigurationNode node) throws FunctionCreationException {
        T value;
        try {
            value = node.get(valueType());
        } catch (SerializationException e) {
            throw new FunctionCreationException(e);
        }
        if (value == null)
            throw new FunctionCreationException("Null value descriptor created");

        return base -> base == null ? value : apply(base, value);
    }

    protected abstract T apply(T base, T value);
}
