package com.gitlab.aecsocket.unifiedframework.core.util.log;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents how important a message that is being logged is.
 */
public class LogLevel {
    /** Fine detail, such as for debug messages. */
    public static final LogLevel VERBOSE = new LogLevel(-1, "VERBOSE", "VERB", "44", "39", "37");
    /** General informational messages. */
    public static final LogLevel INFO = new LogLevel(0, "INFO", "INFO", "42", "30", "39");
    /** Improper outcome that does not stop all execution. */
    public static final LogLevel WARN = new LogLevel(2, "WARN", "WARN", "43", "30", "33");
    /** Improper outcome that is fatal to execution. */
    public static final LogLevel ERROR = new LogLevel(3, "ERROR", "ERR ", "41", "39", "31");

    private static final Map<String, LogLevel> defaultLevels = new HashMap<>();
    private static final String start = "\033[";
    private static final String split = ";";
    private static final String end = "m";

    static {
        defaultLevels.put(VERBOSE.name, VERBOSE);
        defaultLevels.put(INFO.name, INFO);
        defaultLevels.put(WARN.name, WARN);
        defaultLevels.put(ERROR.name, ERROR);
    }

    private int level;
    private String name;
    private String display;
    private String background;
    private String foreground;
    private String text;

    public LogLevel(int level, String name, String display, String background, String foreground, String text) {
        this.level = level;
        this.name = name;
        this.display = display;
        this.background = background;
        this.foreground = foreground;
        this.text = text;
    }

    public int level() { return level; }
    public void level(int level) { this.level = level; }

    public String name() { return name; }
    public void name(String name) { this.name = name; }

    public String display() { return display; }
    public void display(String display) { this.display = display; }

    public String background() { return background; }
    public void background(String background) { this.background = background; }

    public String foreground() { return foreground; }
    public void foreground(String foreground) { this.foreground = foreground; }

    public String text() { return text; }
    public void text(String text) { this.text = text; }

    /**
     * Generates the prefix that is used in {@link LabelledLogger#log(LogLevel, String, Object...)}.
     * @return The prefix.
     */
    public String generatePrefix() {
        return start + background + split + foreground + end +
                " " + display + " " +
                start + "0" + split + text + end;
    }

    @Override public String toString() { return name; }

    /**
     * Gets one of the default {@link LogLevel}s from its name.
     * @param name The name to look up.
     * @return The {@link LogLevel}, or null if it was not found.
     */
    public static LogLevel forName(String name) {
        return defaultLevels.get(name);
    }
}
