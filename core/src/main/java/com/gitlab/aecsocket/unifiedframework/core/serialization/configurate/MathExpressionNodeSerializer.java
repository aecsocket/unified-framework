package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.EvaluationException;
import com.gitlab.aecsocket.unifiedframework.core.parsing.lexing.LexerException;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathParser;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public class MathExpressionNodeSerializer implements TypeSerializer<MathExpressionNode> {
    public static final MathExpressionNodeSerializer INSTANCE = new MathExpressionNodeSerializer();

    @Override
    public void serialize(Type type, @Nullable MathExpressionNode obj, ConfigurationNode node) throws SerializationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public MathExpressionNode deserialize(Type type, ConfigurationNode node) throws SerializationException {
        String arg = node.getString();
        if (arg == null)
            throw new SerializationException(node, type, "Must be string of math expression");
        try {
            return MathParser.parses(arg);
        } catch (LexerException | EvaluationException e) {
            throw new SerializationException(node, type, "Could not parse math expression", e);
        }
    }
}
