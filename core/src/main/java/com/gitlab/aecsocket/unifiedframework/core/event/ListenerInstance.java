package com.gitlab.aecsocket.unifiedframework.core.event;

import java.util.function.Consumer;

/**
 * A wrapper class containing a listener and its priority.
 * @param <E> The event type.
 */
public class ListenerInstance<E> {
    private final Class<E> eventType;
    private final Consumer<E> listener;
    private int priority;

    public ListenerInstance(Class<E> eventType, Consumer<E> listener, int priority) {
        this.eventType = eventType;
        this.listener = listener;
        this.priority = priority;
    }

    public ListenerInstance(ListenerInstance<E> o) {
        eventType = o.eventType;
        listener = o.listener;
        priority = o.priority;
    }

    public Class<E> getEventType() { return eventType; }
    public Consumer<E> getListener() { return listener; }

    public int getPriority() { return priority; }
    public void setPriority(int priority) { this.priority = priority; }

    @Override public String toString() { return listener + " @ " + priority; }
}
