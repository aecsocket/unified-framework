package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.descriptor;

import io.leangen.geantyref.TypeToken;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.ConfigurateSerializer;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.NumberDescriptor;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Descriptor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Allows proper deserialization of a {@link NumberDescriptor}.
 */
public abstract class DescriptorSerializer<V, T extends Descriptor<V>> implements TypeSerializer<T>, ConfigurateSerializer {
    @Override
    public void serialize(Type type, @Nullable T obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            if (obj.operation() == NumberDescriptor.Operation.SET)
                node.set(obj.value());
            else {
                node.appendListNode().set(obj.operation().symbol());
                node.appendListNode().set(obj.value());
                if (obj.suffix() != null)
                    node.appendListNode().set(obj.suffix().symbol());
            }
        }
    }

    @Override
    public T deserialize(Type type, ConfigurationNode node) throws SerializationException {
        if (node.isList()) {
            List<? extends ConfigurationNode> list = asList(node, type, "operation", "value");
            return create(
                    list.get(1).get(valueType()),
                    Descriptor.Operation.bySymbol(list.get(0).getString()),
                    list.size() >= 3 ? Descriptor.Suffix.bySymbol(list.get(2).getString()) : null
            );
        } else
            return create(node.get(valueType()));
    }

    protected abstract TypeToken<V> valueType();
    protected abstract T create(V value);
    protected abstract T create(V value, Descriptor.Operation operation, Descriptor.Suffix suffix);
}
