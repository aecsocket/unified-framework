package com.gitlab.aecsocket.unifiedframework.core.parsing.logic.term;

import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicVisitor;

public class Or extends StandardGate {
    public Or(LogicExpressionNode a, LogicExpressionNode b) { super(a, b); }

    @Override public int type() { return LogicExpressionNode.OR; }
    @Override public Or accept(LogicVisitor visitor) { return (Or) super.accept(visitor); }

    @Override public boolean value() { return a.value() | b.value(); }
}
