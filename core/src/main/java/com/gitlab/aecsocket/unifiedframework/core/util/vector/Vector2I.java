package com.gitlab.aecsocket.unifiedframework.core.util.vector;

import java.util.Objects;

/**
 * Immutable data class holding an x, y, z integer vector.
 */
public class Vector2I {
    public static final Vector2I ZERO = new Vector2I();

    private final int x;
    private final int y;

    public Vector2I(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vector2I(int v) { this(v, v); }

    public Vector2I() { this(0, 0); }

    public int x() { return x; }
    public Vector2I x(int x) { return new Vector2I(x, y); }

    public int y() { return y; }
    public Vector2I y(int y) { return new Vector2I(x, y); }

    public Vector2I add(int x, int y) { return new Vector2I(this.x + x, this.y + y); }
    public Vector2I add(Vector2I o) { return add(o.x, o.y); }
    public Vector2I add(int v) { return add(v, v); }

    public Vector2I subtract(int x, int y) { return add(-x, -y); }
    public Vector2I subtract(Vector2I o) { return subtract(o.x, o.y); }
    public Vector2I subtract(int v) { return subtract(v, v); }

    public Vector2I multiply(int x, int y) { return new Vector2I(this.x * x, this.y * y); }
    public Vector2I multiply(Vector2I o) { return multiply(o.x, o.y); }
    public Vector2I multiply(int v) { return multiply(v, v); }

    public Vector2I divide(int x, int y) { return new Vector2I(this.x / x, this.y / y); }
    public Vector2I divide(Vector2I o) { return divide(o.x, o.y); }
    public Vector2I divide(int v) { return divide(v, v); }

    /**
     * Gets <code>abs(x) + abs(y) + abs(z)</code>.
     * @return The result.
     */
    public int manhattanLength() { return Math.abs(x) + Math.abs(y); }

    /**
     * Gets <code>sqrt((x*x) + (y*y))</code>.
     * @return The result.
     */
    public double length() { return Math.sqrt((x*x) + (y*y)); }

    public int dot(Vector2I o) { return (x*o.x) + (y*o.y); }

    public Vector2D toDoubles() { return new Vector2D(x, y); }

    @Override public String toString() { return "(" + x + ", " + y + ")"; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector2I vector3 = (Vector2I) o;
        return vector3.x == x && vector3.y == y;
    }

    @Override
    public int hashCode() { return Objects.hash(x, y); }
}
