package com.gitlab.aecsocket.unifiedframework.core.util.color;

/* package */ class HSVImpl implements HSV {
    private final float h;
    private final float s;
    private final float v;

    public HSVImpl(float h, float s, float v) {
        this.h = h;
        this.s = s;
        this.v = v;
    }

    @Override public float h() { return h; }
    @Override public float s() { return s; }
    @Override public float v() { return v; }

    @Override public String toString() { return String.format("%.3f, %.3f, %.3f", h(), s(), v()); }
}
