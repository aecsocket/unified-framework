package com.gitlab.aecsocket.unifiedframework.core.stat.impl;

import com.gitlab.aecsocket.unifiedframework.core.stat.AbstractStat;
import io.leangen.geantyref.TypeToken;
import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.ConfigurateStat;
import org.spongepowered.configurate.ConfigurationNode;

import java.util.function.Function;

/**
 * A stat which holds a string.
 */
public class StringStat extends AbstractStat<String> implements ConfigurateStat<String> {
    public StringStat(String defaultValue) { super(defaultValue); }
    public StringStat() {}
    @Override public TypeToken<String> valueType() { return new TypeToken<>(){}; }

    @Override
    public Function<String, String> getModFunction(ConfigurationNode node) {
        return b -> node.getString();
    }
}
