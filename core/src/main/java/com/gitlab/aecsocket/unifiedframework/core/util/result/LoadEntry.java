package com.gitlab.aecsocket.unifiedframework.core.util.result;

import java.nio.file.Path;
import java.util.function.Consumer;

public class LoadEntry<S, E extends Throwable> {
    private final Path path;
    private final S result;
    private final E exception;

    public LoadEntry(Path path, S result, E exception) {
        this.path = path;
        this.result = result;
        this.exception = exception;
    }

    public Path path() { return path; }
    public S result() { return result; }
    public E exception() { return exception; }

    public void apply(Consumer<S> ifSuccessful, Consumer<E> ifFailure) {
        if (exception == null)
            ifSuccessful.accept(result);
        else
            ifFailure.accept(exception);
    }

    public static <S, E extends Throwable> LoadEntry<S, E> of(Path path, S result) {
        return new LoadEntry<>(path, result, null);
    }

    public static <S, E extends Throwable> LoadEntry<S, E> of(Path path, E exception) {
        return new LoadEntry<>(path, null, exception);
    }
}
