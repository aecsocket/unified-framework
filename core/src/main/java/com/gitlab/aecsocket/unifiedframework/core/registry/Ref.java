package com.gitlab.aecsocket.unifiedframework.core.registry;

import java.util.Objects;

/**
 * A reference to an {@link Identifiable} stored in a {@link Registry}.
 */
public final class Ref<T extends Identifiable> {
    private T object;

    public Ref(T object) {
        Objects.requireNonNull(object);
        this.object = object;
    }

    public String getId() { return object.id(); }
    public T get() { return object; }
    public void set(T object) {
        Objects.requireNonNull(object);
        this.object = object;
    }

    @Override public String toString() { return "Ref{" + object + "}"; }
}
