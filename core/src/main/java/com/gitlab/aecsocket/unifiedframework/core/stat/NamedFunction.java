package com.gitlab.aecsocket.unifiedframework.core.stat;

import java.util.function.Function;

/**
 * Function with a name, accessible by using {@link #toString()}.
 * @param <T> The parameter type.
 * @param <R> The resulting type.
 */
public class NamedFunction<T, R> implements Function<T, R> {
    private final Function<T, R> function;
    private final String name;

    public NamedFunction(Function<T, R> function, String name) {
        this.function = function;
        this.name = name;
    }

    @Override public R apply(T t) { return function.apply(t); }
    @Override public String toString() { return name; }
}
