package com.gitlab.aecsocket.unifiedframework.core.util.vector;

import com.gitlab.aecsocket.unifiedframework.core.util.Utils;

import java.util.Objects;

/**
 * Immutable data class holding an x, y, z decimal vector.
 */
public class Vector2D {
    public static final Vector2D ZERO = new Vector2D();

    private final double x;
    private final double y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D(double v) { this(v, v); }

    public Vector2D() { this(0, 0); }

    public double x() { return x; }
    public Vector2D x(double x) { return new Vector2D(x, y); }

    public double y() { return y; }
    public Vector2D y(double y) { return new Vector2D(x, y); }

    public Vector2D add(double x, double y) { return new Vector2D(this.x + x, this.y + y); }
    public Vector2D add(Vector2D o) { return add(o.x, o.y); }
    public Vector2D add(double v) { return add(v, v); }

    public Vector2D subtract(double x, double y) { return add(-x, -y); }
    public Vector2D subtract(Vector2D o) { return subtract(o.x, o.y); }
    public Vector2D subtract(double v) { return subtract(v, v); }

    public Vector2D multiply(double x, double y) { return new Vector2D(this.x * x, this.y * y); }
    public Vector2D multiply(Vector2D o) { return multiply(o.x, o.y); }
    public Vector2D multiply(double v) { return multiply(v, v); }

    public Vector2D divide(double x, double y) { return new Vector2D(this.x / x, this.y / y); }
    public Vector2D divide(Vector2D o) { return divide(o.x, o.y); }
    public Vector2D divide(double v) { return divide(v, v); }

    /**
     * Gets <code>abs(x) + abs(y) + abs(z)</code>.
     * @return The result.
     */
    public double manhattanLength() { return Math.abs(x) + Math.abs(y); }

    /**
     * Gets <code>sqrt((x*x) + (y*y))</code>.
     * @return The result.
     */
    public double length() { return Math.sqrt((x*x) + (y*y)); }

    public Vector2D normalize() {
        double length = length();
        return new Vector2D(x / length, y / length);
    }

    public double dot(Vector2D o) { return (x*o.x) + (y*o.y); }

    /**
     * Gets the angle between this vector and another.
     * @param o The other vector.
     * @return The angle.
     */
    public double angle(Vector2D o) {
        double dot = Utils.clamp(dot(o) / (length() * o.length()), -1, 1);
        return Math.acos(dot);
    }

    public Vector2I toInts() { return new Vector2I((int) x, (int) y); }

    @Override public String toString() { return "(" + x + ", " + y + ")"; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector2D vector3 = (Vector2D) o;
        return Double.compare(vector3.x, x) == 0 && Double.compare(vector3.y, y) == 0;
    }

    @Override
    public int hashCode() { return Objects.hash(x, y); }
}
