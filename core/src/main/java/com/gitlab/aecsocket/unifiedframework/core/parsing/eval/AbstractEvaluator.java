package com.gitlab.aecsocket.unifiedframework.core.parsing.eval;

import com.gitlab.aecsocket.unifiedframework.core.parsing.lexing.Token;

import java.util.Deque;
import java.util.LinkedList;
import java.util.function.Function;
import java.util.function.Supplier;

public abstract class AbstractEvaluator<N extends ExpressionNode<?>> implements Evaluator<N> {
    protected Deque<Token> tokens;
    protected Token lookahead;

    @Override
    public N evaluate(Deque<Token> tokens) {
        this.tokens = new LinkedList<>(tokens);
        if (this.tokens.isEmpty())
            throw new EvaluationException("No tokens provided");
        lookahead = this.tokens.getFirst();

        N expr = evaluate();

        if (lookahead.id() != Token.EPSILON.id())
            throw new EvaluationException("Unexpected token '" + lookahead.sequence() + "'");

        return expr;
    }

    protected final void nextToken() {
        tokens.pop();
        lookahead = tokens.isEmpty() ? Token.EPSILON : tokens.getFirst();
    }

    @SafeVarargs
    protected final N seq(Supplier<N> start, Function<N, N>... mappers) {
        N result = start.get();
        for (Function<N, N> mapper : mappers)
            result = mapper.apply(result);
        return result;
    }

    protected final TokenEvaluator<N> handle() { return new TokenEvaluator<>(); }

    protected abstract N evaluate();
}
