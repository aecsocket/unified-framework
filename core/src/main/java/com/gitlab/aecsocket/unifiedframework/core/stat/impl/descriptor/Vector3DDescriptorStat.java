package com.gitlab.aecsocket.unifiedframework.core.stat.impl.descriptor;

import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import io.leangen.geantyref.TypeToken;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Vector3DDescriptor;

public class Vector3DDescriptorStat extends DescriptorStat<Vector3D, Vector3DDescriptor> {
    public Vector3DDescriptorStat(Vector3DDescriptor defaultValue) { super(defaultValue); }
    public Vector3DDescriptorStat(Vector3D defaultValue) { this(new Vector3DDescriptor(defaultValue)); }
    public Vector3DDescriptorStat() {}
    public Vector3DDescriptorStat(Vector3DDescriptorStat o) { super(o); }
    @Override public TypeToken<Vector3DDescriptor> valueType() { return new TypeToken<>(){}; }

    @Override
    protected Vector3DDescriptor apply(Vector3DDescriptor base, Vector3DDescriptor value) {
        return new Vector3DDescriptor(value.apply(base.apply()));
    }
}
