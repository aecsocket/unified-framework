package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public interface ConfigurateSerializer {
    ConfigurateSerializer INSTANCE = new ConfigurateSerializer(){};

    default Map<Object, ? extends ConfigurationNode> asMap(ConfigurationNode node, Type expectedType) throws SerializationException {
        if (!node.isMap())
            throw new SerializationException(node, expectedType, "Must be map");
        return node.childrenMap();
    }

    default List<? extends ConfigurationNode> asList(ConfigurationNode node, Type expectedType) throws SerializationException {
        if (!node.isList())
            throw new SerializationException(node, expectedType, "Must be list");
        return node.childrenList();
    }

    default List<? extends ConfigurationNode> asList(ConfigurationNode node, Type expectedType, String... required) throws SerializationException {
        List<? extends ConfigurationNode> list = asList(node, expectedType);
        if (list.size() < required.length)
            throw new SerializationException(node, expectedType, "Must be list with [ " + String.join(", ", required) + " ]");
        return list;
    }

    default ConfigurationNode get(Map<?, ? extends ConfigurationNode> nodes, String key, ConfigurationNode node, Type expectedType) throws SerializationException {
        if (!nodes.containsKey(key))
            throw new SerializationException(node, expectedType, "Must be map with key [" + key + "]");
        return nodes.get(key);
    }
}
