package com.gitlab.aecsocket.unifiedframework.core.parsing.logic;

import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.ExpressionNode;

public interface LogicExpressionNode extends ExpressionNode<LogicVisitor> {
    int CONSTANT = 1;
    int VARIABLE = 2;
    int NOT = 3;
    int AND = 4;
    int OR = 5;
    int XOR = 6;

    LogicExpressionNode accept(LogicVisitor visitor);
    boolean value();
    default void setVariable(String name, boolean value) {
        accept(new SetVariableVisitor(name, value));
    }
}
