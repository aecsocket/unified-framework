package com.gitlab.aecsocket.unifiedframework.core.resource;

import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.util.CheckedFunction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ConfigurateSettings {
    public static class Path {
        private final Object[] path;

        public Path(Object[] path) {
            this.path = path;
        }

        public Object[] path() { return path; }

        @Override public String toString() { return Arrays.toString(path); }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Path path1 = (Path) o;
            return Arrays.equals(path, path1.path);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(path);
        }
    }

    public interface Function<T> extends CheckedFunction<ConfigurationNode, T, SerializationException> {}

    private ConfigurationNode root;
    private final Map<Path, Object> cache = new HashMap<>();

    public ConfigurateSettings(ConfigurationNode root) {
        this.root = root;
    }

    public ConfigurateSettings() {}

    public ConfigurationNode root() { return root; }
    public void root(ConfigurationNode root) {
        this.root = root;
        cache.clear();
    }

    public Map<Path, Object> cache() { return cache; }
    public void clean() { cache.clear(); }
    public <T> void force(T result, Object... path) {
        cache.put(new Path(path.clone()), result);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(Function<T> function, Object... path) {
        Path rPath = new Path(path);
        if (cache.containsKey(rPath)) {
            return (T) cache.get(rPath);
        }

        ConfigurationNode node = root.node(path);
        T result = null;
        try {
            result = function.apply(node);
        } catch (SerializationException ignore) {}
        cache.put(new Path(path.clone()), result);
        return result;
    }
}
