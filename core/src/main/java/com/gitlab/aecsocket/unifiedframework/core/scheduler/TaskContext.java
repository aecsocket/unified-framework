package com.gitlab.aecsocket.unifiedframework.core.scheduler;

import java.util.function.Consumer;

public interface TaskContext extends Scheduler {
    abstract class Generic implements TaskContext {
        private final long elapsed;
        private final long delta;
        private final int iteration;
        public boolean cancelled;

        public Generic(long elapsed, long delta, int iteration) {
            this.elapsed = elapsed;
            this.delta = delta;
            this.iteration = iteration;
        }

        @Override public long elapsed() { return elapsed; }
        @Override public long delta() { return delta; }
        @Override public int iteration() { return iteration; }
        @Override public boolean cancelled() { return cancelled; }

        @Override
        public void cancel() {
            cancelled = true;
        }
    }

    Scheduler scheduler();
    long elapsed();
    long delta();
    int iteration();

    void cancel();
    boolean cancelled();

    default void run(Task task) { scheduler().run(task); }
    default void run(Consumer<TaskContext> function) { run(Task.single(function::accept, 0)); }
}
