package com.gitlab.aecsocket.unifiedframework.core.stat.impl.operation;

import java.util.Map;
import java.util.function.Function;

public class Operation<T> {
    private final Function<Class<?>, LoadOperation> loadOperationFactory;
    private final RunOperation<T> runOperation;

    public Operation(Function<Class<?>, LoadOperation> loadOperationFactory, RunOperation<T> runOperation) {
        this.loadOperationFactory = loadOperationFactory;
        this.runOperation = runOperation;
    }

    public Function<Class<?>, LoadOperation> getLoadOperationFactory() { return loadOperationFactory; }
    public RunOperation<T> getRunOperation() { return runOperation; }

    public Function<T, T> getFunction(Map<String, Object> passedArgs) {
        return base -> runOperation.run(base, passedArgs);
    }
}
