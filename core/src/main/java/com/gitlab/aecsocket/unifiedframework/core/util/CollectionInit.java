package com.gitlab.aecsocket.unifiedframework.core.util;

import java.util.Collection;

public class CollectionInit<E, T extends Collection<E>> {
    private final T collection;
    private CollectionInit(T collection) { this.collection = collection; }
    public CollectionInit<E, T> init(E object) { collection.add(object); return this; }
    public CollectionInit<E, T> init(Collection<E> collection) { this.collection.addAll(collection); return this; }
    public T get() { return collection; }

    public static <E, T extends Collection<E>> CollectionInit<E, T> of(T collection) { return new CollectionInit<>(collection); }
}
