package com.gitlab.aecsocket.unifiedframework.core.stat;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.function.Function;

/**
 * A collection of {@link StatInstance}s linked to their stat keys.
 */
public class StatMap extends LinkedHashMap<String, StatInstance<?>> {
    public StatMap(int initialCapacity, float loadFactor) { super(initialCapacity, loadFactor); }
    public StatMap(int initialCapacity) { super(initialCapacity); }
    public StatMap() {}
    public StatMap(Map<? extends String, ? extends StatInstance<?>> m) { super(m); }
    public StatMap(int initialCapacity, float loadFactor, boolean accessOrder) { super(initialCapacity, loadFactor, accessOrder); }

    //region Get/set

    /**
     * Gets a value from a stat in the map.
     * @param key The stat key.
     * @param <T> The stat value type.
     * @return The value, or null if the stat was null.
     */
    @SuppressWarnings("unchecked")
    public <T> T val(String key) { return containsKey(key) ? (T) get(key).get() : null; }

    /**
     * Sets a stat in the map.
     * @param key The stat key.
     * @param inst The stat instasnce.
     * @param <T> The stat value type.
     * @return This instance.
     */
    public <T> StatMap set(String key, StatInstance<T> inst) { put(key, inst); return this; }

    /**
     * Sets a stat in the map, and initializes it with no modifier.
     * @param key The stat key.
     * @param stat The stat.
     * @param <T> The stat value type.
     * @return This instance.
     */
    public <T> StatMap set(String key, Stat<T> stat) { return set(key, new StatInstance<>(stat, null)); }
    //endregion

    //region Evaluation
    /**
     * Evaluates a stat modification, but does not modify the map itself.
     * @param key The stat key.
     * @param other The stat instance to combine with.
     * @param <T> The stat value type.
     * @return The combination result, or the provided stat instance if the stat is not found in this map.
     */
    public <T> StatInstance<T> eval(String key, StatInstance<T> other) {
        @SuppressWarnings("unchecked")
        StatInstance<T> inst = (StatInstance<T>) get(key);
        return inst == null ? other : inst.modify(other);
    }

    /**
     * Evaluates a stat modification, but does not modify the map itself.
     * @param key The stat key.
     * @param valueType The stat value type.
     * @param function The modification function.
     * @param <T> The stat value type.
     * @return The combination result, or null if the stat is not found in this map.
     */
    public <T> T eval(String key, Class<T> valueType, Function<T, T> function) {
        @SuppressWarnings("unchecked")
        StatInstance<T> inst = (StatInstance<T>) get(key);
        return inst == null ? null : inst.modify(function).get();
    }

    /**
     * Evaluates modifying all stats with another stat map.
     * @param other The other stat map.
     * @return The resulting map.
     * @see #eval(String, StatInstance)
     */
    public StatMap evalAll(StatMap other) {
        StatMap map = new StatMap();
        other.forEach((key, inst) -> map.put(key, eval(key, inst)));
        return map;
    }

    //endregion

    //region Modification
    /**
     * Modifies a stat and puts it back into the map.
     * @param key The stat key.
     * @param other The stat instance to combine with.
     * @param <T> The stat value type.
     * @return The combination result, or the provided stat instance if the stat is not found in this map.
     */
    public <T> StatInstance<T> mod(String key, StatInstance<T> other) {
        @SuppressWarnings("unchecked")
        StatInstance<T> inst = (StatInstance<T>) get(key);
        StatInstance<T> result = inst == null ? other : inst.modify(other);
        put(key, result);
        return result;
    }

    /**
     * Modifies a stat and puts it back into the map.
     * @param key The stat key.
     * @param valueType The stat value type.
     * @param function The modification function.
     * @param <T> The stat value type.
     * @return The combination result, or the provided stat instance if the stat is not found in this map.
     */
    public <T> T mod(String key, Class<T> valueType, Function<T, T> function) {
        @SuppressWarnings("unchecked")
        StatInstance<T> inst = (StatInstance<T>) get(key);
        if (inst == null)
            return null;
        StatInstance<T> result = inst.modify(function);
        put(key, result);
        return result.get();
    }

    /**
     * Modifies all stats with another stat map.
     * @param other The other stat map.
     * @return This instance.
     * @see #mod(String, StatInstance)
     */
    public StatMap modAll(StatMap other) {
        other.forEach(this::mod);
        return this;
    }
    //endregion

    @Override
    public String toString() {
        StringJoiner result = new StringJoiner(", ");
        for (Map.Entry<String, StatInstance<?>> entry : entrySet()) {
            if (!entry.getValue().empty())
                result.add(entry.toString());
        }
        return "{" + result + "}";
    }

    /**
     * Converts this map to its "original" form.
     * @return The original form.
     */
    public Map<String, Stat<?>> toOriginals() {
        Map<String, Stat<?>> originals = new LinkedHashMap<>();
        forEach((key, stat) -> originals.put(key, stat.stat()));
        return originals;
    }

    /**
     * Creates a stat map from its "original" form. This does not describe the values that the stat instances contain.
     * @param originals The original form.
     * @return The stat map.
     */
    public static StatMap fromOriginals(Map<String, Stat<?>> originals) {
        StatMap map = new StatMap();
        originals.forEach((key, stat) -> map.put(key, stat.inst()));
        return map;
    }
}
