package com.gitlab.aecsocket.unifiedframework.core.stat;

import org.jetbrains.annotations.Nullable;

import java.util.function.Function;

/**
 * Holds a {@link Stat} and a modifier function.
 * @param <T> The type of value this instance's stat accepts.
 */
public final class StatInstance<T> {
    private final Stat<T> stat;
    private @Nullable final Function<T, T> modifier;

    public StatInstance(Stat<T> stat, @Nullable Function<T, T> modifier) {
        this.stat = stat;
        this.modifier = modifier;
    }

    public StatInstance(StatInstance<T> o) {
        stat = o.stat;
        modifier = o.modifier;
    }

    public Stat<T> stat() { return stat; }
    public @Nullable Function<T, T> modifier() { return modifier; }

    public StatInstance<T> withModifier(Function<T, T> modifier) {
        return new StatInstance<>(stat, modifier);
    }

    /**
     * Gets the raw value of this instance.
     * @return The raw value.
     */
    public T raw() { return applyModifier(stat, modifier, null); }

    /**
     * Gets the value or default of this instance.
     * @return The value.
     */
    public T get() { return empty() ? stat.defaultValue() : raw(); }

    /**
     * Gets if this instance is considered empty.
     * @return If this instance is empty.
     */
    public boolean empty() { return modifier == null; }

    /**
     * Applies the modifier based on a previous result.
     * @param result The previous result.
     * @return The value.
     */
    public T applyModifier(T result) { return applyModifier(stat, modifier, result); }

    /**
     * Applies a separate modifier along with a previous result.
     * @param modifier The modifier.
     * @param result The previous result.
     * @return The value.
     */
    public T applyModifier(@Nullable Function<T, T> modifier, T result) { return applyModifier(stat, modifier, result); }

    /**
     * Chains this modifier with another's, producing a new StatInstance.
     * <p>
     * If the next modifier is null, it will return this instance instead.
     * @param modifier The next modifier.
     * @return The new StatInstance.
     */
    public StatInstance<T> modify(@Nullable Function<T, T> modifier) {
        if (modifier == null) return this;
        T result = applyModifier(modifier, raw());
        return new StatInstance<>(stat, __ -> result);
    }

    /**
     * Chains this instance's modifier with another instance's, producing a new StatInstance.
     * <p>
     * If the next modifier is null, it will return this instance instead.
     * @param other The next StatInstance.
     * @return The new StatInstance.
     */
    public StatInstance<T> modify(StatInstance<T> other) {
        return modify(other.modifier);
    }

    /**
     * Applies a modifier using a Stat and previous result.
     * @param stat The Stat.
     * @param modifier The modifier.
     * @param result The previous result.
     * @param <T> The type of value held by the Stat.
     * @return The value.
     */
    public static <T> T applyModifier(Stat<T> stat, @Nullable Function<T, T> modifier, T result) {
        if (modifier == null)
            return stat.preModify(result);
        return stat.postModify(modifier.apply(stat.preModify(result)));
    }

    @Override
    public String toString() {
        String modString = null;
        if (modifier != null) {
            modString = modifier.toString();
            if (modString.endsWith("@" + String.format("%x", System.identityHashCode(modifier))))
                modString = null;
            else
                modString = modifier.toString();
        }
        if (modString == null)
            return stat + ": " + stat.toString(get());
        else
            return stat + ": " + stat.toString(get()) + " <" + modifier + ">";
    }
}
