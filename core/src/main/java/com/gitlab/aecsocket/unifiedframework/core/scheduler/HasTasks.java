package com.gitlab.aecsocket.unifiedframework.core.scheduler;

public interface HasTasks {
    void runTasks(Scheduler scheduler);
}
