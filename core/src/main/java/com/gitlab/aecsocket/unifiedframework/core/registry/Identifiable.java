package com.gitlab.aecsocket.unifiedframework.core.registry;

import java.util.Collection;
import java.util.Collections;

/**
 * An object with an ID and which can be stored in a {@link Registry}.
 */
public interface Identifiable {
    /**
     * Gets this object's ID for storing in a {@link Registry}.
     * @return The ID.
     */
    String id();

    /**
     * Sets this object's ID for storing in a {@link Registry}.
     * @param id The ID.
     */
    void id(String id);

    /**
     * Gets a collection of other Identifiable IDs which this instance requires.
     * @return The collection of IDs.
     */
    default Collection<String> dependencies() { return Collections.emptySet(); }

    /**
     * Resolves this object when provided with a {@link ResolutionContext}.
     * @param context The context.
     * @throws ResolutionException If a dependency could not be satisfied.
     */
    default void resolve(ResolutionContext context) throws ResolutionException {}

    /**
     * Validates the object.
     * @throws ValidationException If validation failed.
     */
    default void validate() throws ValidationException {}
}
