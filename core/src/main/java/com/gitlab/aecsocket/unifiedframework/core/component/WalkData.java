package com.gitlab.aecsocket.unifiedframework.core.component;

import java.util.Optional;

public class WalkData {
    private final Slot slot;
    private boolean stopped;

    public WalkData(Slot slot) {
        this.slot = slot;
    }

    public <S extends Slot> S slot() {
        @SuppressWarnings("unchecked")
        S s = (S) slot;
        return s;
    }

    public <C extends Component> C component() { return slot.get(); }
    public <C extends Component> Optional<C> optComponent() { return Optional.ofNullable(slot.get()); }

    public boolean stopped() { return stopped; }
    public void stopped(boolean stopped) { this.stopped = stopped; }
    public void stop() { stopped = true; }

    public static class Depthed extends WalkData {
        private final int depth;

        public Depthed(Slot slot, int depth) {
            super(slot);
            this.depth = depth;
        }

        public int depth() { return depth; }
    }

    public static class Pathed extends Depthed {
        private final String[] path;

        public Pathed(Slot slot, String[] path) {
            super(slot, path.length - 1);
            this.path = path;
        }

        public String[] path() { return path; }
        public String key() { return path[path.length - 1]; }
    }
}
