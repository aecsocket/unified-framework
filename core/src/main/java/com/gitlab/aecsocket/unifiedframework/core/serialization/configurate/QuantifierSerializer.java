package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.util.Quantifier;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public class QuantifierSerializer<T> implements TypeSerializer<Quantifier<T>>, ConfigurateSerializer {
    @Override
    public void serialize(Type type, @Nullable Quantifier<T> obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.appendListNode().set(obj.get());
            node.appendListNode().set(obj.getAmount());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Quantifier<T> deserialize(Type type, ConfigurationNode node) throws SerializationException {
        List<? extends ConfigurationNode> list = asList(node, type, "object", "amount");
        Type vType = ((ParameterizedType) type).getActualTypeArguments()[0];
        return new Quantifier<>(
                (T) list.get(0).get(vType),
                list.get(1).getInt()
        );
    }
}
