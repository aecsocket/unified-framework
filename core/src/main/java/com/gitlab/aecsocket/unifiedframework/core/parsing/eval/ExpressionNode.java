package com.gitlab.aecsocket.unifiedframework.core.parsing.eval;

public interface ExpressionNode<V extends NodeVisitor<?>> {
    int type();
    ExpressionNode<V> accept(V visitor);
}
