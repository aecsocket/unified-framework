package com.gitlab.aecsocket.unifiedframework.core.parsing.math.term;

import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathExpressionNode;

public class Term {
    private final MathExpressionNode expression;
    private final boolean positive;

    public Term(MathExpressionNode expression, boolean positive) {
        this.expression = expression;
        this.positive = positive;
    }

    public MathExpressionNode expression() { return expression; }
    public boolean positive() { return positive; }
}
