package com.gitlab.aecsocket.unifiedframework.core.parsing.math.term;

import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathVisitor;

public class Multiplication extends Sequence {
    @Override public Multiplication add(MathExpressionNode expression, boolean positive) { return (Multiplication) super.add(expression, positive); }

    @Override public int type() { return MathExpressionNode.MULTIPLICATION; }
    @Override public Multiplication accept(MathVisitor visitor) { return (Multiplication) super.accept(visitor); }

    @Override
    public double value() {
        double result = 1d;
        for (Term term : terms) {
            if (term.positive())
                result *= term.expression().value();
            else
                result /= term.expression().value();
        }
        return result;
    }
}
