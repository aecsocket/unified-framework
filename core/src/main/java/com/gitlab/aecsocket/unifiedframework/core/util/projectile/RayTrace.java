package com.gitlab.aecsocket.unifiedframework.core.util.projectile;

import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;

public class RayTrace<C> {
    private final C collided;
    private final Vector3D position;
    private final Vector3D collisionNormal;

    public RayTrace(C collided, Vector3D position, Vector3D collisionNormal) {
        this.collided = collided;
        this.position = position;
        this.collisionNormal = collisionNormal;
    }

    public C collided() { return collided; }
    public Vector3D position() { return position; }
    public Vector3D collisionNormal() { return collisionNormal; }
}
