package com.gitlab.aecsocket.unifiedframework.core.util.descriptor;

import java.text.DecimalFormat;

public abstract class NumberDescriptor<N extends Number> extends Descriptor<N> {
    public NumberDescriptor(N value, Operation operation, Suffix suffix) {
        super(value, operation, suffix);
    }

    public NumberDescriptor(N value, Operation operation) {
        super(value, operation);
    }

    public NumberDescriptor(N value) {
        super(value);
    }

    public abstract N toGeneric(double v);
    public abstract double toDouble(N v);

    @Override
    public N apply(N base) {
        if (base == null || value == null) return base;
        return toGeneric(apply(toDouble(base), toDouble(value)));
    }

    @Override
    public N apply() { return apply(toGeneric(0d)); }

    public String toString(DecimalFormat format) { return toString(toDouble(value), format); }
    @Override public String toString() { return toString(toDouble(value)); }

    public static final class Byte extends NumberDescriptor<java.lang.Byte> {
        public Byte(java.lang.Byte value, Operation operation, Suffix suffix) { super(value, operation, suffix); }
        public Byte(java.lang.Byte value, Operation operation) { super(value, operation); }
        public Byte(java.lang.Byte value) { super(value); }
        @Override public java.lang.Byte toGeneric(double v) { return (byte) v; }
        @Override public double toDouble(java.lang.Byte v) { return (double) v; }
    }

    public static final class Short extends NumberDescriptor<java.lang.Short> {
        public Short(java.lang.Short value, Operation operation, Suffix suffix) { super(value, operation, suffix); }
        public Short(java.lang.Short value, Operation operation) { super(value, operation); }
        public Short(java.lang.Short value) { super(value); }
        @Override public java.lang.Short toGeneric(double v) { return (short) v; }
        @Override public double toDouble(java.lang.Short v) { return (double) v; }
    }

    public static final class Integer extends NumberDescriptor<java.lang.Integer> {
        public Integer(java.lang.Integer value, Operation operation, Suffix suffix) { super(value, operation, suffix); }
        public Integer(java.lang.Integer value, Operation operation) { super(value, operation); }
        public Integer(java.lang.Integer value) { super(value); }
        @Override public java.lang.Integer toGeneric(double v) { return (int) v; }
        @Override public double toDouble(java.lang.Integer v) { return (double) v; }
    }

    public static final class Long extends NumberDescriptor<java.lang.Long> {
        public Long(java.lang.Long value, Operation operation, Suffix suffix) { super(value, operation, suffix); }
        public Long(java.lang.Long value, Operation operation) { super(value, operation); }
        public Long(java.lang.Long value) { super(value); }
        @Override public java.lang.Long toGeneric(double v) { return (long) v; }
        @Override public double toDouble(java.lang.Long v) { return (double) v; }
    }

    public static final class Float extends NumberDescriptor<java.lang.Float> {
        public Float(java.lang.Float value, Operation operation, Suffix suffix) { super(value, operation, suffix); }
        public Float(java.lang.Float value, Operation operation) { super(value, operation); }
        public Float(java.lang.Float value) { super(value); }
        @Override public java.lang.Float toGeneric(double v) { return (float) v; }
        @Override public double toDouble(java.lang.Float v) { return (double) v; }
    }

    public static final class Double extends NumberDescriptor<java.lang.Double> {
        public Double(java.lang.Double value, Operation operation, Suffix suffix) { super(value, operation, suffix); }
        public Double(java.lang.Double value, Operation operation) { super(value, operation); }
        public Double(java.lang.Double value) { super(value); }
        @Override public java.lang.Double toGeneric(double v) { return v; }
        @Override public double toDouble(java.lang.Double v) { return v; }
    }

    public static Integer of(int value) { return new Integer(value); }
    public static Long of(long value) { return new Long(value); }
    public static Float of(float value) { return new Float(value); }
    public static Double of(double value) { return new Double(value); }
    public static Byte of(byte value) { return new Byte(value); }
    public static Short of(short value) { return new Short(value); }
}
