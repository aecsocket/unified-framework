package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.util.color.ColorModifier;
import com.gitlab.aecsocket.unifiedframework.core.util.color.HSV;
import com.gitlab.aecsocket.unifiedframework.core.util.color.RGBA;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.List;

public class ColorModifierSerializer implements TypeSerializer<ColorModifier>, ConfigurateSerializer {
    public static final BiMap<String, Class<?>> TYPES = new ImmutableBiMap.Builder<String, Class<?>>()
            .put("rgba", RGBA.class)
            .put("hsv", HSV.class)
            .build();
    public static final ColorModifierSerializer INSTANCE = new ColorModifierSerializer();

    @Override
    public void serialize(Type type, @Nullable ColorModifier obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            Object value = obj.value();
            node
                    .appendListNode().set(TYPES.inverse().get(value.getClass()))
                    .appendListNode().set(value);
        }
    }

    @Override
    public ColorModifier deserialize(Type type, ConfigurationNode node) throws SerializationException {
        List<? extends ConfigurationNode> list = asList(node, type, "type", "value");
        String sType = list.get(0).getString();
        Class<?> valueType = TYPES.get(sType);
        if (valueType == null)
            throw new SerializationException("Invalid value type `" + sType + "`: must be [" + String.join(", ", TYPES.keySet()) + "]");
        return ColorModifier.wrap(list.get(1).get(valueType));
    }
}
