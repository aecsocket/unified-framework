package com.gitlab.aecsocket.unifiedframework.core.locale;

import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.LocaleSerializer;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.TranslationsSerializer;
import com.gitlab.aecsocket.unifiedframework.core.util.Utils;
import com.gitlab.aecsocket.unifiedframework.core.util.result.LoadEntry;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.ConfigurationOptions;
import org.spongepowered.configurate.hocon.HoconConfigurationLoader;
import org.spongepowered.configurate.loader.ConfigurationLoader;
import org.spongepowered.configurate.serialize.TypeSerializerCollection;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

/**
 * Allows loading translations for {@link Localization}s.
 */
public final class TranslationsLoader {
    /** The loader options used to deserialize files. */
    public static final ConfigurationOptions LOADER_OPTIONS = ConfigurationOptions.defaults()
            .serializers(
                    TypeSerializerCollection.defaults().childBuilder()
                            .register(Locale.class, new LocaleSerializer())
                            .register(Translations.class, new TranslationsSerializer())
                            .build()
            );

    private TranslationsLoader() {}

    /**
     * Load into a locale manager using an arbitrary configuration loader.
     * @param root The root file.
     * @param localization The localization object.
     * @param loaderFunction The configuration loader provider.
     * @return The result.
     */
    public static List<LoadEntry<Translations, ConfigurateException>> load(File root, Localization<?> localization, Function<File, ConfigurationLoader<?>> loaderFunction) {
        List<LoadEntry<Translations, ConfigurateException>> result = new ArrayList<>();
        Utils.applyRecursively(root, (file, path) -> {
            ConfigurationLoader<?> loader = loaderFunction.apply(file);
            try {
                ConfigurationNode node = loader.load();
                Translations translation = node.get(Translations.class);

                if (translation != null) {
                    localization.add(translation);
                    result.add(LoadEntry.of(path, translation));
                }
            } catch (ConfigurateException e) {
                result.add(LoadEntry.of(path, e));
            }
        });
        return result;
    }

    /**
     * Load into a locale manager using a HOCON configuration loader.
     * @param root The root file.
     * @param localization The localization object.
     * @return The result.
     */
    public static List<LoadEntry<Translations, ConfigurateException>> hocon(File root, Localization<?> localization) {
        List<LoadEntry<Translations, ConfigurateException>> r = load(root, localization, file -> HoconConfigurationLoader.builder()
                .file(file)
                .defaultOptions(LOADER_OPTIONS)
                .build()
        );
        return r;
    }
}
