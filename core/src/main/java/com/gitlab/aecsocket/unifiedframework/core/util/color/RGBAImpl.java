package com.gitlab.aecsocket.unifiedframework.core.util.color;

/* package */ class RGBAImpl implements RGBA {
    private final int value;

    public RGBAImpl(int value) {
        this.value = value;
    }

    @Override public int value() { return value; }

    @Override public String toString() { return toString(Format.HEX); }
}
