package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.locale.Translations;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.BasicConfigurationNode;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.*;

/**
 * Allows proper deserialization of a {@link Translations}.
 */
public class TranslationsSerializer implements TypeSerializer<Translations>, ConfigurateSerializer {
    public static final TranslationsSerializer INSTANCE = new TranslationsSerializer();
    public static final String DELIMETER = ".";

    @Override
    public void serialize(Type type, @Nullable Translations obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            for (var entry : obj.values().entrySet()) {
                String value = entry.getValue();
                ConfigurationNode child = BasicConfigurationNode.root();
                String[] lines = value.split("\n");
                if (lines.length == 1)
                    child.set(value);
                else
                    child.setList(String.class, Arrays.asList(lines));
                node.node(entry.getKey()).set(child);
            }
            node.node("locale").set(obj.locale());
        }
    }

    private void deserialize(Map<Object, ? extends ConfigurationNode> nodes, Map<String, String> translations, Type type, String[] path) throws SerializationException {
        for (Map.Entry<Object, ? extends ConfigurationNode> entry : nodes.entrySet()) {
            String key = entry.getKey().toString();
            ConfigurationNode child = entry.getValue();

            String[] newPath = Arrays.copyOf(path, path.length + 1);
            newPath[path.length] = key;

            if (child.isMap())
                deserialize(new HashMap<>(asMap(child, type)), translations, type, newPath);
            else if (child.isList()) {
                List<String> lines = child.getList(String.class);
                if (lines == null)
                    throw new SerializationException(child, type, "Must be list of strings");
                translations.put(String.join(DELIMETER, newPath), String.join("\n", lines));
            } else
                translations.put(String.join(DELIMETER, newPath), child.getString());
        }
    }

    @Override
    public Translations deserialize(Type type, ConfigurationNode node) throws SerializationException {
        Map<Object, ? extends ConfigurationNode> nodes = new HashMap<>(asMap(node, type));

        Map<String, String> translations = new HashMap<>();
        Locale locale = get(nodes, "locale", node, type).get(Locale.class);
        nodes.remove("locale");

        deserialize(nodes, translations, type, new String[0]);

        return new Translations(locale, translations);
    }
}
