package com.gitlab.aecsocket.unifiedframework.core.util.vector;

import java.util.Objects;

/**
 * Immutable data class holding an x, y, z integer vector.
 */
public class Vector3I {
    public static final Vector3I ZERO = new Vector3I();

    private final int x;
    private final int y;
    private final int z;

    public Vector3I(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3I(int v) { this(v, v, v); }

    public Vector3I() { this(0, 0, 0); }

    public int x() { return x; }
    public Vector3I x(int x) { return new Vector3I(x, y, z); }

    public int y() { return y; }
    public Vector3I y(int y) { return new Vector3I(x, y, z); }

    public int z() { return z; }
    public Vector3I z(int z) { return new Vector3I(x, y, z); }

    public Vector3I add(int x, int y, int z) { return new Vector3I(this.x + x, this.y + y, this.z + z); }
    public Vector3I add(Vector3I o) { return add(o.x, o.y, o.z); }
    public Vector3I add(int v) { return add(v, v, v); }

    public Vector3I subtract(int x, int y, int z) { return add(-x, -y, -z); }
    public Vector3I subtract(Vector3I o) { return subtract(o.x, o.y, o.z); }
    public Vector3I subtract(int v) { return subtract(v, v, v); }

    public Vector3I multiply(int x, int y, int z) { return new Vector3I(this.x * x, this.y * y, this.z * z); }
    public Vector3I multiply(Vector3I o) { return multiply(o.x, o.y, o.z); }
    public Vector3I multiply(int v) { return multiply(v, v, v); }

    public Vector3I divide(int x, int y, int z) { return new Vector3I(this.x / x, this.y / y, this.z / z); }
    public Vector3I divide(Vector3I o) { return divide(o.x, o.y, o.y); }
    public Vector3I divide(int v) { return divide(v, v, v); }

    /**
     * Gets <code>abs(x) + abs(y) + abs(z)</code>.
     * @return The result.
     */
    public int manhattanLength() { return Math.abs(x) + Math.abs(y) + Math.abs(z); }

    /**
     * Gets <code>sqrt((x*x) + (y*y))</code>.
     * @return The result.
     */
    public double length() { return Math.sqrt((x*x) + (y*y) + (z*z)); }

    public Vector3I crossProduct(Vector3I o) {
        return new Vector3I(
                y * o.z - o.y * z,
                z * o.x - o.z * x,
                x * o.y - o.x * y
        );
    }

    public int dot(Vector3I o) { return (x*o.x) + (y*o.y) + (z*o.z); }

    public Vector3D toDoubles() { return new Vector3D(x, y, z); }

    @Override public String toString() { return "(" + x + ", " + y + ", " + z + ")"; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector3I vector3D = (Vector3I) o;
        return vector3D.x == x && vector3D.y == y && vector3D.z == z;
    }

    @Override
    public int hashCode() { return Objects.hash(x, y, z); }
}
