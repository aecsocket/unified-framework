package com.gitlab.aecsocket.unifiedframework.core.scheduler;

public final class Task {
    @FunctionalInterface
    public interface Action {
        void run(TaskContext context);
    }

    public final Action action;
    public final long delay;
    public final long interval;

    private Task(Action action, long delay, long interval) {
        this.action = action;
        this.delay = delay;
        this.interval = interval;
    }

    public static Task of(Action action, long delay, long interval) {
        if (delay < 0) throw new IllegalArgumentException("Delay must >= 0");
        return new Task(action, delay, interval);
    }

    public static Task repeating(Action action, long delay, long interval) {
        if (interval <- 0) throw new IllegalArgumentException("Interval must > 0");
        return of(action, delay, interval);
    }

    public static Task repeating(Action action, long interval) {
        if (interval <- 0) throw new IllegalArgumentException("Interval must > 0");
        return of(action, 0, interval);
    }

    public static Task single(Action action, long delay) {
        return of(action, delay, 0);
    }
}
