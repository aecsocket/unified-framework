package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.vector;

import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.ConfigurateSerializer;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2I;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class Vector2ISerializer implements TypeSerializer<Vector2I>, ConfigurateSerializer {
    public static final Vector2ISerializer INSTANCE = new Vector2ISerializer();

    @Override
    public void serialize(Type type, @Nullable Vector2I obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            if (obj.x() == obj.y())
                node.set(obj.x());
            else
                node.setList(Integer.class, Arrays.asList(
                        obj.x(), obj.y()
                ));
        }
    }

    @Override
    public Vector2I deserialize(Type type, ConfigurationNode node) throws SerializationException {
        if (node.isList()) {
            List<? extends ConfigurationNode> list = asList(node, Vector2I.class, "x", "y");
            return new Vector2I(
                    list.get(0).getInt(),
                    list.get(1).getInt()
            );
        } else
            return new Vector2I(node.getInt());
    }
}
