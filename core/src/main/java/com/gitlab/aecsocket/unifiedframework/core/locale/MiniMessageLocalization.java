package com.gitlab.aecsocket.unifiedframework.core.locale;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;

import java.util.Arrays;
import java.util.Locale;

public class MiniMessageLocalization extends AbstractLocalization<Component> {
    public static final String DEFAULT_FALLBACK = "<gen_key> <args>";

    private MiniMessage miniMessage = MiniMessage.get();
    private Component join;
    private String fallback;

    public MiniMessage miniMessage() { return miniMessage; }
    public void miniMessage(MiniMessage miniMessage) { this.miniMessage = miniMessage; }

    public Component join() { return join; }
    public void join(Component join) { this.join = join; }

    public String fallback() { return fallback == null ? DEFAULT_FALLBACK : fallback; }
    public boolean hasFallback() { return fallback != null; }
    public void fallback(String fallback) { this.fallback = fallback; }

    @Override
    protected Component genFallback(Locale locale, String key, Object... args) {
        return format(locale, fallback(),
                        "gen_key", key,
                        "args", Arrays.toString(args)
        );
    }

    @Override protected Component format(Locale locale, String value, Object... args) {
        return miniMessage.parse(value, args);
    }
}
