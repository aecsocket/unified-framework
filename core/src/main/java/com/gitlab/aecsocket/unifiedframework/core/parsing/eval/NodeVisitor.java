package com.gitlab.aecsocket.unifiedframework.core.parsing.eval;

public interface NodeVisitor<N extends ExpressionNode<?>> {
    void visit(N node);
}
