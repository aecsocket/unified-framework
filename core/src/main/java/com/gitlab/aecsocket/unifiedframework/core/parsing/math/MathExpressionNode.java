package com.gitlab.aecsocket.unifiedframework.core.parsing.math;

import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.ExpressionNode;

public interface MathExpressionNode extends ExpressionNode<MathVisitor> {
    int CONSTANT = 1;
    int VARIABLE = 2;
    int ADDITION = 3;
    int MULTIPLICATION = 4;
    int EXPONENT = 5;
    int FUNCTION = 6;

    MathExpressionNode accept(MathVisitor visitor);
    double value();
    default MathExpressionNode set(String name, double value) {
        accept(new SetVariableVisitor(name, value));
        return this;
    }
}
