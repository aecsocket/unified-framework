package com.gitlab.aecsocket.unifiedframework.core.stat;

import io.leangen.geantyref.TypeToken;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.function.Function;

/**
 * An immutable class, used in a {@link StatMap} to define how a stat is saved and modified.
 * @param <T> The type of value this stat accepts.
 */
public interface Stat<T> {
    /**
     * Gets the type for serialization/deserialization.
     * @return The type.
     */
    TypeToken<T> valueType();

    /**
     * Gets the default value if no other is present in a {@link StatInstance}.
     * @return The default value.
     */
    T defaultValue();

    /**
     * Gets the string representation of a value.
     * @param value The value.
     * @return The string representation.
     */
    default @NotNull String toString(T value) {
        if (value == null)
            return "null";
        if (value.getClass().isArray()) {
            Object[] arr = (Object[]) value;
            return Arrays.toString(arr);
        }
        return value.toString();
    }

    /**
     * Pre-modifies a value before it is processed by a modifier function.
     * @param value The last value.
     * @return The new value.
     */
    default T preModify(T value) { return value; }

    /**
     * Modifies a value after it has been processed by a modifier function.
     * @param result The last result.
     * @return The new result.
     */
    default T postModify(T result) { return result; }

    /**
     * Creates a new {@link StatInstance} using this stat, with a modifier.
     * @param modifier The modifier to pass.
     * @return The stat instance.
     */
    default StatInstance<T> inst(Function<T, T> modifier) { return new StatInstance<>(this, modifier); }

    /**
     * Creates a new {@link StatInstance} using this stat, with a set value.
     * @param value The value that the modifier will apply to.
     * @return The stat instance.
     */
    default StatInstance<T> inst(T value) { return new StatInstance<>(this, __ -> value); }

    /**
     * Creates a new {@link StatInstance} using this stat.
     * @return The stat instance.
     */
    default StatInstance<T> inst() { return new StatInstance<>(this, null); }
}
