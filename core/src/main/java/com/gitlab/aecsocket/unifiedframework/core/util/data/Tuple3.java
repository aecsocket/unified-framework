package com.gitlab.aecsocket.unifiedframework.core.util.data;

public final class Tuple3<A, B, C> {
    private final A a;
    private final B b;
    private final C c;

    private Tuple3(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public A a() { return a; }
    public B b() { return b; }
    public C c() { return c; }

    @Override public String toString() { return "(" + a + ", " + b + ", " + c + ")"; }

    public static <A, B, C> Tuple3<A, B, C> of(A a, B b, C c) { return new Tuple3<>(a, b, c); }
}
