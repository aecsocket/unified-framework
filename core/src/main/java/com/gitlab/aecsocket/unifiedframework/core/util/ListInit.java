package com.gitlab.aecsocket.unifiedframework.core.util;

import java.util.List;

public class ListInit<E, T extends List<E>> {
    private final T list;
    private ListInit(T list) { this.list = list; }
    public ListInit<E, T> init(E element) { list.add(element); return this; }
    public ListInit<E, T> init(List<E> list) { this.list.addAll(list); return this; }
    public T get() { return list; }

    public static <E, T extends List<E>> ListInit<E, T> of(T list) { return new ListInit<>(list); }
}
