package com.gitlab.aecsocket.unifiedframework.core.parsing.eval;

import com.gitlab.aecsocket.unifiedframework.core.parsing.lexing.Token;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class TokenEvaluator<N extends ExpressionNode<?>> {
    private final Map<Integer, Supplier<N>> types = new HashMap<>();
    private Supplier<N> defaultBranch;

    public TokenEvaluator<N> add(int token, Supplier<N> result) { types.put(token, result); return this; }
    public TokenEvaluator<N> epsilon(Supplier<N> defaultBranch) { this.defaultBranch = defaultBranch; return this; }

    public N run(Token lookahead, String expected) {
        Supplier<N> function = types.get(lookahead.id());
        if (function != null)
            return function.get();
        else {
            if (defaultBranch == null)
                throw new IllegalTokenException(lookahead.sequence(), expected);
            return defaultBranch.get();
        }
    }

    public N run(Token lookahead) { return run(lookahead, null); }
}
