package com.gitlab.aecsocket.unifiedframework.core.stat.serialization;

import com.gitlab.aecsocket.unifiedframework.core.stat.Stat;
import org.spongepowered.configurate.ConfigurationNode;

import java.util.function.Function;

/**
 * A stat that can be serialized through the Configurate library.
 * @param <T> The held value type.
 */
public interface ConfigurateStat<T> extends Stat<T> {
    Function<T, T> getModFunction(ConfigurationNode node) throws FunctionCreationException;
}
