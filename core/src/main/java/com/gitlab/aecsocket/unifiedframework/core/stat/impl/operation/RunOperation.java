package com.gitlab.aecsocket.unifiedframework.core.stat.impl.operation;

import java.util.Map;

public interface RunOperation<T> {
    T run(T base, Map<String, Object> passedArgs);
}
