package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.descriptor;

import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Descriptor;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Vector3DDescriptor;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import io.leangen.geantyref.TypeToken;

/**
 * Allows proper deserialization of a {@link Vector3DDescriptor}.
 */
public class Vector3DDescriptorSerializer extends DescriptorSerializer<Vector3D, Vector3DDescriptor> {
    public static final Vector3DDescriptorSerializer INSTANCE = new Vector3DDescriptorSerializer();

    @Override public TypeToken<Vector3D> valueType() { return new TypeToken<>(){}; }

    @Override
    protected Vector3DDescriptor create(Vector3D value) {
        return new Vector3DDescriptor(value);
    }

    @Override
    protected Vector3DDescriptor create(Vector3D value, Descriptor.Operation operation, Descriptor.Suffix suffix) {
        return new Vector3DDescriptor(value, operation, suffix);
    }
}
