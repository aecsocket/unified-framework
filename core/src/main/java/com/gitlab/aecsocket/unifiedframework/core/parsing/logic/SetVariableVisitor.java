package com.gitlab.aecsocket.unifiedframework.core.parsing.logic;

import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.node.Variable;

public class SetVariableVisitor implements LogicVisitor {
    private final String name;
    private final boolean value;

    public SetVariableVisitor(String name, boolean value) {
        this.name = name;
        this.value = value;
    }

    public String name() { return name; }
    public boolean value() { return value; }

    @Override
    public void visit(LogicExpressionNode node) {
        if (node instanceof Variable) {
            Variable var = (Variable) node;
            if (var.name().equals(name))
                var.value(value);
        }
    }
}
