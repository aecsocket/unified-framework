package com.gitlab.aecsocket.unifiedframework.core.stat.impl.operation;

import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.FunctionCreationException;

import java.lang.reflect.Type;

public interface ArgumentProvider {
    <A> A get(int i, Type type) throws FunctionCreationException;
}
