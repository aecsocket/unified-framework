package com.gitlab.aecsocket.unifiedframework.core.util.descriptor;

import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;

public class Vector3DDescriptor extends Descriptor<Vector3D> {
    public Vector3DDescriptor(Vector3D value, Operation operation, Suffix suffix) {
        super(value, operation, suffix);
    }

    public Vector3DDescriptor(Vector3D value, Operation operation) {
        super(value, operation);
    }

    public Vector3DDescriptor(Vector3D value) {
        super(value);
    }

    @Override
    public Vector3D apply(Vector3D base) {
        if (base == null || value == null) return base;
        return new Vector3D(
                apply(base.x(), value.x()),
                apply(base.y(), value.y()),
                apply(base.z(), value.z())
        );
    }

    @Override public Vector3D apply() { return apply(new Vector3D()); }
    @Override public String toString() {
        return Double.compare(value.x(), value.y()) == 0 && Double.compare(value.x(), value.z()) == 0
                ? toString(value.x())
                : toString(value.x()) + ", " + toString(value.y()) + ", " + toString(value.z());
    }
}
