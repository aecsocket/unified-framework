package com.gitlab.aecsocket.unifiedframework.core.util.result;

import com.gitlab.aecsocket.unifiedframework.core.util.log.LabelledLogger;
import com.gitlab.aecsocket.unifiedframework.core.util.log.LogLevel;

public class LoggingEntry {
    private final LogLevel level;
    private final Throwable exception;
    private final String message;

    public LoggingEntry(LogLevel level, Throwable exception, String message) {
        this.level = level;
        this.exception = exception;
        this.message = message;
    }

    public LogLevel level() { return level; }
    public Throwable exception() { return exception; }
    public String message() { return message; }

    public String infoBasic() { return LabelledLogger.infoBasic(exception, message); }
    public String infoDetail() { return LabelledLogger.infoDetail(exception, message); }

    public void logBasic(LabelledLogger logger) { logger.logBasic(level, exception, message); }
    public void logDetail(LabelledLogger logger) { logger.logDetail(level, exception, message); }

    public static LoggingEntry of(LogLevel level, Throwable detail, String message, Object... args) {
        return new LoggingEntry(level, detail, String.format(message, args));
    }

    public static LoggingEntry of(LogLevel level, String message, Object... args) {
        return of(level, null, message, args);
    }
}
