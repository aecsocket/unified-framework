package com.gitlab.aecsocket.unifiedframework.core.util.data;

public final class Tuple4<A, B, C, D> {
    private final A a;
    private final B b;
    private final C c;
    private final D d;

    private Tuple4(A a, B b, C c, D d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public A a() { return a; }
    public B b() { return b; }
    public C c() { return c; }
    public D d() { return d; }

    @Override public String toString() { return "(" + a + ", " + b + ", " + c + ", " + d + ")"; }

    public static <A, B, C, D> Tuple4<A, B, C, D> of(A a, B b, C c, D d) { return new Tuple4<>(a, b, c, d); }
}
