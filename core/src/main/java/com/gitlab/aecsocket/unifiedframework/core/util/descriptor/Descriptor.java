package com.gitlab.aecsocket.unifiedframework.core.util.descriptor;

import com.gitlab.aecsocket.unifiedframework.core.util.MapInit;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public abstract class Descriptor<T> {
    public interface NumberFunction {
        double apply(double base, double value, Suffix suffix);
    }

    public enum Operation {
        SET         ("=", false, (base, value, suffix) -> value),
        ADD         ("+", true, (base, value, suffix) -> suffix == Suffix.PERCENTAGE ? base * (1 + value) : base + value, Suffix.PERCENTAGE),
        SUBTRACT    ("-", true, (base, value, suffix) -> suffix == Suffix.PERCENTAGE ? base * (1 - value) : base - value, Suffix.PERCENTAGE),
        MULTIPLY    ("x", true, (base, value, suffix) -> base * value);

        private static final Map<String, Operation> bySymbol = new HashMap<>();
        private static final Map<Operation, Operation> opposite = MapInit.of(new HashMap<Operation, Operation>())
                .init(Operation.ADD, Operation.SUBTRACT)
                .init(Operation.SUBTRACT, Operation.ADD)
                .get();

        static {
            for (Operation operation : values())
                bySymbol.put(operation.symbol, operation);
        }

        private final String symbol;
        private final boolean showSymbol;
        private final NumberFunction function;
        private final Suffix[] suffixes;

        Operation(String symbol, boolean showSymbol, NumberFunction function, Suffix... suffixes) {
            this.symbol = symbol;
            this.showSymbol = showSymbol;
            this.function = function;
            this.suffixes = suffixes;
        }

        public String symbol() { return symbol; }
        public boolean showSymbol() { return showSymbol; }
        public NumberFunction function() { return function; }
        public Suffix[] suffixes() { return suffixes; }

        public boolean compatible(Suffix suffix) {
            for (Suffix compatible : suffixes) {
                if (compatible == suffix)
                    return true;
            }
            return false;
        }

        public static Operation bySymbol(String symbol) {
            Operation result = bySymbol.get(symbol);
            if (result == null)
                throw new IllegalArgumentException("Unknown symbol " + symbol);
            return result;
        }

        public static Operation opposite(Operation operation) {
            return opposite.get(operation);
        }
    }

    public enum Suffix {
        PERCENTAGE  ("%", (val, op) -> val / 100);

        private static final Map<String, Suffix> bySymbol = new HashMap<>();

        static {
            for (Suffix suffix : values())
                bySymbol.put(suffix.symbol, suffix);
        }

        private final String symbol;
        private final BiFunction<Double, Operation, Double> function;

        Suffix(String symbol, BiFunction<Double, Operation, Double> function) {
            this.symbol = symbol;
            this.function = function;
        }

        public String symbol() { return symbol; }
        public BiFunction<Double, Operation, Double> function() { return function; }

        public static Suffix bySymbol(String symbol) {
            Suffix result = bySymbol.get(symbol);
            if (result == null)
                throw new IllegalArgumentException("Unknown symbol " + symbol);
            return result;
        }
    }

    protected static final DecimalFormat defaultFormat = new DecimalFormat("0.###");

    protected final T value;
    protected final Operation operation;
    protected final Suffix suffix;

    public Descriptor(T value, Operation operation, Suffix suffix) {
        this.value = value;
        this.operation = operation;
        this.suffix = suffix;
        if (suffix != null && !operation.compatible(suffix))
            throw new IllegalArgumentException("Suffix " + suffix.name() + " is not compatible with operation " + operation.name());
    }

    public Descriptor(T value, Operation operation) {
        this(value, operation, null);
    }

    public Descriptor(T value) {
        this(value, Operation.SET);
    }

    public T value() { return value; }
    public Operation operation() { return operation; }
    public Suffix suffix() { return suffix; }

    protected double apply(double base, double value) {
        return operation.function().apply(base, suffix == null ? value : suffix.function().apply(value, operation), suffix);
    }
    public abstract T apply(T base);
    public abstract T apply();

    public String toString(double n, DecimalFormat format) {
        StringBuilder result = new StringBuilder();
        String addSymbol = operation.showSymbol ? operation.symbol : "";
        if (n < 0) {
            Operation opposite = Operation.opposite(operation);
            if (opposite != null) {
                addSymbol = opposite.symbol;
                n = -n;
            }
        }
        result.append(addSymbol);

        result.append(format.format(n));

        if (suffix != null)
            result.append(suffix.symbol);

        return result.toString();
    }

    public String toString(double n) { return toString(n, defaultFormat); }
}
