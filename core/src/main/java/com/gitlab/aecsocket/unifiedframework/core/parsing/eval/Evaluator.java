package com.gitlab.aecsocket.unifiedframework.core.parsing.eval;

import com.gitlab.aecsocket.unifiedframework.core.parsing.lexing.Token;

import java.util.Deque;

public interface Evaluator<N extends ExpressionNode<?>> {
    N evaluate(Deque<Token> tokens);
}
