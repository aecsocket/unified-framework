package com.gitlab.aecsocket.unifiedframework.core.stat.impl.operation;

import java.util.Map;

public interface LoadOperation {
    void run(ArgumentProvider args, Object ctx, Map<String, Object> passed);
}
