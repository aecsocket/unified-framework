package com.gitlab.aecsocket.unifiedframework.core.stat.impl;

import com.gitlab.aecsocket.unifiedframework.core.stat.AbstractStat;
import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.ConfigurateStat;
import io.leangen.geantyref.TypeToken;
import org.spongepowered.configurate.ConfigurationNode;

import java.util.function.Function;

/**
 * A stat which holds a boolean.
 */
public class BooleanStat extends AbstractStat<Boolean> implements ConfigurateStat<Boolean> {
    public BooleanStat(Boolean defaultValue) { super(defaultValue); }
    public BooleanStat() {}
    @Override public TypeToken<Boolean> valueType() { return new TypeToken<>(){}; }

    @Override
    public Function<Boolean, Boolean> getModFunction(ConfigurationNode node) {
        return b -> node.getBoolean();
    }
}
