package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;

public class OptionalSerializer<T> implements TypeSerializer<Optional<T>> {
    @Override
    public void serialize(Type type, @Nullable Optional<T> obj, ConfigurationNode node) throws SerializationException {
        if (obj == null || obj.isEmpty()) node.set(null);
        else node.set(obj.get());
    }

    @Override
    @SuppressWarnings("unchecked")
    public Optional<T> deserialize(Type type, ConfigurationNode node) throws SerializationException {
        Type vType = ((ParameterizedType) type).getActualTypeArguments()[0];
        return Optional.ofNullable((T) node.get(vType));
    }
}
