package com.gitlab.aecsocket.unifiedframework.core.util.descriptor;

import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2D;

public class Vector2DDescriptor extends Descriptor<Vector2D> {
    public Vector2DDescriptor(Vector2D value, Operation operation, Suffix suffix) {
        super(value, operation, suffix);
    }

    public Vector2DDescriptor(Vector2D value, Operation operation) {
        super(value, operation);
    }

    public Vector2DDescriptor(Vector2D value) {
        super(value);
    }

    @Override
    public Vector2D apply(Vector2D base) {
        if (base == null || value == null) return base;
        return new Vector2D(
                apply(base.x(), value.x()),
                apply(base.y(), value.y())
        );
    }

    @Override public Vector2D apply() { return apply(new Vector2D()); }
    @Override public String toString() {
        return Double.compare(value.x(), value.y()) == 0
                ? toString(value.x())
                : toString(value.x()) + ", " + toString(value.y());
    }
}
