package com.gitlab.aecsocket.unifiedframework.core.util;

import com.google.common.base.Preconditions;
import com.google.common.collect.AbstractIterator;
import com.google.common.collect.UnmodifiableIterator;
import com.google.common.graph.Graph;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("UnstableApiUsage")
/**
 * Guava bruh moment
 */
public final class GraphUtils {
    private GraphUtils() {}

    /**
     * Topologically sort a graph's nodes.
     * @param graph The graph.
     * @param <N> The node type.
     * @return The topologically sorted nodes.
     */
    public static <N> Set<N> topologicallySortedNodes(Graph<N> graph) {
        // TODO: Do we want this method to be lazy or eager?
        return new TopologicallySortedNodes<>(graph);
    }

    private static class TopologicallySortedNodes<N> extends AbstractSet<N> {
        private final Graph<N> graph;

        private TopologicallySortedNodes(Graph<N> graph) {
            this.graph = Preconditions.checkNotNull(graph, "graph");
        }

        @Override
        public UnmodifiableIterator<N> iterator() {
            return new TopologicalOrderIterator<>(graph);
        }

        @Override
        public int size() {
            return graph.nodes().size();
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }
    }

    private static class TopologicalOrderIterator<N> extends AbstractIterator<N> {
        private final Graph<N> graph;
        private final Queue<N> roots;
        private final Map<N, Integer> nonRootsToInDegree;

        private TopologicalOrderIterator(Graph<N> graph) {
            this.graph = Preconditions.checkNotNull(graph, "graph");
            this.roots =
                    graph
                            .nodes()
                            .stream()
                            .filter(node -> graph.inDegree(node) == 0)
                            .collect(Collectors.toCollection(ArrayDeque::new));
            this.nonRootsToInDegree =
                    graph
                            .nodes()
                            .stream()
                            .filter(node -> graph.inDegree(node) > 0)
                            .collect(Collectors.toMap(node -> node, graph::inDegree, (a, b) -> a, HashMap::new));
        }

        @Override
        protected N computeNext() {
            // Kahn's algorithm
            if (!roots.isEmpty()) {
                N next = roots.remove();
                for (N successor : graph.successors(next)) {
                    int newInDegree = nonRootsToInDegree.get(successor) - 1;
                    nonRootsToInDegree.put(successor, newInDegree);
                    if (newInDegree == 0) {
                        nonRootsToInDegree.remove(successor);
                        roots.add(successor);
                    }
                }
                return next;
            }
            Preconditions.checkState(nonRootsToInDegree.isEmpty(), "graph has at least one cycle");
            return endOfData();
        }
    }
}
