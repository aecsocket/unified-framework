package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.util.color.RGBA;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class RGBASerializer implements TypeSerializer<RGBA>, ConfigurateSerializer {
    public static final RGBASerializer INSTANCE = new RGBASerializer(RGBA.Format.VALUE);

    private final RGBA.Format format;

    public RGBASerializer(@NotNull RGBA.Format format) {
        this.format = format;
    }

    public RGBA.Format format() { return format; }

    @Override
    public void serialize(Type type, @Nullable RGBA obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            switch (format) {
                case VALUE: node.set(obj.value());
                case HEX: node.set(String.format("#%x", obj.value()));
                case COMPONENTS: node.setList(int.class, Arrays.asList(obj.r(), obj.g(), obj.b(), obj.a()));
            }
        }
    }

    @Override
    public RGBA deserialize(Type type, ConfigurationNode node) throws SerializationException {
        if (node.isList()) {
            List<? extends ConfigurationNode> list = asList(node, type, "r", "g", "b", "a");
            return RGBA.of(
                    list.get(0).getInt(),
                    list.get(1).getInt(),
                    list.get(2).getInt(),
                    list.get(3).getInt()
            );
        }

        String hex = node.getString();
        if (hex != null && hex.startsWith("#")) {
            try {
                return RGBA.of((int) Long.parseLong(hex.substring(1), 16));
            } catch (NumberFormatException e) {
                throw new SerializationException(node, type, "Invalid hex color `" + hex + "`: must be in format `#rrggbbaa`", e);
            }
        }

        return RGBA.of(node.getInt());
    }
}
