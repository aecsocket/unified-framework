package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.util.color.RGBA;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.configurate.ConfigurationNode;

import java.lang.reflect.Type;

public class TextColorSerializer extends AbstractRGBSerializer<TextColor> {
    public static final TextColorSerializer INSTANCE = new TextColorSerializer();

    public TextColorSerializer(RGBA.@NotNull Format format) { super(format); }
    public TextColorSerializer() {}

    @Override protected int value(TextColor obj) { return obj.value(); }
    @Override protected TextColor of(int value) { return TextColor.color(value); }

    @Override
    protected TextColor fallback(Type type, ConfigurationNode node) {
        String name = node.getString();
        if (name != null) {
            NamedTextColor color = NamedTextColor.NAMES.value(name);
            if (color != null)
                return color;
        }
        return super.fallback(type, node);
    }
}
