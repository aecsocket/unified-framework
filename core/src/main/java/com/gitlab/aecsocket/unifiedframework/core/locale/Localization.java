package com.gitlab.aecsocket.unifiedframework.core.locale;

import java.util.Locale;
import java.util.Map;

public interface Localization<R> {
    Map<Locale, Translations> translations();
    Translations get(Locale locale);
    boolean has(Locale locale);

    void set(Translations translations) ;
    void set(Locale locale, String key, String... value);

    Translations add(Translations translations);
    Translations remove(Locale locale);
    void clear();

    Locale defaultLocale();
    void defaultLocale(Locale defaultLocale);

    R gen(Locale locale, String key, Object... args);
    default R gen(String key, Object... args) { return gen(defaultLocale(), key, args); }
}
