package com.gitlab.aecsocket.unifiedframework.core.event;

import java.util.*;
import java.util.function.Consumer;

/**
 * Responsible for sending events to listener objects.
 */
public class EventDispatcher {
    private final List<ListenerInstance<?>> listeners = new ArrayList<>();

    public EventDispatcher() {}

    public EventDispatcher(EventDispatcher o) {
        for (ListenerInstance<?> listener : o.listeners) {
            listeners.add(new ListenerInstance<>(listener));
        }
    }

    /**
     * Gets all registered listeners.
     * @return The listeners.
     */
    public List<ListenerInstance<?>> getListeners() { return listeners; }

    /**
     * Gets all registered listeners for a specific event type, or its subclasses.
     * @param eventType The event type.
     * @param <E> The event type.
     * @return The listeners.
     * @see EventDispatcher#getExactListeners(Class)
     */
    public <E> List<ListenerInstance<E>> getListeners(Class<? extends E> eventType) {
        List<ListenerInstance<E>> result = new ArrayList<>();
        for (ListenerInstance<?> listener : listeners) {
            if (listener.getEventType().isAssignableFrom(eventType)) {
                @SuppressWarnings("unchecked")
                ListenerInstance<E> eListener = (ListenerInstance<E>) listener;
                result.add(eListener);
            }
        }
        return result;
    }

    /**
     * Gets all registered listeners for a specific event type, but not its subclasses.
     * @param eventType The event type.
     * @param <E> The event type.
     * @return The listeners.
     * @see EventDispatcher#getListeners(Class)
     */
    public <E> List<ListenerInstance<E>> getExactListeners(Class<E> eventType) {
        List<ListenerInstance<E>> result = new ArrayList<>();
        for (ListenerInstance<?> listener : listeners) {
            if (eventType == listener.getEventType()) {
                @SuppressWarnings("unchecked")
                ListenerInstance<E> eListener = (ListenerInstance<E>) listener;
                result.add(eListener);
            }
        }
        return result;
    }

    /**
     * Gets all raw listener consumers for a specific event type, or its subclasses.
     * @param eventType The event type.
     * @param <E> The event type.
     * @return The listeners.
     */
    public <E> List<Consumer<E>> listenersOf(Class<? extends E> eventType) {
        List<Consumer<E>> result = new ArrayList<>();
        for (ListenerInstance<? extends E> inst : getListeners(eventType)) {
            @SuppressWarnings("unchecked")
            Consumer<E> listener = (Consumer<E>) inst.getListener();
            result.add(listener);
        }
        return result;
    }

    /**
     * Gets the {@link ListenerInstance} of a specific listener, or null.
     * @param listener The listener.
     * @param <E> The event's type.
     * @return The ListenerInstance.
     */
    @SuppressWarnings("unchecked")
    public <E> ListenerInstance<E> getListenerInstance(Consumer<E> listener) {
        for (ListenerInstance<?> inst : listeners) {
            if (inst.getListener() == listener) return (ListenerInstance<E>) inst;
        }
        return null;
    }

    /**
     * Adds a listener to an event class, which is informed every time {@link EventDispatcher#call(Object)}
     * is called with an event of the same type, or a subclass.
     * @param eventType The event's type.
     * @param listener The listener.
     * @param priority The priority of the listener. The higher, the later it receives the event.
     * @param <E> The event's type.
     * @return The ListenerInstance stored internally.
     * @see EventDispatcher#unregisterListener(Consumer)
     */
    public <E> ListenerInstance<E> registerListener(Class<E> eventType, Consumer<E> listener, int priority) {
        ListenerInstance<E> inst = getListenerInstance(listener);
        if (inst == null)
            listeners.add(new ListenerInstance<>(eventType, listener, priority));
        else
            inst.setPriority(priority);
        listeners.sort(Comparator.comparingInt(ListenerInstance::getPriority));
        return inst;
    }

    /**
     * Removes a listener from being informed by events through {@link EventDispatcher#call(Object)}.
     * @param listener The listener.
     * @see EventDispatcher#registerListener(Class, Consumer, int)
     * @see EventDispatcher#unregisterAll(Class)
     */
    public void unregisterListener(Consumer<?> listener) { listeners.removeIf(inst -> inst.getListener() == listener); }

    /**
     * Removes all listeners for a specific event type.
     * @param eventType The event's type.
     */
    public void unregisterAll(Class<?> eventType) { listeners.removeIf(inst -> inst.getEventType() == eventType); }

    /**
     * Removes all listeners.
     */
    public void unregisterAll() { listeners.clear(); }

    /**
     * Broadcasts an event to all registered listeners of its type.
     * @param event The event.
     * @param <E> The event's type.
     * @return The event passed.
     */
    public <E> E call(E event) {
        @SuppressWarnings("unchecked")
        Class<E> type = (Class<E>) event.getClass();
        List<Consumer<E>> listeners = listenersOf(type);
        listeners.forEach(listener -> listener.accept(event));
        return event;
    }
}
