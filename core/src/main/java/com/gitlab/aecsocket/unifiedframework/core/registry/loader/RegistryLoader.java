package com.gitlab.aecsocket.unifiedframework.core.registry.loader;

import com.gitlab.aecsocket.unifiedframework.core.registry.Identifiable;
import com.gitlab.aecsocket.unifiedframework.core.registry.Ref;
import com.gitlab.aecsocket.unifiedframework.core.registry.Registry;
import com.gitlab.aecsocket.unifiedframework.core.registry.ValidationException;
import com.gitlab.aecsocket.unifiedframework.core.resource.ResourceLoadException;
import com.gitlab.aecsocket.unifiedframework.core.util.Utils;
import com.gitlab.aecsocket.unifiedframework.core.util.result.LoadEntry;

import java.io.File;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public abstract class RegistryLoader<T extends Identifiable> {
    private final File root;
    private final Map<Path, Type> types;

    public RegistryLoader(File root, Map<Path, Type> types) {
        this.root = root;
        this.types = types;
    }

    public RegistryLoader(File root) {
        this.root = root;
        types = new LinkedHashMap<>();
    }

    public Map<Path, Type> getTypes() { return types; }

    /**
     * Adds a path that is deserialized by a certain type.
     * @param path The path.
     * @param type The type that files here are deserialized as.
     * @return This instance.
     */
    public RegistryLoader<T> with(Path path, Type type) { types.put(path, type); return this; }

    /**
     * Adds a path that is deserialized by a certain type.
     * @param path The path.
     * @param type The type that files here are deserialized as.
     * @return This instance.
     */
    public RegistryLoader<T> with(String path, Type type) { return with(Paths.get(path), type); }

    protected abstract Collection<T> deserialize(File file, Path path, Type type, List<LoadEntry<Ref<T>, ResourceLoadException>> result);

    /**
     * Loads {@link T}s from the filesystem into the specified registry.
     * @param registry The registry.
     * @return The result.
     */
    public List<LoadEntry<Ref<T>, ResourceLoadException>> load(Registry<T> registry) {
        List<LoadEntry<Ref<T>, ResourceLoadException>> result = new ArrayList<>();
        types.forEach((rootPath, type) -> Utils.applyRecursively(root.toPath().resolve(rootPath).toFile(), (file, path) -> {
            for (T object : deserialize(file, path, type, result)) {
                String id = object.id();
                if (id == null) {
                    result.add(LoadEntry.of(path, new ResourceLoadException("Object has no ID")));
                } else if (registry.has(id)) {
                    result.add(LoadEntry.of(path, new ResourceLoadException(String.format("Object %s is already registered", id))));
                } else {
                    try {
                        result.add(LoadEntry.of(path, registry.register(object)));
                    } catch (ValidationException e) {
                        result.add(LoadEntry.of(path, new ResourceLoadException("Could not validate " + id, e)));
                    }
                }
            }
        }));
        return result;
    }
}
