package com.gitlab.aecsocket.unifiedframework.core.component;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface Component {
    @NotNull <S extends Slot> Map<String, S> slots();

    //region Navigation
    <S extends Slot> S parent();

    Component parent(Slot parent);

    <S extends Slot> S slot(String key);

    default <C extends Component> C component(String key) {
        Slot slot = slot(key);
        if (slot == null)
            return null;
        return slot.get();
    }
    //endregion

    //region Recursive Navigation
    default <S extends Slot> S slot(String... path) {
        if (path.length == 0)
            return parent();

        S next = slot(path[0]);
        if (path.length == 1 || next == null)
            return next;

        Component nextComp = next.get();
        return nextComp == null ? null : nextComp.slot(Arrays.copyOfRange(path, 1, path.length));
    }

    default <C extends Component> C component(String... path) {
        if (path.length == 0) {
            @SuppressWarnings("unchecked")
            C c = (C) this;
            return c;
        }

        Slot slot = slot(path);
        if (slot == null)
            return null;
        return slot.get();
    }
    //endregion

    //region Walking
    default boolean walk(Consumer<WalkData.Pathed> function, String[] path) {
        for (Map.Entry<String, Slot> entry : slots().entrySet()) {
            String key = entry.getKey();
            Slot slot = entry.getValue();

            String[] newPath = new String[path.length + 1];
            System.arraycopy(path, 0, newPath, 0, path.length);
            newPath[newPath.length - 1] = key;

            WalkData.Pathed inst = new WalkData.Pathed(slot, newPath);
            function.accept(inst);
            if (inst.stopped())
                return false;
            else if (slot.get() != null) {
                if (!slot.get().walk(function, newPath))
                    return false;
            }
        }
        return true;
    }

    default void walk(Consumer<WalkData.Pathed> function) {
        walk(function, new String[0]);
    }

    @SuppressWarnings("unchecked")
    default <C extends Component> void forWalkAndThis(BiConsumer<C, String[]> function) {
        function.accept((C) this, new String[0]);
        walk(data -> {
            if (data.component() != null)
                function.accept(data.component(), data.path());
        });
    }

    default void walkParents(Consumer<WalkData.Depthed> function) {
        Component current = this;
        Slot slot;
        for (int depth = 0; (slot = current.parent()) != null; depth++) {
            WalkData.Depthed inst = new WalkData.Depthed(slot, depth);
            function.accept(inst);
            if (inst.stopped())
                return;

            SlotRef<?> parent = slot.parent();
            if (parent != null)
                current = parent.component();
        }
    }

    default <C extends Component> C root() {
        Component current;
        for (
                current = this;
                current.parent() != null && current.parent().parent() != null;
                current = current.parent().parent().component()

        );
        @SuppressWarnings("unchecked")
        C c = (C) current;
        return c;
    }

    default boolean isRoot() { return parent() == null; }

    default String[] path() {
        LinkedList<String> path = new LinkedList<>();
        walkParents(data -> {
            if (data.slot().parent() != null)
                path.addFirst(data.slot().parent().slotKey());
        });
        return path.toArray(new String[0]);
    }
    //endregion
}
