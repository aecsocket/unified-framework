package com.gitlab.aecsocket.unifiedframework.core.util.vector;

// in radians
public final class ViewCoordinates {
    private final double yaw;
    private final double pitch;

    public ViewCoordinates(double yaw, double pitch) {
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public double yaw() { return yaw; }
    public ViewCoordinates yaw(double yaw) { return new ViewCoordinates(yaw, pitch); }

    public double pitch() { return pitch; }
    public ViewCoordinates pitch(double pitch) { return new ViewCoordinates(yaw, pitch); }

    public Vector3D toVector() {
        double xz = Math.cos(pitch);
        return new Vector3D(
                -xz * Math.sin(yaw),
                -Math.sin(pitch),
                xz * Math.cos(yaw)
        );
    }

    @Override public String toString() { return "(" + Math.toDegrees(yaw) + ", " + Math.toDegrees(pitch) + ")"; }
}
