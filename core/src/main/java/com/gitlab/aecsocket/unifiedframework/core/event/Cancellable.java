package com.gitlab.aecsocket.unifiedframework.core.event;

/**
 * An event which can be cancelled.
 */
public interface Cancellable {
    /**
     * If the event is cancelled.
     * @return The result.
     */
    boolean cancelled();

    /**
     * Cancels the event.
     */
    void cancel();
}
