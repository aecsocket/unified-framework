package com.gitlab.aecsocket.unifiedframework.core.parsing.lexing;

import java.util.Deque;
import java.util.LinkedList;
import java.util.regex.Matcher;

public interface Lexer {
    static Deque<Token> tokens(String text, Definition... definitions) {
        Deque<Token> tokens = new LinkedList<>();
        while (text.length() != 0) {
            boolean matched = false;
            for (Definition definition : definitions) {
                Matcher match = definition.pattern().matcher(text);
                if (match.find()) {
                    matched = true;

                    String sequence = match.group().trim();
                    tokens.add(definition.inst(sequence));

                    text = match.replaceFirst("").strip();
                    break;
                }
            }
            if (!matched)
                throw new LexerException("Unexpected sequence '" + text + "'");
        }
        return tokens;
    }

    Definition[] getDefinitions();

    default Deque<Token> tokens(String text) { return tokens(text, getDefinitions()); }
}
