package com.gitlab.aecsocket.unifiedframework.core.stat.serialization;

public class FunctionCreationException extends RuntimeException {
    public FunctionCreationException() {}
    public FunctionCreationException(String message) { super(message); }
    public FunctionCreationException(String message, Throwable cause) { super(message, cause); }
    public FunctionCreationException(Throwable cause) { super(cause); }
    public FunctionCreationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) { super(message, cause, enableSuppression, writableStackTrace); }
}
