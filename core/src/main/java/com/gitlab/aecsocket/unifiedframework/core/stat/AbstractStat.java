package com.gitlab.aecsocket.unifiedframework.core.stat;

/**
 * A generic stat.
 * @param <T> The held value type.
 */
public abstract class AbstractStat<T> implements Stat<T> {
    private T defaultValue;

    public AbstractStat(T defaultValue) {
        this.defaultValue = defaultValue;
    }

    public AbstractStat() {}

    public AbstractStat(AbstractStat<T> o) {
        defaultValue = o.defaultValue;
    }

    @Override
    public T defaultValue() { return defaultValue; }
    public AbstractStat<T> defaultValue(T defaultValue) { this.defaultValue = defaultValue; return this; }
    @Override
    public String toString() {
        return "[" + com.google.gson.reflect.TypeToken.get(valueType().getType()).getRawType().getSimpleName() + " " + defaultValue + "]";
    }
}
