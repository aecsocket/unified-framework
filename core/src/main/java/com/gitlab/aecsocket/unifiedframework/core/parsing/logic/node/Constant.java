package com.gitlab.aecsocket.unifiedframework.core.parsing.logic.node;

import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicVisitor;

public class Constant implements LogicExpressionNode {
    private final boolean value;

    public Constant(boolean value) {
        this.value = value;
    }

    public Constant(String text) {
        value = Boolean.parseBoolean(text);
    }

    @Override public int type() { return LogicExpressionNode.CONSTANT; }
    @Override public Constant accept(LogicVisitor visitor) { visitor.visit(this); return this; }

    @Override public boolean value() { return value; }
}
