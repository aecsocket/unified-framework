package com.gitlab.aecsocket.unifiedframework.core.stat.impl.operation;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;

public class JsonLoadContext {
    private final JsonArray json;
    private final JsonDeserializationContext context;

    public JsonLoadContext(JsonArray json, JsonDeserializationContext context) {
        this.json = json;
        this.context = context;
    }

    public JsonArray getJson() { return json; }
    public JsonDeserializationContext getContext() { return context; }
}
