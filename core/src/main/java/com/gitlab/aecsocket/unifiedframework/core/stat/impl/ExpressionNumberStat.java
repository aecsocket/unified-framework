package com.gitlab.aecsocket.unifiedframework.core.stat.impl;

import com.gitlab.aecsocket.unifiedframework.core.stat.AbstractStat;
import com.gitlab.aecsocket.unifiedframework.core.stat.serialization.ConfigurateStat;

// it doesnt work
@Deprecated(forRemoval = true)
public abstract class ExpressionNumberStat<T extends Number> extends AbstractStat<T> implements ConfigurateStat<T> {
    /*
    private T min;
    private T max;
    private Function<T, String> displayAs;

    public ExpressionNumberStat(T defaultValue) { super(defaultValue); }
    public ExpressionNumberStat() {}

    @Override public T min() { return min; }
    public ExpressionNumberStat<T> min(T min) { this.min = min; return this; }

    @Override public T max() { return max; }
    public ExpressionNumberStat<T> max(T max) { this.max = max; return this; }

    public Function<T, String> displayAs() { return displayAs; }
    public ExpressionNumberStat<T> displayAs(Function<T, String> displayAs) { this.displayAs = displayAs; return this; }

    protected abstract T toGeneric(Number value);

    @Override public T preModify(T value) { return value == null ? toGeneric(0) : value; }

    protected String defaultDisplay(T value) { return value.toString(); }

    public Function<T, T> getModFunction(String text) {
        MathExpressionNode expr = MathParser.parses(text);
        return base -> {
            expr.setVariable("b", base.doubleValue());
            return toGeneric(expr.value());
        };
    }

    @Override
    public Function<T, T> getModFunction(ConfigurationNode node) { return getModFunction(node.getString()); }

    @Override
    public @NotNull String toString(T value) {
        return displayAs == null ? defaultDisplay(value) : displayAs.apply(value);
    }

    @Override
    public ExpressionNumberStat<T> configure(ConfigurationNode settings) {
        return ((ExpressionNumberStat<T>) super.configure(settings))
                .min(configureField(settings, getValueType(), min, "min"))
                .max(configureField(settings, getValueType(), max, "max"));
    }

    public static final class Byte extends ExpressionNumberStat<java.lang.Byte> {
        public Byte(java.lang.Byte defaultValue) { super(defaultValue); }
        public Byte() {}

        @Override public Type getValueType() { return double.class; }
        @Override protected java.lang.Byte toGeneric(Number value) { return value.byteValue(); }

        @Override protected ExpressionNumberStat<java.lang.Byte> createNew() { return new Byte(); }
    }

    public static final class Short extends ExpressionNumberStat<java.lang.Short> {
        public Short(java.lang.Short defaultValue) { super(defaultValue); }
        public Short() {}

        @Override public Type getValueType() { return double.class; }
        @Override protected java.lang.Short toGeneric(Number value) { return value.shortValue(); }

        @Override protected ExpressionNumberStat<java.lang.Short> createNew() { return new Short(); }
    }

    public static final class Integer extends ExpressionNumberStat<java.lang.Integer> {
        public Integer(java.lang.Integer defaultValue) { super(defaultValue); }
        public Integer() {}

        @Override public Type getValueType() { return int.class; }
        @Override protected java.lang.Integer toGeneric(Number value) { return value.intValue(); }

        @Override protected ExpressionNumberStat<java.lang.Integer> createNew() { return new Integer(); }
    }

    public static final class Long extends ExpressionNumberStat<java.lang.Long> {
        public Long(java.lang.Long defaultValue) { super(defaultValue); }
        public Long() {}

        @Override public Type getValueType() { return long.class; }
        @Override protected java.lang.Long toGeneric(Number value) { return value.longValue(); }

        @Override protected ExpressionNumberStat<java.lang.Long> createNew() { return new Long(); }
    }


    public static abstract class Decimal<T extends Number> extends ExpressionNumberStat<T> {
        private DecimalFormat format;

        public Decimal(T defaultValue, DecimalFormat format) {
            super(defaultValue);
            this.format = format;
        }
        public Decimal(T defaultValue) { super(defaultValue); }
        public Decimal() {}

        public DecimalFormat format() { return format; }
        public Decimal<T> format(DecimalFormat format) { this.format = format; return this; }
        public Decimal<T> format(String pattern) { format = pattern == null ? null : new DecimalFormat(pattern); return this; }

        @Override
        protected String defaultDisplay(T value) {
            return format == null ? value.toString() : format.format(value);
        }

        @Override
        public AbstractStat<T> configure(ConfigurationNode settings) {
            return ((Decimal<T>) super.configure(settings))
                    .format(settings.node("format").getString());
        }
    }

    public static final class Float extends Decimal<java.lang.Float> {
        public Float(java.lang.Float defaultValue) { super(defaultValue); }
        public Float() {}

        @Override public Type getValueType() { return float.class; }
        @Override protected java.lang.Float toGeneric(Number value) { return value.floatValue(); }

        @Override protected ExpressionNumberStat<java.lang.Float> createNew() { return new Float(); }
    }

    public static final class Double extends Decimal<java.lang.Double> {
        public Double(java.lang.Double defaultValue) { super(defaultValue); }
        public Double() {}

        @Override public Type getValueType() { return double.class; }
        @Override protected java.lang.Double toGeneric(Number value) { return value.doubleValue(); }

        @Override protected ExpressionNumberStat<java.lang.Double> createNew() { return new Double(); }
    }

    public static ExpressionNumberStat<java.lang.Integer> of(int defaultValue) { return new Integer(defaultValue); }
    public static ExpressionNumberStat<java.lang.Long> of(long defaultValue) { return new Long(defaultValue); }
    public static ExpressionNumberStat<java.lang.Float> of(float defaultValue) { return new Float(defaultValue); }
    public static ExpressionNumberStat<java.lang.Double> of(double defaultValue) { return new Double(defaultValue); }
    public static ExpressionNumberStat<java.lang.Byte> of(byte defaultValue) { return new Byte(defaultValue); }
    public static ExpressionNumberStat<java.lang.Short> of(short defaultValue) { return new Short(defaultValue); }
     */
}
