package com.gitlab.aecsocket.unifiedframework.core.registry.loader;

import com.gitlab.aecsocket.unifiedframework.core.registry.Identifiable;
import com.gitlab.aecsocket.unifiedframework.core.registry.Ref;
import com.gitlab.aecsocket.unifiedframework.core.resource.ResourceLoadException;
import com.gitlab.aecsocket.unifiedframework.core.util.result.LoadEntry;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.ConfigurationOptions;
import org.spongepowered.configurate.hocon.HoconConfigurationLoader;
import org.spongepowered.configurate.loader.ConfigurationLoader;
import org.spongepowered.configurate.serialize.SerializationException;

import java.io.File;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.*;

public abstract class ConfigurateRegistryLoader<T extends Identifiable> extends RegistryLoader<T> {
    public static class Hocon<T extends Identifiable> extends ConfigurateRegistryLoader<T> {
        public Hocon(File root, Map<Path, Type> types) { super(root, types); }
        public Hocon(File root) { super(root); }

        @Override
        protected ConfigurationLoader<?> createLoader(File file) {
            return HoconConfigurationLoader.builder()
                    .file(file)
                    .defaultOptions(options() )
                    .build();
        }

        @Override public Hocon<T> with(Path path, Type type) { return (Hocon<T>) super.with(path, type); }
        @Override public Hocon<T> with(String path, Type type) { return (Hocon<T>) super.with(path, type); }
    }

    private ConfigurationOptions options;

    public ConfigurateRegistryLoader(File root, Map<Path, Type> types) { super(root, types); }
    public ConfigurateRegistryLoader(File root) { super(root); }

    public ConfigurationOptions options() { return options; }
    public ConfigurateRegistryLoader<T> options(ConfigurationOptions options) { this.options = options; return this; }

    @Override public ConfigurateRegistryLoader<T> with(Path path, Type type) { return (ConfigurateRegistryLoader<T>) super.with(path, type); }
    @Override public ConfigurateRegistryLoader<T> with(String path, Type type) { return (ConfigurateRegistryLoader<T>) super.with(path, type); }

    protected abstract ConfigurationLoader<?> createLoader(File file);

    @Override
    protected Collection<T> deserialize(File file, Path path, Type type, List<LoadEntry<Ref<T>, ResourceLoadException>> result) {
        try {
            ConfigurationLoader<?> loader = createLoader(file);
            ConfigurationNode root = loader.load();
            if (root == null)
                return Collections.emptySet();

            List<T> identifiables = new ArrayList<>();
            for (var entry : root.node("entries").childrenMap().entrySet()) {
                try {
                    @SuppressWarnings("unchecked")
                    T obj = (T) entry.getValue().get(type);
                    if (obj != null) {
                        obj.id(entry.getKey().toString());
                        identifiables.add(obj);
                    }
                } catch (SerializationException e) {
                    result.add(LoadEntry.of(path, new ResourceLoadException(e)));
                }
            }
            return identifiables;
        } catch (ConfigurateException | RuntimeException e) {
            result.add(LoadEntry.of(path, new ResourceLoadException(e)));
            return Collections.emptySet();
        }
    }
}
