package com.gitlab.aecsocket.unifiedframework.core.parsing.logic.term;

import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.parsing.logic.LogicVisitor;

public class And extends StandardGate {
    public And(LogicExpressionNode a, LogicExpressionNode b) { super(a, b); }

    @Override public int type() { return LogicExpressionNode.AND; }
    @Override public And accept(LogicVisitor visitor) { return (And) super.accept(visitor); }

    @Override public boolean value() { return a.value() & b.value(); }
}
