package com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.descriptor;

import io.leangen.geantyref.TypeToken;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Descriptor;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Vector2DDescriptor;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2D;

/**
 * Allows proper deserialization of a {@link Vector2DDescriptor}.
 */
public class Vector2DDescriptorSerializer extends DescriptorSerializer<Vector2D, Vector2DDescriptor> {
    public static final Vector2DDescriptorSerializer INSTANCE = new Vector2DDescriptorSerializer();

    @Override protected TypeToken<Vector2D> valueType() { return new TypeToken<>(){}; }

    @Override
    protected Vector2DDescriptor create(Vector2D value) {
        return new Vector2DDescriptor(value);
    }

    @Override
    protected Vector2DDescriptor create(Vector2D value, Descriptor.Operation operation, Descriptor.Suffix suffix) {
        return new Vector2DDescriptor(value, operation, suffix);
    }
}
