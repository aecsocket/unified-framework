package com.gitlab.aecsocket.unifiedframework.paper;

import com.gitlab.aecsocket.unifiedframework.core.resource.ConfigurateSettings;
import org.junit.jupiter.api.Test;
import org.spongepowered.configurate.BasicConfigurationNode;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConfigurateSettingsTest {
    private ConfigurateSettings newSettings() throws SerializationException {
        ConfigurationNode node = BasicConfigurationNode.root();
        node.node("one").set(1);
        node.node("two").set("two");
        node.node("three", "sub").setList(String.class, Arrays.asList("a", "b"));
        return new ConfigurateSettings(node);
    }

    @Test
    void testAccess() throws SerializationException {
        ConfigurateSettings settings = newSettings();
        assertEquals(1, (int) settings.get(ConfigurationNode::getInt, "one"));
        assertEquals("two", settings.get(ConfigurationNode::getString, "two"));
        assertEquals(Arrays.asList("a", "b"), settings.get(n -> n.getList(String.class), "three", "sub"));
    }

    @Test
    void testCache() throws SerializationException {
        ConfigurateSettings settings = newSettings();
        assertEquals(1, (int) settings.get(ConfigurationNode::getInt, "one"));
        settings.force(2, "one");
        assertEquals(2, (int) settings.get(ConfigurationNode::getInt, "one"));

        assertEquals(Arrays.asList("a", "b"), settings.get(n -> n.getList(String.class), "three", "sub"));
        settings.force(Arrays.asList("c"), "three", "sub");
        assertEquals(Arrays.asList("c"), settings.get(n -> n.getList(String.class), "three", "sub"));
    }
}
