package com.gitlab.aecsocket.unifiedframework.paper;

import com.gitlab.aecsocket.unifiedframework.core.component.Component;
import com.gitlab.aecsocket.unifiedframework.core.component.ComponentImpl;
import com.gitlab.aecsocket.unifiedframework.core.component.IncompatibleComponentException;
import com.gitlab.aecsocket.unifiedframework.core.component.SlotImpl;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

public class ComponentTest {
    static class CompatibleComponent extends ComponentImpl {
        CompatibleComponent(Map<String, SlotImpl> slots) {
            super(slots);
        }
    }

    private Component createRoot() {
        return new ComponentImpl(new HashMap<>())
                .slot("a", new SlotImpl().set(
                        new ComponentImpl(new HashMap<>())
                                .slot("a", new SlotImpl().set(new ComponentImpl(new HashMap<>())))
                                .slot("b", new SlotImpl())
                ))
                .slot("b", new SlotImpl().set(
                        new ComponentImpl(new HashMap<>())
                                .slot("a", new SlotImpl())
                ));
    }

    @Test
    void testTree() {
        Component root = createRoot();

        assertNotNull(root.component("a"));
        assertNotNull(root.component("a", "a"));
        assertNotNull(root.slot("a", "b"));
        assertNotNull(root.component("b"));
        assertNotNull(root.slot("b", "a"));
    }

    @Test
    void testNavigation() {
        Component root = createRoot();

        assertEquals(root, root.component("a", "a").root());
        assertEquals((Object) root.slot("a", "a"), root.component("a", "a").parent());
        assertEquals(root.component("a"), root.slot("a", "a").parent().component());

        int target = 3;
        AtomicInteger count = new AtomicInteger(0);
        root.walk(data -> {
            if (count.incrementAndGet() == target)
                data.stop();
        });
        assertEquals(target, count.get());
    }

    @Test
    void testSlot() {
        SlotImpl slot = new SlotImpl() {
            @Override
            public boolean isCompatible(@NotNull Component component) {
                return component instanceof CompatibleComponent;
            }
        };

        assertTrue(slot.isCompatible(new CompatibleComponent(new HashMap<>())));
        assertThrows(IncompatibleComponentException.class, () -> slot.set(new ComponentImpl(new HashMap<>())));
    }
}
