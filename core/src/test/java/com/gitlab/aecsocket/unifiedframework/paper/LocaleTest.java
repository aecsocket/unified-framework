package com.gitlab.aecsocket.unifiedframework.paper;

import com.gitlab.aecsocket.unifiedframework.core.locale.MiniMessageLocalization;
import com.gitlab.aecsocket.unifiedframework.core.locale.Translations;
import com.gitlab.aecsocket.unifiedframework.core.locale.TranslationsLoader;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.TranslationsSerializer;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.spongepowered.configurate.BasicConfigurationNode;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.util.CheckedConsumer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LocaleTest {
    private final GsonComponentSerializer serializer = GsonComponentSerializer.gson();

    private String serialize(Component component) { return serializer.serialize(component); }
    private String serialize(Component[] components) {
        String[] results = new String[components.length];
        for (int i = 0; i < components.length; i++) {
            results[i] = serialize(components[i]);
        }
        return String.join("\n", results);
    }

    protected File doFile(File dataRoot, String extra, CheckedConsumer<File, IOException> consumer) {
        File file = new File(dataRoot, extra);
        try {
            consumer.accept(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }

    @Test
    void testGenerate() {
        MiniMessageLocalization localization = new MiniMessageLocalization();
        localization.set(Locale.US, "single", "value");
        localization.set(Locale.US, "multi", "one", "two");

        assertEquals(
                serialize(Component.text().append(Component.text("value")).build()),
                serialize(localization.gen("single")));

        assertEquals(
                serialize(Component.text()
                        .append(Component.text("one"))
                        .append(Component.newline())
                        .append(Component.text("two"))
                        .build()
                ),
                serialize(localization.gen("multi"))
        );
    }

    @Test
    void testLocale() {
        MiniMessageLocalization localization = new MiniMessageLocalization();
        localization.set(Locale.US, "one", "us one");
        localization.set(Locale.UK, "two", "uk two");

        assertEquals(
                serialize(Component.text().append(Component.text("us one")).build()),
                serialize(localization.gen(Locale.UK, "one")));
    }

    @Test
    void testFallbackLocale() {
        MiniMessageLocalization localization = new MiniMessageLocalization();
        localization.set(Locale.US, "single", "us");
        localization.set(Locale.US, "multi", "us one", "us two");

        assertEquals(
                serialize(Component.text().append(Component.text("us")).build()),
                serialize(localization.gen(Locale.UK, "single")));

        assertEquals(
                serialize(Component.text()
                        .append(Component.text("us one"))
                        .append(Component.newline())
                        .append(Component.text("us two"))
                        .build()
                ),
                serialize(localization.gen("multi"))
        );
    }

    @Test
    void testAltFallbackLocale() {
        MiniMessageLocalization localization = new MiniMessageLocalization();
        localization.defaultLocale(Locale.UK);
        localization.set(Locale.US, "key", "us");
        localization.set(Locale.UK, "key", "uk");

        assertEquals(
                serialize(Component.text().append(Component.text("uk")).build()),
                serialize(localization.gen(Locale.FRENCH, "key")));
    }

    @Test
    void testFallbackMessage() {
        MiniMessageLocalization localization = new MiniMessageLocalization();
        localization.fallback("the key: <gen_key>");

        assertEquals(
                serialize(Component.text().append(Component.text("the key: test")).build()),
                serialize(localization.gen("test")));

        localization.fallback("the key: <gen_key>, the args: <args>");
        assertEquals(
                serialize(Component.text().append(Component.text("the key: test, the args: [arg, 3]")).build()),
                serialize(localization.gen("test", "arg", 3)));
    }

    @Test
    void testHoconLoader(@TempDir File dataRoot) throws IOException {
        File root = doFile(dataRoot, "lang", File::mkdir);
        File test = doFile(dataRoot, "lang/test.conf", File::createNewFile);
        Files.write(
                test.toPath(),
                Arrays.asList(
                        "locale: \"en-US\"",
                        "single: \"value\"",
                        "sub.key: \"value\"",         // HOCON loader is in alphabetical order
                        "multi: [ \"one\", \"two\" ]"
                )
        );

        MiniMessageLocalization localization = new MiniMessageLocalization();

        TranslationsLoader.hocon(root, localization);
        assertEquals(
                serialize(Component.text().append(Component.text("value")).build()),
                serialize(localization.gen("single")));

        assertEquals(
                serialize(Component.text()
                        .append(Component.text("one"))
                        .append(Component.newline())
                        .append(Component.text("two"))
                        .build()
                ),
                serialize(localization.gen("multi"))
        );
    }

    @Test
    void testSerialization() throws IOException {
        Translations translations = new Translations(Locale.US);
        translations.set("one", "a");
        translations.set("section.two", "b");
        translations.set("section.three", "c", "d");

        ConfigurationNode node = BasicConfigurationNode.root();
        TranslationsSerializer.INSTANCE.serialize(Translations.class, translations, node);
        assertEquals("en_US", node.node("locale").getString());
        assertEquals("a", node.node("one").getString());
        assertEquals("b", node.node("section.two").getString());
        assertEquals(Arrays.asList("c", "d"), node.node("section.three").getList(String.class));
    }
}
