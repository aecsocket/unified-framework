package com.gitlab.aecsocket.unifiedframework.paper;

import com.gitlab.aecsocket.unifiedframework.core.event.EventDispatcher;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EventDispatcherTest {
    static class BasicEvent {
        final int x;

        BasicEvent(int x) {
            this.x = x;
        }

        int getX() { return x; }
    }

    static class ComplexEvent extends BasicEvent {
        final int y;

        ComplexEvent(int x, int y) {
            super(x);
            this.y = y;
        }

        int getY() { return y; }
    }

    @Test
    void testListen() throws InterruptedException {
        EventDispatcher dispatcher = new EventDispatcher();
        AtomicBoolean success = new AtomicBoolean(false);
        dispatcher.registerListener(BasicEvent.class, event -> {
            assertEquals(3, event.x);
            success.set(true);
        }, 0);
        dispatcher.call(new BasicEvent(3));

        Thread.sleep(10);
        if (!success.get())
            throw new AssertionFailedError("Timed out");
    }

    @Test
    void testListenSubclass() throws InterruptedException {
        EventDispatcher dispatcher = new EventDispatcher();
        AtomicBoolean success = new AtomicBoolean(false);
        dispatcher.registerListener(BasicEvent.class, event -> {
            assertEquals(3, event.x);
            success.set(true);
        }, 0);
        dispatcher.call(new ComplexEvent(3, 4));

        Thread.sleep(10);
        if (!success.get())
            throw new AssertionFailedError("Timed out");
    }

    @Test
    void testPriority() throws InterruptedException {
        EventDispatcher dispatcher = new EventDispatcher();
        AtomicInteger value = new AtomicInteger(0);
        dispatcher.registerListener(BasicEvent.class, event -> {
            value.set(value.get() + 1);
        }, 0);
        dispatcher.registerListener(BasicEvent.class, event -> {
            value.set(value.get() * 2);
        }, 1);
        dispatcher.call(new BasicEvent(0));

        Thread.sleep(10);
        assertEquals(2, value.get());
    }
}
