package com.gitlab.aecsocket.unifiedframework.paper;

import com.gitlab.aecsocket.unifiedframework.core.parsing.eval.EvaluationException;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathParser;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.SetVariableVisitor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParserTest {
    @Test
    void testMathBasic() {
        assertEquals(5, MathParser.parses("3 + 2").value());
        assertEquals(11, MathParser.parses("5 + 3 * 2").value());
        assertEquals(16, MathParser.parses("(5 + 3) * 2").value());
        assertEquals(16, MathParser.parses("4 ^ 2").value());
    }

    @Test
    void testMathVariables() {
        assertEquals(8, MathParser.parses("var + 5").accept(new SetVariableVisitor("var", 3)).value());
        assertEquals(10, MathParser.parses("(b * 4) + 2").accept(new SetVariableVisitor("b", 2)).value());
    }

    @Test
    void testMathFunctions() {
        assertEquals(Math.sin(5), MathParser.parses("sin 5").value());
        assertEquals(Math.cos(5), MathParser.parses("cos 5").value());
    }

    @Test
    void testInvalid() {
        assertThrows(EvaluationException.class, () -> MathParser.parses(""));
        assertThrows(EvaluationException.class, () -> MathParser.parses("("));
        assertThrows(EvaluationException.class, () -> MathParser.parses("5 *"));
    }
}
