package com.gitlab.aecsocket.unifiedframework.paper;

import com.gitlab.aecsocket.unifiedframework.core.util.Utils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UtilsTest {
    @Test
    void testMath() {
        assertEquals(0.5, Utils.clamp(0.5, 0, 1));
        assertEquals(0, Utils.clamp(-1, 0, 1));
        assertEquals(1, Utils.clamp(2, 0, 1));

        assertEquals(0.5, Utils.clamp01(0.5));
        assertEquals(0, Utils.clamp01(-1));
        assertEquals(1, Utils.clamp01(2));

        assertEquals(0.5, Utils.wrap(0.5, 0, 1));
        assertEquals(0.5, Utils.wrap(1.5, 0, 1));
        assertEquals(0.5, Utils.wrap(2.5, 0, 1));
        assertEquals(0.5, Utils.wrap(-0.5, 0, 1));
        assertEquals(0.5, Utils.wrap(-1.5, 0, 1));
    }
}
