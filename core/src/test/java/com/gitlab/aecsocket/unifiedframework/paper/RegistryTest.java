package com.gitlab.aecsocket.unifiedframework.paper;

import com.gitlab.aecsocket.unifiedframework.core.registry.Identifiable;
import com.gitlab.aecsocket.unifiedframework.core.registry.Ref;
import com.gitlab.aecsocket.unifiedframework.core.registry.Registry;
import com.gitlab.aecsocket.unifiedframework.core.registry.loader.ConfigurateRegistryLoader;
import com.gitlab.aecsocket.unifiedframework.core.util.GraphUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.spongepowered.configurate.ConfigurationOptions;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.util.CheckedConsumer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegistryTest {
    protected File doFile(File dataRoot, String extra, CheckedConsumer<File, IOException> consumer) {
        File file = new File(dataRoot, extra);
        try {
            consumer.accept(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }

    static class BasicIdentifiable implements Identifiable {
        private String id;
        private final String[] dependencies;

        BasicIdentifiable(String id, String... dependencies) {
            this.id = id;
            this.dependencies = dependencies;
        }

        @Override public String id() { return id; }
        @Override public void id(String id) { this.id = id; }

        @Override public Collection<String> dependencies() { return Arrays.asList(dependencies); }

        @Override
        public String toString() { return id; }
    }

    @ConfigSerializable
    static class ComplexIdentifiable implements Identifiable {
        String id;
        int x;
        int y;

        ComplexIdentifiable(String id, int x, int y) {
            this.id = id;
            this.x = x;
            this.y = y;
        }

        ComplexIdentifiable() {}

        @Override public String id() { return id; }
        @Override public void id(String id) { this.id = id; }

        @Override
        public String toString() { return id; }
    }

    @Test
    void testRegister() {
        Registry<Identifiable> registry = new Registry<>();
        registry.register(new BasicIdentifiable("test"));
        assertTrue(registry.has("test"));
    }

    @Test
    void testLinking() {
        Registry<Identifiable> registry = new Registry<>();
        Identifiable dependency = new BasicIdentifiable("dependency");
        Identifiable dependent = new BasicIdentifiable("dependent") {
            @Override
            public Collection<String> dependencies() {
                return Collections.singleton("dependency");
            }
        };
        List<Ref<Identifiable>> expectedNodes = new ArrayList<>();
        expectedNodes.add(new Ref<>(dependency));
        expectedNodes.add(new Ref<>(dependent));

        registry.register(dependency);
        registry.register(dependent);
        registry.link();

        assertEquals(
                expectedNodes.toString(),
                GraphUtils.topologicallySortedNodes(registry.getDependencyGraph()).toString()
        );
    }

    @Test
    void testCyclicalDependencies() {
        Registry<Identifiable> registry = new Registry<>();
        Identifiable dependency = new BasicIdentifiable("dependency", "dependent");
        Identifiable dependent = new BasicIdentifiable("dependent", "dependency");

        registry.register(dependency);
        registry.register(dependent);
        assertEquals(2, registry.link().size());
    }

    @Test
    void testHoconLoader(@TempDir File dataRoot) throws IOException {
        File root = doFile(dataRoot, "hocon", File::mkdir);
        File test = doFile(dataRoot, "hocon/test.json", File::createNewFile);
        // cannot use relative `include` statements
        Files.write(
                test.toPath(),
                Arrays.asList(
                        "base { x: 5 }",
                        "y: 3",
                        "entries: {",
                        "  test: ${base} {",
                        "    y: ${y}",
                        "  }",
                        "}"
                )
        );

        Registry<ComplexIdentifiable> registry = new Registry<>();
        new ConfigurateRegistryLoader.Hocon<ComplexIdentifiable>(root)
                .options(ConfigurationOptions.defaults())
                .with("", ComplexIdentifiable.class)
                .load(registry);
        assertTrue(registry.has("test")); // registry has
        assertEquals(5, registry.get("test").x); // correct values
        assertEquals(3, registry.get("test").y);
    }
}
