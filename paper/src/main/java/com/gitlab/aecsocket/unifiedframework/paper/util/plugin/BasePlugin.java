package com.gitlab.aecsocket.unifiedframework.paper.util.plugin;

import com.gitlab.aecsocket.unifiedframework.core.locale.MiniMessageLocalization;
import com.gitlab.aecsocket.unifiedframework.core.locale.TranslationsLoader;
import com.gitlab.aecsocket.unifiedframework.core.parsing.math.MathExpressionNode;
import com.gitlab.aecsocket.unifiedframework.core.resource.ConfigurateSettings;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.*;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.descriptor.NumberDescriptorSerializer;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.descriptor.Vector2DDescriptorSerializer;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.descriptor.Vector3DDescriptorSerializer;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.vector.Vector2DSerializer;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.vector.Vector2ISerializer;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.vector.Vector3DSerializer;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.vector.Vector3ISerializer;
import com.gitlab.aecsocket.unifiedframework.core.util.color.ColorModifier;
import com.gitlab.aecsocket.unifiedframework.core.util.color.HSV;
import com.gitlab.aecsocket.unifiedframework.core.util.color.RGBA;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.NumberDescriptor;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Vector2DDescriptor;
import com.gitlab.aecsocket.unifiedframework.core.util.descriptor.Vector3DDescriptor;
import com.gitlab.aecsocket.unifiedframework.core.util.log.LabelledLogger;
import com.gitlab.aecsocket.unifiedframework.core.util.log.LogLevel;
import com.gitlab.aecsocket.unifiedframework.core.util.result.LoggingEntry;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2D;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2I;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3I;
import com.gitlab.aecsocket.unifiedframework.paper.gui.GUIVector;
import com.gitlab.aecsocket.unifiedframework.paper.scheduler.WrappedScheduler;
import com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate.*;
import com.gitlab.aecsocket.unifiedframework.paper.util.data.ParticleData;
import com.gitlab.aecsocket.unifiedframework.paper.util.data.SoundData;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.*;
import org.bukkit.block.data.BlockData;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerLoadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import org.spongepowered.configurate.ConfigurationOptions;
import org.spongepowered.configurate.hocon.HoconConfigurationLoader;
import org.spongepowered.configurate.loader.ParsingException;
import org.spongepowered.configurate.objectmapping.ObjectMapper;
import org.spongepowered.configurate.serialize.TypeSerializerCollection;
import org.spongepowered.configurate.util.NamingSchemes;

import java.io.File;
import java.util.*;

public abstract class BasePlugin extends JavaPlugin implements Listener {
    public static final String SETTINGS_FILE = "settings.conf";
    public static final String LANGUAGE_ROOT = "lang";
    public static final List<String> DEFAULT_RESOURCES = Arrays.asList(
            SETTINGS_FILE,
            LANGUAGE_ROOT + "/default_en-US.conf",
            LANGUAGE_ROOT + "/en-US.conf"
    );

    protected final Map<String, NamespacedKey> keys = new HashMap<>();
    protected final LabelledLogger logger = new LabelledLogger(getLogger(), defaultLogLevel());
    protected final ConfigurateSettings settings = new ConfigurateSettings();
    protected final MiniMessageLocalization localization = new MiniMessageLocalization();
    protected final WrappedScheduler scheduler = new WrappedScheduler(this);
    protected ConfigurationOptions configOptions = ConfigurationOptions.defaults();

    protected String settingsFile() { return SETTINGS_FILE; }
    protected String languageRoot() { return LANGUAGE_ROOT; }
    protected List<String> defaultResources() { return DEFAULT_RESOURCES; }
    protected LogLevel defaultLogLevel() { return LogLevel.INFO; }
    protected abstract List<PluginHook> hooks();
    protected void configOptionDefaults(TypeSerializerCollection.Builder serializers, ObjectMapper.Factory.Builder mapperFactory) {
        mapperFactory.defaultNamingScheme(NamingSchemes.SNAKE_CASE);
        serializers
                .register(Color.class, ColorSerializer.INSTANCE)
                .register(TextColor.class, TextColorSerializer.INSTANCE)
                .register(RGBA.class, RGBASerializer.INSTANCE)
                .register(HSV.class, HSVSerializer.INSTANCE)
                .register(ColorModifier.class, ColorModifierSerializer.INSTANCE)
                .register(BlockData.class, BlockDataSerializer.INSTANCE)
                .register(Particle.DustOptions.class, DustOptionsSerializer.INSTANCE)
                .register(ItemStack.class, ItemStackSerializer.INSTANCE)
                .register(Location.class, LocationSerializer.INSTANCE)
                .register(World.class, WorldSerializer.INSTANCE)
                .register(NamespacedKey.class, NamespacedKeySerializer.INSTANCE)
                .register(PotionEffectType.class, PotionEffectTypeSerializer.INSTANCE)
                .register(PotionEffect.class, PotionEffectSerializer.INSTANCE)

                .register(NumberDescriptor.Byte.class, NumberDescriptorSerializer.Byte.INSTANCE)
                .register(NumberDescriptor.Short.class, NumberDescriptorSerializer.Short.INSTANCE)
                .register(NumberDescriptor.Integer.class, NumberDescriptorSerializer.Integer.INSTANCE)
                .register(NumberDescriptor.Long.class, NumberDescriptorSerializer.Long.INSTANCE)
                .register(NumberDescriptor.Float.class, NumberDescriptorSerializer.Float.INSTANCE)
                .register(NumberDescriptor.Double.class, NumberDescriptorSerializer.Double.INSTANCE)
                .register(Vector2DDescriptor.class, Vector2DDescriptorSerializer.INSTANCE)
                .register(Vector3DDescriptor.class, Vector3DDescriptorSerializer.INSTANCE)

                .register(Vector.class, VectorSerializer.INSTANCE)
                .register(GUIVector.class, GUIVectorSerializer.INSTANCE)
                .register(Vector3D.class, Vector3DSerializer.INSTANCE)
                .register(Vector3I.class, Vector3ISerializer.INSTANCE)
                .register(Vector2D.class, Vector2DSerializer.INSTANCE)
                .register(Vector2I.class, Vector2ISerializer.INSTANCE)

                .register(ParticleData.class, ParticleDataSerializer.INSTANCE)
                .register(SoundData.class, new SoundDataSerializer(this))

                .register(MathExpressionNode.class, MathExpressionNodeSerializer.INSTANCE);
    }

    @Override
    public void onEnable() {
        saveDefaults();
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    protected void createConfigOptions() {
        configOptions = configOptions.serializers(serializers -> {
            ObjectMapper.Factory.Builder mapperFactory = ObjectMapper.factoryBuilder();
            hooks().forEach(hook -> hook.setupSerializer(serializers, mapperFactory));
            configOptionDefaults(serializers, mapperFactory);
            serializers.registerAnnotatedObjects(mapperFactory.build());
        });
    }

    @EventHandler
    public boolean serverLoad(ServerLoadEvent event) {
        createConfigOptions();
        List<LoggingEntry> log = new ArrayList<>();
        boolean load = load(log);
        log(log);
        if (!load) {
            logger.log(LogLevel.ERROR, "Fatal error encountered - exiting");
            Bukkit.getPluginManager().disablePlugin(this);
            return false;
        }
        return true;
    }

    public LabelledLogger logger() { return logger; }
    public ConfigurationOptions configOptions() { return configOptions; }
    public ConfigurateSettings settings() { return settings; }
    public MiniMessageLocalization localization() { return localization; }
    public WrappedScheduler scheduler() { return scheduler; }

    public List<LoggingEntry> log(List<LoggingEntry> result) {
        for (LoggingEntry entry : result) {
            log(entry.level(), entry.exception(), entry.message());
        }
        return result;
    }

    public void saveDefaults() {
        if (!getDataFolder().exists()) {
            for (String resource : defaultResources()) {
                saveResource(resource, true);
            }
        }
    }

    public boolean load(List<LoggingEntry> result) {
        result.add(LoggingEntry.of(LogLevel.VERBOSE, "Loading data from %s", getDataFolder().getAbsolutePath()));
        loadSettings(result);
        if (settings.root() == null)
            return false;
        loadLanguages(result);
        return true;
    }

    protected void loadSettings(List<LoggingEntry> result) {
        settings.root(null);
        String settingsFile = settingsFile();
        try {
            settings.root(
                    HoconConfigurationLoader.builder()
                            .file(file(settingsFile))
                            .build()
                    .load(configOptions)
            );
            result.add(LoggingEntry.of(LogLevel.VERBOSE, "Loaded settings from %s", settingsFile));
        } catch (ParsingException e) {
            result.add(LoggingEntry.of(LogLevel.ERROR, e, "Could not load settings from %s", settingsFile));
            return;
        }

        Locale locale = setting(n -> n.get(Locale.class), "locale");
        if (locale != null)
            localization.defaultLocale(locale);

        logger.setLevel(LogLevel.forName(setting(n -> n.getString(defaultLogLevel().name()), "log_level")));
    }

    protected void loadLanguages(List<LoggingEntry> result) {
        localization.clear();
        TranslationsLoader.hocon(file(languageRoot()), localization).forEach(entry -> entry.apply(
                r -> result.add(LoggingEntry.of(LogLevel.VERBOSE, "Loaded language %s from %s", r.locale().toLanguageTag(), entry.path())),
                e -> result.add(LoggingEntry.of(LogLevel.WARN, e, "Could not load language from %s (%s)", entry.path(), e))
        ));
    }

    public void log(LogLevel level, Throwable e, String message, Object... args) {
        if (settings.root() == null || setting(n -> n.getBoolean(true), "print_detailed")) {
            logger.logDetail(level, e, message, args);
        } else {
            logger.logBasic(level, e, message, args);
        }
    }

    public void log(LogLevel level, String message, Object... args) {
        log(level, null, message, args);
    }

    public File file(String path) { return new File(getDataFolder(), path); }
    public NamespacedKey key(String name) { return keys.computeIfAbsent(name, __ -> new NamespacedKey(this, name)); }
    public <T> T setting(ConfigurateSettings.Function<T> function, Object... path) {
        return settings.get(function, path);
    }

    public Locale defaultLocale() { return localization.defaultLocale(); }
    public Locale locale(CommandSender sender) { return sender instanceof Player ? ((Player) sender).locale() : defaultLocale(); }
    public Component gen(Locale locale, String key, Object... args) { return localization.gen(locale, key, args); }
}
