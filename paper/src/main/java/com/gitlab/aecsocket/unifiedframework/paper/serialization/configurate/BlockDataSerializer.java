package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.ConfigurateSerializer;
import org.bukkit.Bukkit;
import org.bukkit.block.data.BlockData;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public class BlockDataSerializer implements TypeSerializer<BlockData>, ConfigurateSerializer {
    public static final BlockDataSerializer INSTANCE = new BlockDataSerializer();

    @Override
    public void serialize(Type type, @Nullable BlockData obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.set(obj.getMaterial().getKey().getKey());
        }
    }

    @Override
    public BlockData deserialize(Type type, ConfigurationNode node) throws SerializationException {
        String arg = node.getString();
        if (arg == null)
            throw new SerializationException(node, type, "Must be string of block data");
        return Bukkit.createBlockData(arg);
    }
}
