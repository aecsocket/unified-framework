package com.gitlab.aecsocket.unifiedframework.paper.gui;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages {@link GUIView}s.
 */
public class GUIManager {
    private final Map<HumanEntity, GUIView> viewMap = new HashMap<>();

    public GUIManager(Plugin plugin) {
        Bukkit.getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void onClick(InventoryClickEvent event) {
                GUIView view = getView(event.getWhoClicked());
                if (view != null) view.onClick(event);
            }

            @EventHandler
            public void onClose(InventoryCloseEvent event) {
                GUIView view = removeView(event.getPlayer());
                if (view != null) view.onClose(event);
            }

            @EventHandler
            public void onDrag(InventoryDragEvent event) {
                GUIView view = getView(event.getWhoClicked());
                if (view != null) view.onDrag(event);
            }

            @EventHandler
            public void onQuit(PlayerQuitEvent event) {
                viewMap.remove(event.getPlayer());
            }
        }, plugin);
    }

    public Map<HumanEntity, GUIView> getViewMap() { return viewMap; }
    public GUIView getView(HumanEntity player) { return viewMap.get(player); }
    public GUIView removeView(HumanEntity player) { return viewMap.remove(player); }
    public GUIView putView(GUIView view) { return viewMap.put(view.getPlayer(), view); }

    /**
     * Closes all open inventory views and removes them. Should be run on plugin disable.
     */
    public void closeAll() {
        viewMap.keySet().forEach(HumanEntity::closeInventory);
        viewMap.clear();
    }
}
