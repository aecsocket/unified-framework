package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.paper.util.data.ParticleData;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import org.bukkit.Particle;
import org.bukkit.util.Vector;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public class ParticleDataSerializer implements TypeSerializer<ParticleData> {
    public static final ParticleDataSerializer INSTANCE = new ParticleDataSerializer();

    @Override
    public void serialize(Type type, @Nullable ParticleData obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.node("particle").set(obj.particle());
            node.node("count").set(obj.count());
            node.node("size").set(obj.size());
            node.node("speed").set(obj.speed());
            node.node("offset").set(obj.offset());
            if (obj.particle().getDataType() != Void.class)
                node.node("data").set(obj.data());
        }
    }

    @Override
    public ParticleData deserialize(Type type, ConfigurationNode node) throws SerializationException {
        ParticleData obj = new ParticleData()
                .particle(node.node("particle").get(Particle.class))
                .count(node.node("count").getInt(0))
                .size(node.node("size").get(Vector3D.class, new Vector3D()))
                .speed(node.node("speed").getDouble(0))
                .offset(node.node("offset").get(Vector.class, new Vector()));
        if (obj.particle().getDataType() != Void.class)
            obj.data(node.node("data").get(obj.particle().getDataType()));
        return obj;
    }
}
