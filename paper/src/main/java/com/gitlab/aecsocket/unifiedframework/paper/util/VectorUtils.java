package com.gitlab.aecsocket.unifiedframework.paper.util;

import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3I;
import org.bukkit.util.BlockVector;
import org.bukkit.util.Vector;

public final class VectorUtils {
    private VectorUtils() {}

    public static Vector toBukkit(Vector3D v) { return new Vector(v.x(), v.y(), v.z()); }
    public static BlockVector toBukkit(Vector3I v) { return new BlockVector(v.x(), v.y(), v.z()); }

    public static Vector3D toUF(Vector v) { return new Vector3D(v.getX(), v.getY(), v.getZ()); }
    public static Vector3I toUF(BlockVector v) { return new Vector3I(v.getBlockX(), v.getBlockY(), v.getBlockZ()); }
}
