package com.gitlab.aecsocket.unifiedframework.paper.scheduler;

import com.gitlab.aecsocket.unifiedframework.core.scheduler.MinecraftScheduler;
import com.gitlab.aecsocket.unifiedframework.core.scheduler.Scheduler;
import com.gitlab.aecsocket.unifiedframework.core.scheduler.Task;
import com.gitlab.aecsocket.unifiedframework.core.scheduler.TaskContext;
import com.gitlab.aecsocket.unifiedframework.core.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class WrappedScheduler implements MinecraftScheduler {
    protected final class Context extends TaskContext.Generic {
        public Context(long elapsed, long delta, int iteration) {
            super(elapsed, delta, iteration);
        }

        @Override public Scheduler scheduler() { return WrappedScheduler.this; }
    }

    private final Plugin plugin;
    private final List<Integer> tasks = new ArrayList<>();

    public WrappedScheduler(Plugin plugin) {
        this.plugin = plugin;
    }

    public Plugin plugin() { return plugin; }

    protected void schedule(Runnable runnable, long delay) {
        AtomicInteger id = new AtomicInteger(0);
        id.set(Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            runnable.run();
            tasks.remove((Object) id.get());
        }, delay));
        tasks.add(id.get());
    }

    private void runnable(Task task, long start, long last, long interval, int iteration) {
        long time = System.currentTimeMillis();
        Context ctx = new Context(time - start, time - last, iteration);
        task.action.run(ctx);
        if (!ctx.cancelled && interval > 0) {
            schedule(() -> runnable(task, start, time, interval, iteration + 1), Utils.toTicks(task.interval));
        }
    }

    @Override
    public void run(Task task) {
        long start = System.currentTimeMillis();
        schedule(() -> runnable(task, start, start, Utils.toTicks(task.interval), 0), Utils.toTicks(task.delay));
    }

    @Override
    public void cancel() {
        tasks.forEach(Bukkit.getScheduler()::cancelTask);
        tasks.clear();
    }
}
