package com.gitlab.aecsocket.unifiedframework.paper.stat.impl.data;

import com.gitlab.aecsocket.unifiedframework.core.stat.impl.ArrayStat;
import com.gitlab.aecsocket.unifiedframework.paper.util.data.ParticleData;
import io.leangen.geantyref.TypeToken;

/**
 * An implementation of {@link ArrayStat} holding {@link ParticleData}.
 */
public class ParticleDataStat extends ArrayStat<ParticleData> {
    public ParticleDataStat(ParticleData[] defaultValue) { super(defaultValue); }
    public ParticleDataStat() {}
    @Override public TypeToken<ParticleData> arrayValueType() { return new TypeToken<>(){}; }
    @Override protected ParticleData[] newArray(int length) { return new ParticleData[length]; }
}
