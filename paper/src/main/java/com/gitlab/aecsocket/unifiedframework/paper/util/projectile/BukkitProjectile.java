package com.gitlab.aecsocket.unifiedframework.paper.util.projectile;

import com.gitlab.aecsocket.unifiedframework.core.scheduler.TaskContext;
import com.gitlab.aecsocket.unifiedframework.core.util.projectile.Projectile;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2I;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import com.gitlab.aecsocket.unifiedframework.paper.util.VectorUtils;
import org.bukkit.FluidCollisionMode;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.HumanEntity;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.function.Predicate;

public class BukkitProjectile extends Projectile<BukkitCollidable, BukkitRayTrace> {
    public static final Predicate<Entity> PREDICATE = e -> {
        if (e.isDead() || e.getType() == EntityType.DROPPED_ITEM)
            return false;
        if (e instanceof HumanEntity && ((HumanEntity) e).getGameMode() == GameMode.SPECTATOR)
            return false;
        return true;
    };
    public static final int REMOVE_REASON_UNLOADED = 1;
    public static final int REMOVE_REASON_COLLIDED = 2;

    protected final World world;
    protected double expansion = 0d;
    protected Entity source;
    private boolean leftSource;

    public BukkitProjectile(World world, Vector3D position, Vector3D velocity) {
        super(position, velocity);
        this.world = world;
    }

    public World world() { return world; }

    @Override public BukkitProjectile position(Vector3D position) { return (BukkitProjectile) super.position(position); }
    @Override public BukkitProjectile velocity(Vector3D velocity) { return (BukkitProjectile) super.velocity(velocity); }
    @Override public BukkitProjectile bounce(double bounce) { return (BukkitProjectile) super.bounce(bounce); }
    @Override public BukkitProjectile drag(double drag) { return (BukkitProjectile) super.drag(drag); }
    @Override public BukkitProjectile gravity(double gravity) { return (BukkitProjectile) super.gravity(gravity); }

    public double expansion() { return expansion; }
    public BukkitProjectile expansion(double expansion) { this.expansion = expansion; return this; }

    public Entity source() { return source; }
    public BukkitProjectile source(Entity source) { this.source = source; return this; }

    protected RayTraceResult rayTrace(double distance, Location position, Vector velocity) {
        return world.rayTrace(
                position, velocity,
                distance, FluidCollisionMode.NEVER, true, expansion,
                e -> PREDICATE.test(e) && (leftSource || !e.equals(source))
        );
    }

    @Override
    public void tick(TaskContext ctx) {
        Vector2I chunkPosition = new Vector2I((int) position.x(), (int) position.z()).divide(16);
        if (!world.isChunkLoaded(chunkPosition.x(), chunkPosition.y()) || position.y() < -128) {
            ctx.cancel();
            remove(REMOVE_REASON_UNLOADED);
            return;
        }
        super.tick(ctx);
    }

    @Override
    protected @NotNull BukkitRayTrace rayTrace(double distance) {
        Location position = VectorUtils.toBukkit(this.position).toLocation(world);
        Vector velocity = VectorUtils.toBukkit(this.velocity);
        RayTraceResult ray = rayTrace(distance, position, velocity);

        if (ray == null)
            return new BukkitRayTrace(null, this.position.add(this.velocity.normalize().multiply(distance)), null, null);
        return new BukkitRayTrace(ray);
    }

    @Override
    protected void step(TaskContext ctx, BukkitRayTrace ray, Vector3D from, Vector3D delta, double deltaLength) {
        if (ray.collided() == null)
            leftSource = true;
    }

    @Override
    protected boolean ignoreCollided(BukkitRayTrace ray, BukkitCollidable collided) {
        return !leftSource && collided.isEntity() && collided.entity().equals(source);
    }

    @Override
    protected void collide(TaskContext ctx, BukkitRayTrace ray, BukkitCollidable collided) {
        if (bounce <= 0) {
            ctx.cancel();
            remove(REMOVE_REASON_COLLIDED);
        } else
            bounce(ray);
    }
}
