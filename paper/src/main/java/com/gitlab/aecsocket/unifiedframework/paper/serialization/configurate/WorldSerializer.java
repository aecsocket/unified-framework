package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public class WorldSerializer implements TypeSerializer<World> {
    public static final WorldSerializer INSTANCE = new WorldSerializer();

    @Override
    public void serialize(Type type, @Nullable World obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.set(obj.getName());
        }
    }

    @Override
    public World deserialize(Type type, ConfigurationNode node) throws SerializationException {
        String arg = node.getString();
        if (arg == null)
            throw new SerializationException(node, type, "Must be string of world name");
        return Bukkit.getWorld(arg);
    }
}
