package com.gitlab.aecsocket.unifiedframework.paper.gui;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * An item which is interacted through in a {@link GUIView}.
 */
public interface GUIItem {
    /**
     * Creates the {@link ItemStack} representation of the item.
     * @param view The GUIView.
     * @return The ItemStack.
     */
    ItemStack createItem(GUIView view);

    default void onClick(GUIView view, InventoryClickEvent event) { event.setCancelled(true); }
    default void onRelease(GUIView view, InventoryClickEvent event, GUIItem existing) {}
    default void onInventoryMove(GUIView view, InventoryClickEvent event) {}
}
