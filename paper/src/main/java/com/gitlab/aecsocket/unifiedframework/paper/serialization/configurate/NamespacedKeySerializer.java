package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import org.bukkit.NamespacedKey;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public class NamespacedKeySerializer implements TypeSerializer<NamespacedKey> {
    public static final NamespacedKeySerializer INSTANCE = new NamespacedKeySerializer();

    @Override
    public void serialize(Type type, @Nullable NamespacedKey obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
             node.set(obj.toString());
        }
    }

    @Override
    public NamespacedKey deserialize(Type type, ConfigurationNode node) throws SerializationException {
        String arg = node.getString();
        if (arg == null)
            throw new SerializationException(node, type, "Must be string (namespace automatically resolves to `" + NamespacedKey.MINECRAFT + "`)");
        return NamespacedKey.minecraft(arg);
    }
}
