package com.gitlab.aecsocket.unifiedframework.paper.util.plugin;

import com.gitlab.aecsocket.unifiedframework.core.scheduler.TaskContext;
import org.bukkit.entity.Player;

public interface PlayerData {
    void load();
    void join(Player player);
    void save();

    default void tick(TaskContext ctx) {}
}
