package com.gitlab.aecsocket.unifiedframework.paper.util.data;

import com.gitlab.aecsocket.unifiedframework.paper.util.VectorUtils;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;

/**
 * Stores data which is able to spawn particles for a player or a world at a location.
 */
public class ParticleData implements Cloneable {
    private Particle particle;
    private int count;
    private Vector3D size = new Vector3D();
    private double speed;
    private transient Object data;
    private Vector offset = new Vector();

    public ParticleData() {}

    public ParticleData(ParticleData o) {
        particle = o.particle;
        count = o.count;
        size = o.size;
        speed = o.speed;
        data = o.data;
        offset = o.offset;
    }

    public Particle particle() { return particle; }
    public ParticleData particle(Particle particle) { this.particle = particle; return this; }

    public int count() { return count; }
    public ParticleData count(int count) { this.count = count; return this; }

    public Vector3D size() { return size; }
    public ParticleData size(Vector3D size) { this.size = size; return this; }

    public double speed() { return speed; }
    public ParticleData speed(double speed) { this.speed = speed; return this; }

    public Object data() { return data; }
    public ParticleData data(Object data) { this.data = data; return this; }

    public Vector offset() { return offset; }
    public ParticleData offset(Vector offset) { this.offset = offset; return this; }
    public ParticleData offset(Vector3D offset) { return offset(VectorUtils.toBukkit(offset)); }

    //region Spawning

    /**
     * Spawns the particle for the specified player at the location with the specified particle data.
     * If the particle does not support the provided data type, it will be spawned without the data.
     * Will always spawn forced.
     * @param player The player.
     * @param location The location to spawn at.
     * @param data The particle data.
     */
    public void spawn(Player player, Location location, Object data) {
        location = location.clone().add(offset);
        if (data != null && particle.getDataType().isAssignableFrom(data.getClass()))
            player.spawnParticle(
                    particle, location, count,
                    size.x(), size.y(), size.z(),
                    speed, data);
        else
            player.spawnParticle(
                    particle, location, count,
                    size.x(), size.y(), size.z(),
                    speed);
    }

    /**
     * Spawns the particle for the specified player at the location with the existing particle data.
     * Will always spawn forced.
     * @param player The player to spawn to.
     * @param location The location to spawn at.
     */
    public void spawn(Player player, Location location) { spawn(player, location, data); }

    /**
     * Spawns the particle for all players in the world at the location with the specified particle data.
     * Will always spawn forced.
     * @param location The location to spawn at.
     * @param data The particle data.
     */
    public void spawn(Location location, Object data) {
        location.getWorld().getPlayers().forEach(player -> spawn(player, location, data));
    }

    /**
     * Spawns the particle for all players in the world at the location with the existing particle data.
     * Will always spawn forced.
     * @param location The location to spawn at.
     */
    public void spawn(Location location) { spawn(location, data); }

    //endregion

    @Override
    public String toString() {
        return particle + " x" + count + " @" + speed + " " + size + " + " + offset;
    }

    /**
     * Spawns a collection of ParticleData.
     * @param player The player to spawn to.
     * @param location The location to spawn at.
     * @param data The particle data.
     * @param function The function to apply to each particle data. Can be null.
     * @param particles The particles to spawn.
     * @see #spawn(Player, Location, Object)
     */
    public static void spawn(Player player, Location location, Object data, @Nullable Function<ParticleData, ParticleData> function, ParticleData... particles) {
        if (particles == null) return;
        for (ParticleData particle : particles) {
            if (function != null)
                particle = function.apply(particle);
            particle.spawn(player, location, data);
        }
    }

    /**
     * Spawns a collection of ParticleData.
     * @param player The player to spawn to.
     * @param location The location to spawn at.
     * @param data The particle data.
     * @param particles The particles to spawn.
     * @see #spawn(Player, Location, Object)
     */
    public static void spawn(Player player, Location location, Object data, ParticleData... particles) {
        spawn(player, location, data, null, particles);
    }


    /**
     * Spawns a collection of ParticleData.
     * @param player The player to spawn to.
     * @param location The location to spawn at.
     * @param function The function to apply to each particle data. Can be null.
     * @param particles The particles to spawn.
     * @see #spawn(Player, Location)
     */
    public static void spawn(Player player, Location location, @Nullable Function<ParticleData, ParticleData> function, ParticleData... particles) {
        if (particles == null) return;
        for (ParticleData particle : particles) {
            if (function != null)
                particle = function.apply(particle);
            particle.spawn(player, location);
        }
    }

    /**
     * Spawns a collection of ParticleData.
     * @param player The player to spawn to.
     * @param location The location to spawn at.
     * @param particles The particles to spawn.
     * @see #spawn(Player, Location)
     */
    public static void spawn(Player player, Location location, ParticleData... particles) {
        spawn(player, location, null, particles);
    }



    /**
     * Spawns a collection of ParticleData.
     * @param location The location to spawn at.
     * @param data The particle data.
     * @param function The function to apply to each particle data. Can be null.
     * @param particles The particles to spawn.
     * @see #spawn(Location, Object)
     */
    public static void spawn(Location location, Object data, @Nullable Function<ParticleData, ParticleData> function, ParticleData... particles) {
        if (particles == null) return;
        for (ParticleData particle : particles) {
            if (function != null)
                particle = function.apply(particle);
            particle.spawn(location, data);
        }
    }

    /**
     * Spawns a collection of ParticleData.
     * @param location The location to spawn at.
     * @param data The particle data.
     * @param particles The particles to spawn.
     * @see #spawn(Location, Object)
     */
    public static void spawn(Location location, Object data, ParticleData... particles) {
        spawn(location, data, null, particles);
    }


    /**
     * Spawns a collection of ParticleData.
     * @param location The location to spawn at.
     * @param function The function to apply to each particle data. Can be null.
     * @param particles The particles to spawn.
     * @see #spawn(Location)
     */
    public static void spawn(Location location, @Nullable Function<ParticleData, ParticleData> function, ParticleData... particles) {
        if (particles == null) return;
        for (ParticleData particle : particles) {
            if (function != null)
                particle = function.apply(particle);
            particle.spawn(location);
        }
    }

    /**
     * Spawns a collection of ParticleData.
     * @param location The location to spawn at.
     * @param particles The particles to spawn.
     * @see #spawn(Location)
     */
    public static void spawn(Location location, ParticleData... particles) {
        spawn(location, null, particles);
    }
}
