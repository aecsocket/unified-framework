package com.gitlab.aecsocket.unifiedframework.paper.util.data;

import com.gitlab.aecsocket.unifiedframework.core.util.Utils;
import org.apache.commons.lang.NotImplementedException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Supplier;

/**
 * Stores data which is able to play a sound for a player or a world at a location.
 */
public class SoundData implements Cloneable {
    /** The default speed of sound, in m/s. */
    public static final double DEFAULT_SPEED = 340.29;

    private static Method toMinecraft;
    private static String pkgVersion;

    static {
        String name = Bukkit.getServer().getClass().getPackage().getName();
        pkgVersion = name.substring(name.lastIndexOf(".") + 1);
    }

    private transient final Plugin plugin;
    private String sound;
    private SoundCategory category = SoundCategory.MASTER;
    private float volume = 1;
    private float pitch = 1;
    private double dropoff;
    private double range = 4;
    private double speed = DEFAULT_SPEED;
    private long delay;

    public SoundData(Plugin plugin) {
        this.plugin = plugin;
    }

    public SoundData(SoundData o) {
        plugin = o.plugin;
        sound = o.sound;
        category = o.category;
        volume = o.volume;
        pitch = o.pitch;
        dropoff = o.dropoff;
        range = o.range;
        speed = o.speed;
    }

    public Plugin plugin() { return plugin; }

    public String sound() { return sound; }
    public SoundData sound(String sound) { this.sound = sound; return this; }

    public SoundCategory category() { return category; }
    public SoundData category(SoundCategory category) { this.category = category; return this; }

    public float volume() { return volume; }
    public SoundData volume(float volume) { this.volume = volume; return this; }

    public float pitch() { return pitch; }
    public SoundData pitch(float pitch) { this.pitch = pitch; return this; }

    public double dropoff() { return dropoff; }
    public SoundData dropoff(double dropoff) { this.dropoff = dropoff; return this; }

    public double range() { return range; }
    public SoundData range(double range) { this.range = range; return this; }

    public double speed() { return speed; }
    public SoundData speed(double speed) { this.speed = speed; return this; }

    public long delay() { return delay; }
    public SoundData delay(long delay) { this.delay = delay; return this; }

    /**
     * Plays the sound at the current moment to the specified player at a location.
     * @param player The player to play to.
     * @param location The location to play at.
     */
    public void playNow(Player player, Location location) {
        Location playerLoc = player.getLocation();
        double distance = location.distance(playerLoc);
        if (distance > range)
            return;

        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            // TODO probably make this more precise
            Location loc = distance == 0 ? playerLoc : playerLoc.add(location.clone().subtract(playerLoc).toVector().normalize().multiply(8));
            player.playSound(
                    loc,
                    sound, category,
                    (float) (volume * (1 - Math.min(1, (distance - dropoff) / (range - dropoff)))),
                    pitch
            );
        }, (int) (distance / (speed / Utils.TPS)));
    }

    /**
     * Plays the sound at the current moment at a specific location.
     * @param location The location to play at.
     */
    public void playNow(Location location) {
        location.getWorld().getPlayers().forEach(player -> playNow(player, location));
    }

    private void delayed(Runnable run) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(
                plugin,
                run,
                (int) Utils.toTicks(delay)
        );
    }

    /**
     * Plays the sound after the delay to the specified player at a location.
     * @param player The player.
     * @param location A function to get the location to play at.
     */
    public void play(Player player, Supplier<Location> location) { delayed(() -> playNow(player, location.get())); }

    /**
     * Plays the sound after the delay at a specific location.
     * @param location A function to get the location to play at.
     */
    public void play(Supplier<Location> location) { delayed(() -> playNow(location.get())); }

    public SoundData clone() { try { return (SoundData)super.clone(); } catch (CloneNotSupportedException e) { return null; } }

    @Override
    public String toString() {
        return sound + ":" + category + " " + volume + "," + pitch + " (" + dropoff + "m -> " + range + "m)" + " @" + speed + "m/s | " + delay + "ms";
    }

    /**
     * Uses an underlying CraftBukkit implementation to get the String name of a sound from a {@link Sound}.
     * @param sound The Sound.
     * @return The Minecraft key of the sound.
     * @throws NotImplementedException If the underlying CraftBukkit method could not be called.
     */
    public static String toMinecraft(Sound sound) throws NotImplementedException {
        try {
            if (toMinecraft == null)
                toMinecraft = Class.forName("org.bukkit.craftbukkit." + pkgVersion + ".CraftSound").getMethod("getSound", Sound.class);
            return (String) toMinecraft.invoke(null, sound);
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | ClassCastException e) {
            throw new NotImplementedException("Could not get underlying CraftBukkit method", e);
        }
    }

    /**
     * Plays a collection of SoundData.
     * @param player The player to play to.
     * @param location The location to play at.
     * @param sounds The sounds to play.
     */
    public static void playNow(Player player, Location location, SoundData... sounds) {
        if (sounds == null) return;
        for (SoundData sound : sounds)
            sound.playNow(player, location);
    }

    /**
     * Plays a collection of SoundData.
     * @param location The location to play at.
     * @param sounds The sounds to play.
     */
    public static void playNow(Location location, SoundData... sounds) {
        if (sounds == null) return;
        for (SoundData sound : sounds)
            sound.playNow(location);
    }

    /**
     * Plays a collection of SoundData.
     * @param player The player to play to.
     * @param location A function to get the location to play at.
     * @param sounds The sounds to play.
     */
    public static void play(Player player, Supplier<Location> location, SoundData... sounds) {
        if (sounds == null) return;
        for (SoundData sound : sounds)
            sound.play(player, location);
    }

    /**
     * Plays a collection of SoundData.
     * @param location A function to get the location to play at.
     * @param sounds The sounds to play.
     */
    public static void play(Supplier<Location> location, SoundData... sounds) {
        if (sounds == null) return;
        for (SoundData sound : sounds)
            sound.play(location);
    }
}
