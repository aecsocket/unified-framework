package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.ConfigurateSerializer;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.List;

public class DustOptionsSerializer implements TypeSerializer<Particle.DustOptions>, ConfigurateSerializer {
    public static final DustOptionsSerializer INSTANCE = new DustOptionsSerializer();

    @Override
    public void serialize(Type type, Particle.@Nullable DustOptions obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.appendListNode().set(obj.getColor());
            node.appendListNode().set(obj.getSize());
        }
    }

    @Override
    public Particle.DustOptions deserialize(Type type, ConfigurationNode node) throws SerializationException {
        List<? extends ConfigurationNode> list = asList(node, Color.class, "color", "size");
        return new Particle.DustOptions(
                list.get(0).get(Color.class),
                list.get(1).getFloat()
        );
    }
}
