package com.gitlab.aecsocket.unifiedframework.paper.util.projectile;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.util.RayTraceResult;

import java.util.Objects;

public class BukkitCollidable {
    private final Block block;
    private final Entity entity;

    public BukkitCollidable(Block block) {
        this.block = block;
        entity = null;
    }

    public BukkitCollidable(Entity entity) {
        block = null;
        this.entity = entity;
    }

    public boolean isBlock() { return block != null; }
    public boolean isEntity() { return entity != null; }

    public Block block() { return block; }
    public Entity entity() { return entity; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BukkitCollidable that = (BukkitCollidable) o;
        return
                ((block == null) == (that.block == null) && (entity == null) == (that.entity == null))
                || (block != null && block.getLocation().equals(that.block.getLocation()))
                || (entity != null && entity.getEntityId() == that.entity.getEntityId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(block, entity);
    }

    @Override public String toString() { return "<" + (isEntity() ? entity : block) + ">"; }

    public static BukkitCollidable wrap(Object obj) {
        if (obj instanceof Block)
            return new BukkitCollidable((Block) obj);
        if (obj instanceof Entity)
            return new BukkitCollidable((Entity) obj);
        throw new IllegalArgumentException("Must be either Block or Entity");
    }

    public static BukkitCollidable wrap(RayTraceResult ray) {
        if (ray.getHitBlock() != null)
            return new BukkitCollidable(ray.getHitBlock());
        else
            return new BukkitCollidable(ray.getHitEntity());
    }

    public static Object unwrap(RayTraceResult ray) {
        return ray.getHitBlock() == null ? ray.getHitEntity() : ray.getHitBlock();
    }
}
