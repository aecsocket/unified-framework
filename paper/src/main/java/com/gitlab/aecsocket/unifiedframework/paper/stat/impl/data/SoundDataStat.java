package com.gitlab.aecsocket.unifiedframework.paper.stat.impl.data;

import com.gitlab.aecsocket.unifiedframework.paper.util.data.SoundData;
import io.leangen.geantyref.TypeToken;
import com.gitlab.aecsocket.unifiedframework.core.stat.impl.ArrayStat;

/**
 * An implementation of {@link ArrayStat} holding {@link SoundData}.
 */
public class SoundDataStat extends ArrayStat<SoundData> {
    public SoundDataStat(SoundData[] defaultValue) { super(defaultValue); }
    public SoundDataStat() {}
    @Override public TypeToken<SoundData> arrayValueType() { return new TypeToken<>(){}; }
    @Override protected SoundData[] newArray(int length) { return new SoundData[length]; }
}
