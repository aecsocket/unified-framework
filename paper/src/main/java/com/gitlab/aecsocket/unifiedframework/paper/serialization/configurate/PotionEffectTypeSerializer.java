package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import org.bukkit.potion.PotionEffectType;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public class PotionEffectTypeSerializer implements TypeSerializer<PotionEffectType> {
    public static final PotionEffectTypeSerializer INSTANCE = new PotionEffectTypeSerializer();

    @Override
    public void serialize(Type type, @Nullable PotionEffectType obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.set(obj.getName());
        }
    }

    @Override
    public PotionEffectType deserialize(Type type, ConfigurationNode node) throws SerializationException {
        String arg = node.getString();
        if (arg == null)
            throw new SerializationException(node, type, "Must be string of internal potion effect type");
        return PotionEffectType.getByName(arg);
    }
}
