package com.gitlab.aecsocket.unifiedframework.paper.util;

import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3I;
import org.bukkit.*;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MainHand;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

/**
 * Generic plugin utilities.
 */
public final class BukkitUtils {
    /** Token used in commands to represent the sender player. */
    public static final String SENDER = ".";

    private BukkitUtils() {}

    /**
     * Gets a {@link File} relative to a {@link Plugin}'s data folder ({@link Plugin#getDataFolder()}).
     * @param plugin The {@link Plugin}.
     * @param path The string path to the file.
     * @return The {@link File}.
     */
    public static File getRelative(Plugin plugin, String path) {
        return plugin.getDataFolder().toPath().resolve(path).toFile();
    }


    /**
     * Gives a player items, first attempting to put it in their inventory, otherwise dropping it
     * onto the floor at their position.
     * @param player The player to give to.
     * @param items The items to give.
     */
    public static void giveItem(Player player, ItemStack... items) {
        PlayerInventory inv = player.getInventory();
        Location location = player.getLocation();
        World world = player.getWorld();
        for (ItemStack item : items) {
            if (inv.addItem(item).size() > 0) {
                Item spawn = world.dropItem(location, item);
                spawn.setPickupDelay(0);
            }
        }
    }

    /**
     * Gets a list of player target entries while typing a command, including {@link BukkitUtils#SENDER} if the sender is a {@link Player}.
     * @param sender The {@link CommandSender} who is sending the command.
     * @return The list of command targets.
     */
    public static List<String> getCommandPlayerEntries(CommandSender sender) {
        List<String> result = new ArrayList<>();
        Bukkit.getOnlinePlayers().forEach(player -> result.add(player.getName()));
        if (sender instanceof Player) result.add(SENDER);
        return result;
    }

    /**
     * Gets a player target during command handling, following the same specification as {@link BukkitUtils#getCommandPlayerEntries(CommandSender)}.
     * <p>
     * This first handles the {@link BukkitUtils#SENDER} case, then through a username, and finally through a UUID.
     * @param arg The text typed.
     * @param sender The {@link CommandSender} who is sending the command.
     * @return The player target.
     */
    public static Player getCommandTarget(String arg, CommandSender sender) {
        if (sender instanceof Player && arg.equals(SENDER)) return (Player) sender;
        // Try with name
        Player result = Bukkit.getPlayer(arg);
        if (result != null) return result;
        try {
            // Try with UUID
            return Bukkit.getPlayer(UUID.fromString(arg));
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    /**
     * Gets if an {@link Action} is a right click action or not.
     * @param action The action.
     * @return The result.
     */
    public static boolean isRightClick(Action action) { return action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK; }

    /**
     * Gets if a {@link PlayerInteractEvent} has a right click action or not.
     * @param event The event.
     * @return The result.
     */
    public static boolean isRightClick(PlayerInteractEvent event) { return isRightClick(event.getAction()); }

    /**
     * Modifies the meta of a provided {@link ItemStack} and sets it afterwards.
     * @param stack The stack to modify.
     * @param consumer The consumer of the meta.
     * @return The modified stack.
     */
    public static ItemStack modMeta(ItemStack stack, Consumer<ItemMeta> consumer) {
        if (stack == null) return null;
        ItemMeta meta = stack.getItemMeta();
        if (meta == null) return stack;
        consumer.accept(meta);
        stack.setItemMeta(meta);
        return stack;
    }

    /**
     * Gets if an {@link ItemStack} is null or air.
     * @param stack The stack.
     * @return The result.
     */
    public static boolean empty(ItemStack stack) { return stack == null || stack.getType() == Material.AIR; }

    /**
     * Helper method for using the registered provider for a Bukkit service.
     * @param serviceType The type of service to get the provider for.
     * @param consumer The consumer of the provider.
     * @param <T>  The type of service to get the provider for.
     */
    public static <T> void useService(Class<T> serviceType, Consumer<T> consumer) {
        RegisteredServiceProvider<T> provider = Bukkit.getServicesManager().getRegistration(serviceType);
        if (provider != null) consumer.accept(provider.getProvider());
    }

    /**
     * Helper method for getting a registered provider for a Bukkit service.
     * @param serviceType The type of service to get the provider for.
     * @param <T> The type of service to get the provider for.
     * @return An Optional of the provider.
     */
    public static <T> Optional<T> getService(Class<T> serviceType) {
        RegisteredServiceProvider<T> provider = Bukkit.getServicesManager().getRegistration(serviceType);
        return provider == null ? Optional.empty() : Optional.of(provider.getProvider());
    }

    /**
     * Gets a 3-dimensional integer vector as the normal of a {@link BlockFace}. This can be used to deflect against.
     * @param face The face.
     * @return The vector, or null if the face is not supported.
     */
    public static Vector3I getNormal(BlockFace face) {
        switch (face) {
            case UP: return new Vector3I(0, 1, 0);
            case DOWN: return new Vector3I(0, -1, 0);

            case NORTH: return new Vector3I(0, 0, -1);
            case SOUTH: return new Vector3I(0, 0, 1);

            case EAST: return new Vector3I(-1, 0, 0);
            case WEST: return new Vector3I(1, 0, 0);

            default: return null;
        }
    }

    /**
     * Gets a Vector as the normal of a {@link BlockFace}. This can be used to deflect against.
     * @param face The face.
     * @return The Vector, or null if the face is not supported.
     */
    public static Vector getBukkitNormal(BlockFace face) {
        return VectorUtils.toBukkit(getNormal(face));
    }

    /**
     * Gets the result of <code>v - (v2 * ((v . v2) * 2))</code>.
     * @param v Vector 1. Will be modified.
     * @param v2 Vector 2. Will be modified.
     * @return The modified Vector 1.
     */
    public static Vector deflect(Vector v, Vector v2) { return v.subtract(v2.multiply(v.dot(v2) * 2)); }

    /**
     * Gets the result of {@link BukkitUtils#deflect(Vector, Vector)}, with v2 as {@link BukkitUtils#getBukkitNormal(BlockFace)}.
     * @param v Vector 1. Will be modified.
     * @param face The face to deflect off.
     * @return The modified Vector 1.
     */
    public static Vector deflect(Vector v, BlockFace face) { return deflect(v, getBukkitNormal(face)); }

    /**
     * If the entity provided is a LivingEntity, returns {@link LivingEntity#getEyeLocation()}, otherwise {@link Entity#getLocation()}.
     * @param entity The entity.
     * @return The location.
     */
    public static Location optEye(Entity entity) { return entity instanceof LivingEntity ? ((LivingEntity) entity).getEyeLocation() : entity.getLocation(); }

    /**
     * Gets a position relative to an entity's eye.
     * @param eye The entity's eye position.
     * @param offset The offset from the eye. X = left/right, Y = up/down, Z = near/far
     * @return The position with the relative offset.
     * @see LivingEntity#getEyeLocation()
     */
    public static Location getFacingRelative(Location eye, Vector offset) {
        eye = eye.clone();
        Vector facing = eye.getDirection();

        Vector xzTangent = new Vector(-facing.getZ(), 0.0, facing.getX()).normalize();
        Vector yTangent = xzTangent.getCrossProduct(facing).normalize();

        Vector finalOffset = xzTangent.clone().multiply(offset.getX())
                .add(facing.multiply(offset.getZ())
                        .add(yTangent.multiply(offset.getY())));

        return eye.add(finalOffset);
    }

    /**
     * Gets a position relative to an entity's eye.
     * @param entity The entity.
     * @param offset The offset from the eye. X = left/right, Y = up/down, Z = near/far
     * @return The relative position.
     * @see LivingEntity#getEyeLocation()
     */
    public static Location getFacingRelative(LivingEntity entity, Vector offset) {
        Location location = entity.getEyeLocation();
        if (entity instanceof Player && ((Player) entity).getMainHand() == MainHand.LEFT)
            offset = offset.clone().setX(-offset.getX());
        return getFacingRelative(location, offset);
    }

    /**
     * Checks if there is an entity or block between an entity's {@link BukkitUtils#optEye(Entity)} and a specified offset.
     * Ignores the entity itself.
     * @param entity The entity.
     * @param to The location to check up to.
     * @return If it is obstructed.
     */
    public static boolean isObstructed(Entity entity, Location to) {
        Location start = optEye(entity);
        Vector delta = to.clone().subtract(start).toVector();
        if (delta.getX() + delta.getY() + delta.getZ() == 0) return false;
        RayTraceResult ray = start.getWorld().rayTrace(start, delta, delta.length(), FluidCollisionMode.NEVER, true, 0, e -> e != entity && e instanceof LivingEntity);
        return ray != null;
    }

    /**
     * In a hotbar-related event, gets how far the scroll was up or down.
     * @param from The from slot.
     * @param to The to slot.
     * @return The scroll length. {@literal >} 0 is up, {@literal <} 0 is down.
     */
    public static int scrollDistance(int from, int to) {
        if (from == 0 && to > 4)
            return 9 - to;
        if (from == 8 && to < 4)
            return -to;
        return from - to;
    }

    /**
     * In a {@link PlayerItemHeldEvent}, gets how far the scroll was up or down.
     * @param event The event.
     * @return The scroll length. {@literal >} 0 is up, {@literal <} 0 is down.
     */
    public static int scrollDistance(PlayerItemHeldEvent event) {
        return scrollDistance(event.getPreviousSlot(), event.getNewSlot());
    }
}
