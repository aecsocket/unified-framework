package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.paper.util.data.SoundData;
import org.bukkit.SoundCategory;
import org.bukkit.plugin.Plugin;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public class SoundDataSerializer implements TypeSerializer<SoundData> {
    private final Plugin plugin;

    public SoundDataSerializer(Plugin plugin) {
        this.plugin = plugin;
    }

    public Plugin plugin() { return plugin; }

    @Override
    public void serialize(Type type, @Nullable SoundData obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.node("sound").set(obj.sound());
            node.node("category").set(obj.category());
            node.node("volume").set(obj.volume());
            node.node("pitch").set(obj.pitch());
            node.node("dropoff").set(obj.dropoff());
            node.node("range").set(obj.range());
            node.node("speed").set(obj.speed());
            node.node("delay").set(obj.delay());
        }
    }

    @Override
    public SoundData deserialize(Type type, ConfigurationNode node) throws SerializationException {
        SoundData obj = new SoundData(plugin);
        return obj
                .sound(node.node("sound").getString())
                .category(node.node("category").get(SoundCategory.class, obj.category()))
                .volume(node.node("volume").getFloat(obj.volume()))
                .pitch(node.node("pitch").getFloat(obj.pitch()))
                .dropoff(node.node("dropoff").getDouble(obj.dropoff()))
                .range(node.node("range").getDouble(obj.range()))
                .speed(node.node("speed").getDouble(obj.speed()))
                .delay(node.node("delay").getLong(obj.delay()));
    }
}
