package com.gitlab.aecsocket.unifiedframework.paper.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

/**
 * A wrapper around an {@link InventoryView} holding references useful for {@link GUI}s.
 */
public final class GUIView {
    private final GUI gui;
    private final InventoryView view;
    private final Map<Integer, GUIItem> items;
    private GUIItem cursor;

    public GUIView(GUI gui, InventoryView view, Map<Integer, GUIItem> items) {
        this.gui = gui;
        this.view = view;
        this.items = items;
    }

    public GUI getGUI() { return gui; }
    public InventoryView getView() { return view; }
    public Map<Integer, GUIItem> getItems() { return items; }

    public GUIItem getCursor() { return cursor; }
    public void setCursor(GUIItem cursor) { this.cursor = cursor; }

    public Inventory getInventory() { return view.getTopInventory(); }
    public Player getPlayer() { return (Player) view.getPlayer(); }

    public ItemStack getRawCursor() { return view.getCursor(); }
    public void setRawCursor(ItemStack item) { view.setCursor(item); }

    /**
     * Updates the underlying inventory.
     */
    public void updateItems() {
        Inventory inventory = getInventory();
        inventory.clear();
        items.forEach((slot, item) -> {
            if (slot < inventory.getSize())
                inventory.setItem(slot, item.createItem(this));
        });
    }

    /**
     * Updates the underlying cursor.
     */
    public void updateCursor() {
        view.setCursor(cursor == null ? null : cursor.createItem(this));
    }

    /**
     * Reopens the GUI, ensuring that the cursor is not lost.
     */
    public void reopen() {
        ItemStack cursor = getRawCursor();
        setRawCursor(null);
        GUIView newView = gui.createView(getPlayer());
        newView.setRawCursor(cursor);
        newView.updateItems();
    }

    public void clickItem(int slot, InventoryClickEvent event) {
        GUIItem item = items.get(slot);
        if (cursor == null) {
            if (item == null)
                clickEmptySlot(slot, event);
            else
                clickItem(item, slot, event);
        } else {
            cursor.onRelease(this, event, item);
            if (!event.isCancelled()) {
                items.put(slot, cursor);
                cursor = item;
            }
            if (item != null)
                clickItem(item, slot, event);
        }
    }

    private void clickItem(GUIItem item, int slot, InventoryClickEvent event) {
        item.onClick(this, event);
        if (!event.isCancelled()) {
            cursor = item;
            items.remove(slot);
        }
    }

    protected void clickEmptySlot(int slot, InventoryClickEvent event) {
        event.setCancelled(true);
    }

    public void onClick(InventoryClickEvent event) { gui.onClick(this, event); }
    public void onClose(InventoryCloseEvent event) { gui.onClose(this, event); }
    public void onDrag(InventoryDragEvent event) { gui.onDrag(this, event); }
}
