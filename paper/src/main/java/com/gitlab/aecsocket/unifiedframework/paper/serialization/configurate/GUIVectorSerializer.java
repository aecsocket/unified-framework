package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.paper.gui.GUIVector;
import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.vector.Vector2ISerializer;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2I;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public class GUIVectorSerializer implements TypeSerializer<GUIVector> {
    public static final GUIVectorSerializer INSTANCE = new GUIVectorSerializer();
    private static final Vector2ISerializer delegate = new Vector2ISerializer();

    @Override
    public void serialize(Type type, @Nullable GUIVector obj, ConfigurationNode node) throws SerializationException {
        delegate.serialize(type, obj, node);
    }

    @Override
    public GUIVector deserialize(Type type, ConfigurationNode node) throws SerializationException {
        Vector2I result = delegate.deserialize(type, node);
        return new GUIVector(result.x(), result.y());
    }
}
