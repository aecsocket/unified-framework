package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import org.bukkit.inventory.ItemStack;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Base64;

public class ItemStackSerializer implements TypeSerializer<ItemStack> {
    public static final ItemStackSerializer INSTANCE = new ItemStackSerializer();

    @Override
    public void serialize(Type type, @Nullable ItemStack obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.set(String.class, Base64.getEncoder().encodeToString(obj.serializeAsBytes()));
        }
    }

    @Override
    public ItemStack deserialize(Type type, ConfigurationNode node) throws SerializationException {
        try {
            return ItemStack.deserializeBytes(Base64.getDecoder().decode(node.getString()));
        } catch (IllegalArgumentException e) {
            throw new SerializationException(node, ItemStack.class, "Must be valid base-64 string");
        }
    }
}
