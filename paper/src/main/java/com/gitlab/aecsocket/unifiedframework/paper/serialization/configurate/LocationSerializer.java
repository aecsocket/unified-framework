package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.ConfigurateSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class LocationSerializer implements TypeSerializer<Location>, ConfigurateSerializer {
    public static final LocationSerializer INSTANCE = new LocationSerializer();

    @Override
    public void serialize(Type type, @Nullable Location obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.setList(Object.class, Arrays.asList(
                    obj.getWorld() == null ? null : obj.getWorld().getName(),
                    obj.getX(), obj.getY(), obj.getZ(),
                    obj.getYaw(), obj.getPitch()
            ));
        }
    }

    @Override
    public Location deserialize(Type type, ConfigurationNode node) throws SerializationException {
        List<? extends ConfigurationNode> list = asList(node, Location.class, "world_name", "x", "y", "z");
        return new Location(
                Bukkit.getWorld(list.get(0).getString("")),
                list.get(1).getDouble(),
                list.get(2).getDouble(),
                list.get(3).getDouble(),
                list.size() >= 5 ? list.get(4).getFloat(0) : 0,
                list.size() >= 6 ? list.get(5).getFloat(0) : 0
        );
    }
}
