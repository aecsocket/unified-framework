package com.gitlab.aecsocket.unifiedframework.paper.util;

import com.gitlab.aecsocket.unifiedframework.paper.util.projectile.BukkitCollidable;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public final class RayTraceUtils {
    public static final double HIT_PRECISION = 0.1;

    private RayTraceUtils() {}

    public static List<RayTraceResult> continuous(final Location origin, final Vector direction, double distance, FluidCollisionMode fluidCollisionMode, boolean ignorePassableBlocks, double expansion, Predicate<Entity> predicate) {
        List<RayTraceResult> result = new ArrayList<>();
        World world = origin.getWorld();
        Location current = origin.clone();
        Vector cDirection = direction.clone().normalize();

        BukkitCollidable lastHit = null;
        while (distance > 0) {
            RayTraceResult ray = world.rayTrace(current, direction, distance, fluidCollisionMode, ignorePassableBlocks, expansion, predicate);
            if (ray == null) {
                return result;
            }

            // we use HIT_PRECISION so that, if hitting anything, we don't get stuck hitting the same thing over and over
            double travelled = current.toVector().distance(ray.getHitPosition()) + HIT_PRECISION;
            distance -= travelled;
            current.add(cDirection.clone().multiply(travelled));

            // use a BukkitCollidable since it's designed to deal with entity|block unions and #equals comparisons
            BukkitCollidable hit = BukkitCollidable.wrap(ray);
            if (lastHit == null || !lastHit.equals(hit)) {
                result.add(ray);
                lastHit = hit;
            }
        }
        return result;
    }
}
