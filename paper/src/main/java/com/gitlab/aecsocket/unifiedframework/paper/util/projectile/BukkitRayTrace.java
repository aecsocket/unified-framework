package com.gitlab.aecsocket.unifiedframework.paper.util.projectile;

import com.gitlab.aecsocket.unifiedframework.core.util.projectile.RayTrace;
import com.gitlab.aecsocket.unifiedframework.paper.util.BukkitUtils;
import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector3D;
import com.gitlab.aecsocket.unifiedframework.paper.util.VectorUtils;
import org.bukkit.block.BlockFace;
import org.bukkit.util.RayTraceResult;

public class BukkitRayTrace extends RayTrace<BukkitCollidable> {
    private final BlockFace face;

    public BukkitRayTrace(BukkitCollidable collided, Vector3D position, Vector3D collisionNormal, BlockFace face) {
        super(collided, position, collisionNormal);
        this.face = face;
    }

    public BukkitRayTrace(RayTraceResult ray) {
        this(
                BukkitCollidable.wrap(ray),
                VectorUtils.toUF(ray.getHitPosition()),
                BukkitUtils.getNormal(ray.getHitBlockFace()).toDoubles(),
                ray.getHitBlockFace()
        );
    }

    public BlockFace face() { return face; }
}
