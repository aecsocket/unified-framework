package com.gitlab.aecsocket.unifiedframework.paper.gui;

import com.gitlab.aecsocket.unifiedframework.core.util.vector.Vector2I;

public class GUIVector extends Vector2I {
    public GUIVector(int x, int y) { super(x, y); }
    public GUIVector(int v) { super(v); }
    public GUIVector() {}
    public GUIVector(Vector2I o) { this(o.x(), o.y()); }

    public int slot(int width) { return (y() * width) + x(); }
    public int slot() { return slot(9); }

    @Override
    public String toString() {
        return "G" + super.toString();
    }
}
