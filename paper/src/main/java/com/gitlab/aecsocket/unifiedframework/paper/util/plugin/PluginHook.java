package com.gitlab.aecsocket.unifiedframework.paper.util.plugin;

import org.spongepowered.configurate.objectmapping.ObjectMapper;
import org.spongepowered.configurate.serialize.TypeSerializerCollection;

public interface PluginHook {
    default void setupSerializer(TypeSerializerCollection.Builder serializers, ObjectMapper.Factory.Builder mapperFactory) {}
    default void preLoad() {}
    default void postLoad() {}
}
