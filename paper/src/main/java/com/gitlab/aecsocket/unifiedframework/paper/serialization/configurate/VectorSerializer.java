package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.ConfigurateSerializer;
import org.bukkit.util.Vector;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class VectorSerializer implements TypeSerializer<Vector>, ConfigurateSerializer {
    public static final VectorSerializer INSTANCE = new VectorSerializer();

    @Override
    public void serialize(Type type, @Nullable Vector obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.setList(Double.class, Arrays.asList(
                    obj.getX(), obj.getY(), obj.getZ()
            ));
        }
    }

    @Override
    public Vector deserialize(Type type, ConfigurationNode node) throws SerializationException {
        List<? extends ConfigurationNode> list = asList(node, Vector.class, "x", "y", "z");
        return new Vector(
                list.get(0).getDouble(),
                list.get(1).getDouble(),
                list.get(2).getDouble()
        );
    }
}
