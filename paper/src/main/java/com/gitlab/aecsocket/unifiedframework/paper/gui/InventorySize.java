package com.gitlab.aecsocket.unifiedframework.paper.gui;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

/**
 * Represents either the size in rows or in a {@link InventoryType} of an {@link Inventory}.
 */
public class InventorySize {
    private Integer rows;
    private InventoryType type;

    public InventorySize(int rows) {
        this.rows = rows;
    }

    public InventorySize(InventoryType type) {
        this.type = type;
    }

    public Integer getRows() { return rows; }
    public InventoryType getType() { return type; }

    /**
     * Gets if the size is determined with the type.
     * @return The result.
     */
    public boolean usesType() { return type != null; }

    /**
     * Gets if the size is determined with the number of rows.
     * @return The result.
     */
    public boolean usesRows() { return rows != null; }

    /**
     * Creates an inventory of the specified size and title.
     * @param title The title.
     * @return The inventory.
     */
    public Inventory create(Component title) {
        if (usesType())
            return Bukkit.createInventory(null, type, title);
        else
            return Bukkit.createInventory(null, rows * 9, title);
    }
}
