package com.gitlab.aecsocket.unifiedframework.paper.gui;

import net.kyori.adventure.text.Component;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;

import java.util.Map;

/**
 * An {@link org.bukkit.inventory.Inventory} which a player can interact with to perform actions.
 */
public abstract class GUI {
    /**
     * Gets the GUIManager to register GUIViews to.
     * @return The GUIManager.
     */
    public abstract GUIManager getGUIManager();

    /**
     * Gets the inventory size on creation.
     * @param player The player to create for.
     * @return The size.
     */
    public abstract InventorySize getSize(Player player);

    /**
     * Gets the inventory title on creation.
     * @param player The player to create for.
     * @return The title.
     */
    public abstract Component getTitle(Player player);

    /**
     * Gets a map of items on creation.
     * @param player The player to create for.
     * @return The map of items.
     */
    public abstract Map<Integer, GUIItem> getItems(Player player);

    protected GUIView createView(InventoryView view, Map<Integer, GUIItem> items) { return new GUIView(this, view, items); }

    /**
     * Creates an opened GUIView for the specified player.
     * <p>
     * At this stage, no items have populated the inventory, however it has been added to the GUI manager.
     * @param player The player.
     * @return The GUIView.
     */
    public GUIView createView(Player player) {
        Inventory inv = getSize(player).create(getTitle(player));
        Map<Integer, GUIItem> items = getItems(player);
        GUIView view = createView(player.openInventory(inv), items);
        getGUIManager().putView(view);
        return view;
    }

    /**
     * Opens the GUI to a player.
     * <p>
     * This creates a view with {@link GUI#createView(Player)}, then populates the items inside.
     * @param player The player.
     * @return The GUIView.
     */
    public final GUIView open(Player player) {
        GUIView view = createView(player);
        view.updateItems();
        return view;
    }

    public void onClick(GUIView view, InventoryClickEvent event) {
        InventoryView invView = view.getView();
        int slot = event.getSlot();
        if (event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) event.setCancelled(true);
        else if (event.getClickedInventory() == invView.getTopInventory()) view.clickItem(slot, event);
        else if (view.getCursor() != null) view.getCursor().onInventoryMove(view, event);
    }

    public void onClose(GUIView view, InventoryCloseEvent event) {}

    public void onDrag(GUIView view, InventoryDragEvent event) {
        if (event.getInventory() == event.getView().getTopInventory()) event.setCancelled(true);
    }
}
