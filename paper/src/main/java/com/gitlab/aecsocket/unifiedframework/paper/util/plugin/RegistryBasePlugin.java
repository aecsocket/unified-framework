package com.gitlab.aecsocket.unifiedframework.paper.util.plugin;

import com.gitlab.aecsocket.unifiedframework.core.registry.Identifiable;
import com.gitlab.aecsocket.unifiedframework.core.registry.Registry;
import com.gitlab.aecsocket.unifiedframework.core.registry.loader.ConfigurateRegistryLoader;
import com.gitlab.aecsocket.unifiedframework.core.util.log.LogLevel;
import com.gitlab.aecsocket.unifiedframework.core.util.result.LoggingEntry;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public abstract class RegistryBasePlugin<I extends Identifiable> extends BasePlugin {
    protected final Registry<I> registry = new Registry<>();

    public Registry<I> registry() { return registry; }

    protected abstract Map<Path, Type> registryTypes();
    protected abstract void registerDefaults();

    @Override
    public boolean load(List<LoggingEntry> result) {
        if (super.load(result)) {
            loadRegistry(result);
            return true;
        }
        return false;
    }

    protected void loadRegistry(List<LoggingEntry> result) {
        registry.unregisterAll();

        registerDefaults();

        new ConfigurateRegistryLoader.Hocon<I>(getDataFolder(), registryTypes())
                .options(configOptions)
                .load(registry)
                .forEach(entry -> entry.apply(
                        r -> {},
                        e -> result.add(LoggingEntry.of(LogLevel.WARN, e, "Could not load registry objects from %s", entry.path()))
                ));

        registry.link().forEach(entry -> entry.apply(
                () -> {},
                e -> result.add(LoggingEntry.of(LogLevel.WARN, e, "Could not link %s with %s",
                        entry.root().getId(), entry.target().getId()))
        ));

        registry.removeUnlinked();

        registry.resolve().forEach(entry -> entry.apply(
                r -> {},
                (r, e) -> result.add(LoggingEntry.of(LogLevel.WARN, e, "Could not resolve %s", r.getId()))
        ));

        registry.getRegistry().forEach((id, ref) -> result.add(LoggingEntry.of(LogLevel.VERBOSE, "Registered %s %s", ref.get().getClass().getSimpleName(), ref.getId())));
    }
}
