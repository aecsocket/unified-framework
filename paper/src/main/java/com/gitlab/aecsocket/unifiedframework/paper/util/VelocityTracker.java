package com.gitlab.aecsocket.unifiedframework.paper.util;

import com.gitlab.aecsocket.unifiedframework.core.scheduler.HasTasks;
import com.gitlab.aecsocket.unifiedframework.core.scheduler.Scheduler;
import com.gitlab.aecsocket.unifiedframework.core.scheduler.Task;
import com.gitlab.aecsocket.unifiedframework.core.scheduler.TaskContext;
import com.gitlab.aecsocket.unifiedframework.core.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class VelocityTracker implements HasTasks {
    private final Map<UUID, Location> lastLocation = new HashMap<>();
    private final Map<UUID, Vector> velocities = new HashMap<>();

    public Map<UUID, Vector> velocities() { return new HashMap<>(velocities); }
    public Vector velocity(UUID id) { return velocities.getOrDefault(id, new Vector()); }
    public Vector velocity(Player player) { return velocity(player.getUniqueId()); }

    @Override
    public void runTasks(Scheduler scheduler) {
        scheduler.run(Task.repeating(this::tick, Utils.MSPT));
    }

    public void tick(TaskContext ctx) {
        velocities.clear();
        Bukkit.getOnlinePlayers().forEach(player -> {
            UUID id = player.getUniqueId();
            Location to = player.getLocation();
            Location from = lastLocation.get(id);
            lastLocation.put(id, to.clone());
            if (from == null || !to.getWorld().equals(from.getWorld()))
                return;

            velocities.put(id, to.subtract(from).toVector());
        });
    }
}
