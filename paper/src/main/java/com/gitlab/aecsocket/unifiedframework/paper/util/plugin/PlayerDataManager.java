package com.gitlab.aecsocket.unifiedframework.paper.util.plugin;

import com.gitlab.aecsocket.unifiedframework.core.scheduler.HasTasks;
import com.gitlab.aecsocket.unifiedframework.core.scheduler.Scheduler;
import com.gitlab.aecsocket.unifiedframework.core.scheduler.Task;
import com.gitlab.aecsocket.unifiedframework.core.scheduler.TaskContext;
import com.gitlab.aecsocket.unifiedframework.core.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class PlayerDataManager<D extends PlayerData> implements Listener, HasTasks {
    private final Map<UUID, D> data = new HashMap<>();

    protected abstract D createData(UUID uuid);

    private D map(UUID uuid) {
        D data = createData(uuid);
        data.load();
        return data;
    }

    private D map(Player player) {
        D data = createData(player.getUniqueId());
        data.load();
        data.join(player);
        return data;
    }

    public void setup(Plugin plugin) {
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public Map<UUID, D> data() { return new HashMap<>(data); }

    public D get(UUID uuid) { return data.computeIfAbsent(uuid, this::map); }
    public D get(Player player) { return data.computeIfAbsent(player.getUniqueId(), __ -> map(player)); }

    public D remove(UUID uuid) { return data.remove(uuid); }
    public D remove(Player player) { return remove(player.getUniqueId()); }

    @EventHandler
    public void onLogin(AsyncPlayerPreLoginEvent event) {
        get(event.getUniqueId()).load();
    }

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        get(event.getPlayer()).join(event.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        remove(event.getPlayer().getUniqueId()).save();
    }

    @Override
    public void runTasks(Scheduler scheduler) {
        scheduler.run(Task.repeating(this::tick, Utils.MSPT));
    }

    public void tick(TaskContext ctx) {
        synchronized (data) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                ctx.run(get(player)::tick);
            }
        }
    }
}
