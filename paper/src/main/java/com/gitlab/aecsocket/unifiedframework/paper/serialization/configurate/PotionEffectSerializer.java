package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public class PotionEffectSerializer implements TypeSerializer<PotionEffect> {
    public static final PotionEffectSerializer INSTANCE = new PotionEffectSerializer();

    @Override
    public void serialize(Type type, @Nullable PotionEffect obj, ConfigurationNode node) throws SerializationException {
        if (obj == null) node.set(null);
        else {
            node.node("type").set(obj.getType());
            node.node("duration").set(obj.getDuration());
            node.node("amplifier").set(obj.getAmplifier());
            node.node("ambient").set(obj.isAmbient());
            node.node("particles").set(obj.hasParticles());
            node.node("icon").set(obj.hasIcon());
        }
    }

    @Override
    public PotionEffect deserialize(Type type, ConfigurationNode node) throws SerializationException {
        return new PotionEffect(
                node.node("type").get(PotionEffectType.class),
                node.node("duration").getInt(1),
                node.node("amplifier").getInt(0),
                node.node("ambient").getBoolean(false),
                node.node("particles").getBoolean(true),
                node.node("icon").getBoolean(true)
        );
    }
}
