package com.gitlab.aecsocket.unifiedframework.paper.serialization.configurate;

import com.gitlab.aecsocket.unifiedframework.core.serialization.configurate.AbstractRGBSerializer;
import com.gitlab.aecsocket.unifiedframework.core.util.color.RGBA;
import org.bukkit.Color;
import org.jetbrains.annotations.NotNull;

public class ColorSerializer extends AbstractRGBSerializer<Color> {
    public static final ColorSerializer INSTANCE = new ColorSerializer();

    public ColorSerializer(RGBA.@NotNull Format format) { super(format); }
    public ColorSerializer() {}

    @Override protected int value(Color obj) { return obj.asRGB(); }
    @Override protected Color of(int value) { return Color.fromRGB(value); }
}
